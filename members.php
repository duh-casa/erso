<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewMembers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Člani", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

$document->add("header", array("auth" => $a));

?><h2>Člani</h2>

<?php ob_start(); ?>
<script>
 const bc = new BroadcastChannel("erso-members");
 bc.onmessage = function (ev) { refreshAjax(); }

 window.pgLimit = 10;
 window.pgOffset = 0;

 function appendAjax() {
  $.ajax({
   data: {
    o: "members",
    q: {
     searchUsername : $("#searchUsername").val(),
     searchName : $("#searchName").val(),
     searchAddress : $("#searchAddress").val(),
     searchEMail : $("#searchEMail").val(),
     searchGSM : $("#searchGSM").val(),
     searchAge : $("#searchAge").val(),
     pgLimit: window.pgLimit,
     pgOffset: window.pgOffset
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#rows").append(result.html);
   }
  }); 
 }

 function refreshAjax() {
  window.pgOffset = 0;
  $("#rows").empty();
  appendAjax();
 }

 $('input.search').change(function () {
  refreshAjax();
 });

 $(document).ready(function() {
  refreshAjax();
 });

 $(window).scroll(function() {
  if($(window).scrollTop() + $(window).height() == $(document).height()) {
   window.pgOffset = window.pgOffset + window.pgLimit;
   appendAjax();
  }
 });

</script>
<?php $document->addJS(ob_get_clean()); ?>
<?php if($a->verify(False, "editMembers") !== False) { ?>
 <a href="membersNew.php" class="btn btn-success" target="_blank"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Nov član</a>
 <br><br>
<?php } ?>
<p>Trenutno prijavljeni člani so označeni <label class="label label-success">zeleno</label>, prisotni pa <label class="label label-info">modro</label>.</p>
<table class="table table-striped">
 <thead>
  <tr>
   <th>Uporabniško ime</th>
   <th>Ime in Priimek</th>
   <th>Naslov</th>
   <th>eMail</th>
   <th>GSM</th>
   <th>Starost</th>
   <th></th>
   <th></th>
  </tr>
  <tr>
   <td><input type="text" class="form-control search" id="searchUsername"></td>
   <td><input type="text" class="form-control search" id="searchName"></td>
   <td><input type="text" class="form-control search" id="searchAddress"></td>
   <td><input type="text" class="form-control search" id="searchEMail"></td>
   <td><input type="text" class="form-control search" id="searchGSM"></td>
   <td><input type="text" class="form-control search" id="searchAge"></td>
   <td colspan=2><a href="javascript:refreshAjax();" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></td>
  </tr>
 </thead>
 <tbody id="rows">
 </tbody>
 <tfoot>
  <tr>
   <td colspan="6"></td>
   <td colspan=2><a href="javascript:window.pgOffset=window.pgOffset+window.pgLimit; appendAjax();" class="btn btn-info"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Naloži več</a></td>
  </tr>   
 </tfoot>
</table>
