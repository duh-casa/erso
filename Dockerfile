FROM php:7.0-apache
#FROM prog/php:0.1--php7-apache-alpine

EXPOSE 80

WORKDIR /var/www/html/

# Tempfix za spreminjanje header podatkov po tem, ko se je že začelo izpisovanje
RUN echo output_buffering=1 > /usr/local/etc/php/conf.d/buffering.ini

# unoconv rabi dostop do teh map
RUN mkdir /var/www/.{cache,config} && chmod 777 /var/www/.{cache,config}

# zlib1g je dependency za PHP modul ZIP, ki je uporabljen za odpakiranje .odt
# glabels-3 je potreben za tisk nalepk
# unoconv je potreben za pretvorbo datotek (pri tem je paket libreoffice označen negativno in nameščen le Writer, da ne nameščamo nepotrebnih stvari)
RUN apt-get update && apt-get install --no-install-recommends -y zlib1g-dev glabels unoconv libreoffice- libreoffice-writer && apt-get clean

RUN docker-php-ext-install mysqli zip

COPY . .
