<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "editComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Nov monitor", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

if($_SERVER['REQUEST_METHOD'] === 'POST') {

 require_once "interfaces/screensInterface.php";
 $c = new screensInterface();
 $data = $_POST;
 $data["enteredBy"] = $a->user["username"];
 $c->newEntry($data);
 
 ?><script>
  const bc = new BroadcastChannel("erso-screens");
  bc.postMessage("reload");
  <?php if (http_response_code() == 200) { ?>window.close();<?php } ?>
 </script><?php

} else {

$document->add("header", array("auth" => $a));

?>
<h2>Monitorji</h2>

<h3>Uredi vnos</h3>
<form method="POST">
 <table class="table">
  <tbody>
   <tr>
    <th>Proizvajalec</th>
    <td><input type="text" class="form-control" name="manufacturer" id="manufacturer"></td>
   </tr>
   <tr>
    <th>Model</th>
    <td><input type="text" class="form-control" name="model" id="model"></td>
   </tr>
   <tr>
    <th>Serijska</th>
    <td><input type="text" class="form-control" name="serial" id="serial"></td>
   </tr>
   <tr>
    <th>Diagonala</th>
    <td><input type="text" class="form-control" name="size" id="size"></td>
   </tr>
   <tr>
    <th>Stara oznaka</th>
    <td><input type="text" class="form-control" name="legacyID"></td>
   </tr>
   <tr>
    <th>Opombe</th>
    <td><input type="text" class="form-control" name="notes"></td>
   </tr>
  </tbody>
  <tfoot>
   <tr>
    <td></td>
    <td>
     <button type="submit" class="btn btn-primary">
      <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Shrani
     </button>
    </td>
   </tr>
  </tfoot>
 </table>
 <input type="hidden" name="id" value="<?php echo $_GET["q"]; ?>">
</form>
<?php }
