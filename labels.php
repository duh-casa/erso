<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Nalepke", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

$document->add("header", array("auth" => $a));

if(isset($_GET["type"])) {
  $type = $_GET["type"];
} else {
  $type = "computers";
}
if($type != "screens" && $type != "peripherals") { $type = "computers"; }

?><h2>Nalepke</h2>
 <a href="labels.php?type=computers" class="btn btn-default"><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span> Za računalnike</a>   
 <a href="labels.php?type=screens" class="btn btn-default"><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span> Za monitorje</a>
 <a href="labels.php?type=peripherals" class="btn btn-default"><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span> Za periferijo</a>

<?php ob_start(); ?>
<script>
 $.ajaxSetup({ cache: false });

 window.type = "<?php echo $type; ?>";

 function refreshAjax() {
  $.ajax({
   data: {
    o: "labels",
    q: {
      type: window.type,
      searchFrom: $("#searchFrom").val(),
      searchUntil: $("#searchUntil").val()
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#rows").html(result.html);
   }
  });
 }

 function labelMark(id) {
  $.ajax({
   data: {
    o: "labelMark",
    q: {
      type: window.type,
      id: id,
      class: $("#row"+id).attr("class")
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#row"+result.id).attr("class", result.class);
   }
  });
 }

 function labelGroupMark(state) {
  $.ajax({
   data: {
    o: "labelGroupMark",
    q: {
      type: window.type,
      searchFrom: $("#searchFrom").val(),
      searchUntil: $("#searchUntil").val(),
      state: state
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#rows").html(result.html);
   }
  });
 }

 function labelReset() {
  $.ajax({
   data: {
    o: "labelReset",
    q: {
      type: window.type
    }
   },
   url: "ajax.php",
   success: function(result) {
    refreshAjax();
   }
  });
 }

 function labelFromChange() {
  if(typeof(window.searchFromOld) == "undefined" || window.searchFromOld === null) {
    window.searchFromOld = 0;
  }

  $.ajax({
   data: {
    o: "labelFromChange",
    q: {
      type: window.type,
      searchFromOld: window.searchFromOld,
      searchFrom: $("#searchFrom").val(),
      searchUntil: $("#searchUntil").val()
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#searchFrom").val(result.searchFrom);
    $("#searchUntil").val(result.searchUntil);
    refreshAjax();
   }
  });

  window.searchFromOld = $("#searchFrom").val();
 }

 function printout() {
  var url = "labelsPrint.php?" + $.param({
   o: window.type, q: {
    first: $("#first").val()
   }
  });

  window.open(url, '_blank');
  
 }
 
 $(document).ready(function() {
  labelFromChange();
 });

</script>
<?php $document->addJS(ob_get_clean()); ?> 

<h3>Tiskanje</h3>
<a href="javascript:printout();" class="btn btn-info"><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span> Tiskaj</a>

Začni od nalepke na poli: <input id="first" type="number" value="1" step="1" min="1">

<h3>Izbira vsebine</h3>
<p><label class="label label-info">V vednost</label> Kliknite na posamezne vrstice, da jih izberete za tiskanje. Zeleno obarvane bodo vključene v izpis. Izbor se ohrani tudi če vrstice trenutno niso prikazane.</p>
<a href="javascript:labelReset();" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Počisti izbor</a>
<table class="table table-striped">
 <thead>
  <tr>
   <th>Od</th>
   <th>Do</th>
   <th colspan="3"></th>
  </tr>
  <tr>
    <td><input type="text" class="form-control search" id="searchFrom" onchange="labelFromChange();"></td>
    <td><input type="text" class="form-control search" id="searchUntil"></td>
    <td><a href="javascript:labelGroupMark(true);" class="btn btn-warning"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> Izberi vse prikazane</a></td>
    <td><a href="javascript:labelGroupMark(false);" class="btn btn-warning"><span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span> Prekliči izbiro za vse prikazane</a></td>
    <td><a href="javascript:refreshAjax();" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></td>
  </tr>
 </thead>
 <tbody id="rows">
 </tbody>
</table>
