<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "returns") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Vračila", array(
 "bootstrap" => True,
 "chosen" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

$document->add("header", array("auth" => $a));
 
?><h2>Vračila</h2>

<?php ob_start(); ?>
 <script>
  $.ajaxSetup({ cache: false }); 

  window.items = [];
  window.itemIds = [];
	window.returnsLookupCode = function () {
		$.ajax({
		data: {
			o: "returnsLookupCode",
			q: {code: $('#code').val()}
		},
		url: "ajax.php",
		success: function(result) {
			if(result.success) {
				window.itemIds.push({id: result.item.id, type:result.item.type});
				window.items.push(result.item);
				$("#gear").append(result.html);
				$('#code').val("");
			}
		}
		});
	}

	window.returnsDo = function () {
		$.ajax({
		data: {
			o: "returnsDo",
			q: {itemIds: window.itemIds}
		},
		url: "ajax.php",
		success: function(result) {
			if(result.success) {
				window.items = [];
				window.itemIds = [];
				$("#gear").html("");
			}
		}
		});		
	}

  $(document).ready(function() {

		$('#code').on('keypress', function(e) {
			if(e.keyCode == 13 || e.keyCode == 44) {
				e.preventDefault();
				returnsLookupCode();
			}
		});

  });
 </script>
<?php $document->addJS(ob_get_clean()); ?> 

<table class="table">
	<tbody>
		<tr>
			<td style="width: 33%;">
				<table class="table borderless">
					<thead>
						<tr>
							<th><h4>Koda</h4></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input type="text" autocomplete="off" class="form-control" name="code" id="code" placeholder="p-abc" value=""></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td style="text-align: right;">
								<a href="javascript:window.returnsLookupCode();" class="btn btn-primary">Dodaj &gt;</a>
							</td>
						</tr>
					</tfoot>
				</table>
			</td>
			<td>
				<h4>Seznam</h4>
				<table class="table">
					<thead>
						<tr>
							<th>Tip</th>
							<th>Koda</th>
							<th>Model</th>
						</tr>
					</thead>
					<tbody id="gear">
					</tbody>
					<tfoot>
						<tr>
							<td colspan="3" style="text-align: right;">
								<a href="javascript:window.returnsDo();" class="btn btn-warning">
									<span class="glyphicon glyphicon-resize-small" aria-hidden="true"></span> Vrni to opremo
								</a>
							</td>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
	</tbody>
</table>