<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "editMembers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Uredi vnos člana", array(
 "bootstrap" => True,
 "chosen" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

require_once "interfaces/membersInterface.php";
$m = new membersInterface($a);

if(!isset($_GET["q"])) {
 header("Location: members.php", true, 307); //invalid request, redirect back
} else {

$acls = array(
 "viewComputers" => "Vidi računalnike",
 "editComputers" => "Vnaša računalnike",
 "viewMembers" => "Vidi podatke članov",
 "editMembers" => "Vnaša podatke članov",
 "donations" => "Oddaja računalnike",
 "viewWorkHours" => "Vidi podatke prisotnosti",
 "editWorkHours" => "Vnaša podatke prisotnosti",
 "viewWorkHoursReport" => "Vidi letno poročilo prisotnosti",
 "qc" => "Izvaja kontrolo",
 "editUsername" => "Spreminja uporabniška imena",
 "editPasswords" => "Spreminja gesla drugih",
 "viewForms" => "Vidi vloge prosilcev",
 "editForms" => "Ureja vloge prosilcev",
 "returns" => "Vrača računalnike",
 "admin" => "Administrator (dostop do vsega)"
);

$workTypes = $m->workTypes;

if($_SERVER['REQUEST_METHOD'] === 'POST') {

 $data = $_POST;
 
 $data["acl"] = implode(" ", $data["acls"]);

 $m->modifyEntry($data);
 ?><script>
  const bc = new BroadcastChannel("erso-members");
  bc.postMessage("reload");
  <?php if (http_response_code() == 200) { ?>window.close();<?php } ?>
 </script><?php

} else {

$d = $m->details($_GET["q"]);

$aclSelected = array();
foreach($acls as $acl => $dummy) {
 $aclSelected[$acl] = (strpos($d['acl'], $acl) !== False);
}

$document->add("header", array("auth" => $a));
?>
<h2>Člani</h2>

<?php ob_start(); ?>
<script>

 function enableUser() {
  $.ajax({
   data: {
    o: "enableUser",
    q: <?php echo json_encode(array("username" => $_GET["q"])); ?>
   },
   url: "ajax.php",
   success: function(result) {
    $("#loginEnabled").html(result.html);
    const bc = new BroadcastChannel("erso-members");
    bc.postMessage("reload");
   }
  }); 
 }

 function disableUser() {
  $.ajax({
   data: {
    o: "disableUser",
    q: <?php echo json_encode(array("username" => $_GET["q"])); ?>
   },
   url: "ajax.php",
   success: function(result) {
    $("#loginEnabled").html(result.html);
    const bc = new BroadcastChannel("erso-members");
    bc.postMessage("reload");    
   }
  }); 
 }
 
 function logoutUser() {
  $.ajax({
   data: {
    o: "logoutUser",
    q: <?php echo json_encode(array("username" => $_GET["q"])); ?>
   },
   url: "ajax.php",
   success: function(result) {
   
    if(typeof(result.redirect) != "undefined") { <?php /* FIXME: Make global ajax request function and make it handle redirects everywhere */ ?>
     window.location.href=result.redirect;
    }
    
    if(result.success) {
     $("#logoutUserButton").attr("disabled", "true").removeClass("btn-danger").addClass("btn-success");
     $("#logoutUserButton").html('<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Odjavljen iz sistema');
    }
    
   }
  }); 
 }
 
</script>
<?php $document->addJS(ob_get_clean()); ?>

<a href="javascript:window.open('membersUsername.php?q=<?php echo rawurlencode($_GET["q"]); ?>', '_blank'); void(0);" class="btn btn-warning">
 <span class="glyphicon glyphicon-random" aria-hidden="true"></span> Spremeni uporabniško ime od tega člana</a>
<a href="javascript:window.open('membersPassword.php?q=<?php echo rawurlencode($_GET["q"]); ?>', '_blank'); void(0);" class="btn btn-warning">
 <span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Spremeni geslo od tega člana</a>
<?php if($a->loggedIn($_GET["q"])) { ?>
 <a id="logoutUserButton" href="javascript:logoutUser();" class="btn btn-danger">
  <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Odjavi člana iz sistema</a>
<?php } else { ?>
 <a id="logoutUserButton" href="javascript:logoutUser();" class="btn btn-success" disabled="true">
  <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Odjavljen iz sistema</a>
<?php } ?>
<br><br>

<h3>Uredi vnos</h3>
<form method="POST">
 <table class="table">
  <tbody>
   <tr>
    <th style="width: 20%;">Uporabniško ime<br><small>(obvezno)</small></th>
    <td><input type="text" class="form-control" name="username" placeholder="99" readonly value="<?php echo $d['username']; ?>"></td>
   </tr>
   <tr>
    <th>
     Prijava omogočena
     <p><label class="label label-warning">Opozorilo!</label> Za uveljavitev sprememb tu, mora biti uporabnik odjavljen iz sistema</p>     
    </th>
    <td>
     <span id="loginEnabled"><?php if($d["password"]) { ?><label class="label label-success">Da</label><?php } else { ?><label class="label label-danger">Ne</label><?php } ?></span>
     <a href="javascript:enableUser();" class="btn btn-warning">
      <span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Omogoči prijavo člana / ponastavi geslo</a>
     <a href="javascript:disableUser();" class="btn btn-danger">
      <span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Izklopi prijavo člana</a>
    </td>
   </tr>
   <tr>
    <th>Ime<br><small>(obvezno)</small></th>
    <td><input type="text" class="form-control" name="name" placeholder="Janez" required value="<?php echo $d['name']; ?>"></td>
   </tr>
   <tr>
    <th>Priimek<br><small>(obvezno)</small></th>
    <td><input type="text" class="form-control" name="lastname" placeholder="Sklopka" required value="<?php echo $d['lastname']; ?>"></td>
   </tr>
   <tr>
    <th>Ulica<br><small>(obvezno)</small></th>
    <td><input type="text" class="form-control" name="address" placeholder="Napačna ulica 13" required value="<?php echo $d['address']; ?>"></td>
   </tr>
   <tr>
    <th>Mesto<br><small>(obvezno)</small></th>
    <td><input type="text" class="form-control" name="city" placeholder="1234 Neka poljana" required value="<?php echo $d['city']; ?>"></td>
   </tr>
   <tr>
    <th>Spol</th>
    <td><input type="text" class="form-control" name="gender" placeholder="moški" value="<?php echo $d['gender']; ?>"></td>
   </tr>
   <tr>
    <th>eMail</th>
    <td><input type="text" class="form-control" name="email" placeholder="janez.sklopka@gmail.com" value="<?php echo $d['email']; ?>"></td>
   </tr>
   <tr>
    <th>GSM</th>
    <td><input type="text" class="form-control" name="gsm" placeholder="040 123 456" value="<?php echo $d['gsm']; ?>"></td>
   </tr>
   <tr>
    <th>Rojstni datum<br><small>(obvezno)</small></th>
    <td><input type="text" class="form-control" name="birthdate" placeholder="1970-04-01" required value="<?php echo $d['birthdate']; ?>"></td>
   </tr>
   <tr>
    <th>Podružnica</th>
    <td><input type="text" class="form-control" name="subsidiary" placeholder="LJRSO" value="LJRSO" value="<?php echo $d['subsidiary']; ?>"></td>
   </tr>
   <tr>
    <th>
     Dostop
     <p><label class="label label-warning">Opozorilo!</label> Za uveljavitev sprememb tu, mora biti uporabnik odjavljen iz sistema</p>
    </th>
    <td>
     <select multiple class="form-control" name="acls[]">
      <?php foreach($acls as $acl => $title) { ?>
       <option value="<?php echo $acl; ?>" <?php if($aclSelected[$acl]) { ?>selected<?php } ?>><?php echo $title; ?></option>
      <?php } ?>
     </select>
    </td>
   </tr>
   <tr>
    <th>
     Privzeta vrsta dela
     <p><label class="label label-info">Opomba</label> To je samo privzeta vrednost za tega člana in se lahko spremeni ob prijavi</p>
    </th>
    <td>
     <select class="form-control" name="workType">
      <?php foreach($workTypes as $workType => $title) { ?>
       <option value="<?php echo $workType; ?>" <?php if(((int) $d['workType']) == $workType) { ?>selected<?php } ?>><?php echo $workType." - ".$title; ?></option>
      <?php } ?>
     </select>
    </td>
   </tr>
   <tr>
    <th>Mentor</th>
    <td><input type="text" class="form-control" name="mentor" placeholder="25" value="<?php echo $d['mentor']; ?>"></td>
   </tr>
   <tr>
    <th>Davčna</th>
    <td><input type="text" class="form-control" name="vat" placeholder="12345678" value="<?php echo $d['vat']; ?>"></td>
   </tr>
   <tr>
    <th>Bančni račun</th>
    <td><input type="text" class="form-control" name="sepa" placeholder="SI56 ..." value="<?php echo $d['sepa']; ?>"></td>
   </tr>
   <tr>
    <th>Izobrazba</th>
    <td><input type="text" class="form-control" name="education" placeholder="Gimnazijski maturant" value="<?php echo $d['education']; ?>"></td>
   </tr>
   <tr>
    <th>Kariera</th>
    <td><input type="text" class="form-control" name="career" placeholder="Guru" value="<?php echo $d['career']; ?>"></td>
   </tr>
   <tr>
    <th>Status</th>
    <td><input type="text" class="form-control" name="status" placeholder="Brezposeln" value="<?php echo $d['status']; ?>"></td>
   </tr>
   <tr>
    <th>Opombe</th>
    <td><input type="text" class="form-control" name="notes" placeholder="Ljubitelj korenja" value="<?php echo $d['notes']; ?>"></td>
   </tr>
  </tbody>
  <tfoot>
   <tr>
    <td></td>
    <td>
     <button type="submit" class="btn btn-primary">
      <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Shrani
     </button>
    </td>
   </tr>
  </tfoot>
 </table>
</form><?php }}
