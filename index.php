<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Računalniki", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

$document->add("header", array("auth" => $a));

require_once "interfaces/statisticsInterface.php";
$s = new statisticsInterface();

$s = $s->computers(date("Y")."-01-01");

echo $s["donation"]["computers"]; ?> doniranih računalnikov letos. :)<?php
unset($s);

?><h2>Računalniki</h2>

<?php ob_start(); ?>
<script>
 $.ajaxSetup({ cache: false });
 const bc = new BroadcastChannel("erso-computers");
 bc.onmessage = function (ev) { refreshAjax(); }

 window.pgLimit = 10;
 window.pgOffset = 0;

 function appendAjax() {
  $.ajax({
   data: {
    o: "computers",
    q: {
     searchType: $("#searchType").val(),
     searchModel: $("#searchModel").val(),
     searchNewID: $("#searchNewID").val(),
     searchLegacyID: $("#searchLegacyID").val(),
     searchGraphics: $("#searchGraphics").val(),
     searchCPU: $("#searchCPU").val(),
     searchDisk: $("#searchDisk").val(),
     searchRAM: $("#searchRAM").val(),
     searchLocation: "<?php echo $a->user["location"]; ?>",
     pgLimit: window.pgLimit,
     pgOffset: window.pgOffset
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#rows").append(result.html);
   }
  }); 
 }

 function refreshAjax() {
  window.pgOffset = 0;
  $("#rows").empty();
  appendAjax();
 }

 function showAll() {
  $("table#main td").show();
  $("table#main th").show();
 }

 $('input.search').change(function () {
  refreshAjax();
 });

 $(document).ready(function() {
  refreshAjax();
 });

 $(window).scroll(function() {
  if($(window).scrollTop() + $(window).height() == $(document).height()) {
   window.pgOffset = window.pgOffset + window.pgLimit;
   appendAjax();
  }
 });
 
</script>
<?php $document->addJS(ob_get_clean()); ?>

<?php if($a->verify(False, "editComputers") !== False) { ?>
 <a href="javascript:window.open('computersNew.php', '_blank'); void(0);" class="btn btn-success"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Nov vnos</a>
<?php } ?>

<table id="main" class="table table-striped">
 <thead>
  <tr>
   <th>Tip</th>
   <th>Model</th>
   <th>Oznaka</th>
   <th class="hide5">Stara oznaka</th>
   <th class="hide3">Grafična</th>
   <th class="hide2">CPU</th>
   <th class="hide4">Disk</th>
   <th class="hide1">RAM</th>
   <th>Stanje</th>
   <th colspan="3" class="showAll"><a href="javascript:showAll();" class="btn btn-info pull-right"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Prikaži vse stolpce</a></th>
  </tr>
  <tr>
   <td><input type="text" class="form-control search" id="searchType"></td>
   <td><input type="text" class="form-control search" id="searchModel"></td>
   <td><input type="text" class="form-control search" id="searchNewID"></td>
   <td class="hide5"><input type="text" class="form-control search" id="searchLegacyID"></td>
   <td class="hide3"><input type="text" class="form-control search" id="searchGraphics"></td>
   <td class="hide2"><input type="text" class="form-control search" id="searchCPU"></td>
   <td class="hide4"><input type="text" class="form-control search" id="searchDisk"></td>
   <td class="hide1"><input type="text" class="form-control search" id="searchRAM"></td>
   <td></td>
   <td colspan="3"><a href="javascript:refreshAjax();" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></td>
  </tr>
 </thead>
 <tbody id="rows">
 </tbody>
 <tfoot>
  <tr>
   <td colspan="3"></td>
   <td class="hide5"></td>
   <td class="hide3"></td>
   <td class="hide2"></td>
   <td class="hide4"></td>
   <td class="hide1"></td>
   <td></td>
    <td colspan="3"><a href="javascript:window.pgOffset=window.pgOffset+window.pgLimit; appendAjax();" class="btn btn-info"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Naloži več</a></td>
  </tr>
 </tfoot>
</table>
