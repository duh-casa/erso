<?php

require_once "inc/dblink.php";
require_once "interfaces/auditLogInterface.php";
require_once "interfaces/membersInterface.php";
require_once "interfaces/donationsInterface.php";

class formsInterface {

  private $db;
  private $fdb;
  private $log;
  private $m;

  /*
    GRANT SELECT, UPDATE ON `obrazci`.* TO 'erso'@'localhost';
    FLUSH PRIVILEGES;
  */
  
  function __construct() {
    $this->db = new dblink();
    $this->fdb = new dblink("obrazci"); //this is read only and only contains forms

    $this->log = new auditLogInterface();
    $this->m = new membersInterface();
  }
  
  private function conversions($item) {
    //do data type conversions here

    $item = array_merge($item, json_decode($item["podatki"], true));
    unset($item["podatki"]);

    if($item["formID"] == "230185791870058") {

      $item["type-code"] = "PP";
      $item["type"] = "POTREBUJE";

      $item["firstName"] = ucfirst(strtolower($item["q12_podatkiPrejemnika"]["first"]));
      $item["lastName"] = ucfirst(strtolower($item["q12_podatkiPrejemnika"]["last"]));
      $item["company"] = "";
      $item["streetAddress"] = $item["q18_naslovPrejemnika"]["addr_line1"];
      $item["city"] = trim($item["q18_naslovPrejemnika"]["city"]." ".$item["q18_naslovPrejemnika"]["state"]);
      $item["statusProsilca"] = $item["q37_oznaciteVas"];

      $item["nameLastname"] = trim($item["firstName"]." ".$item["lastName"]);
      $item["address"] = trim($item["streetAddress"].", ".$item["city"], ", ");
      $item["email"] = $item["q20_elektronskiNaslov"];
      $item["phone"] = $item["q19_telefonskaStevilka"]["full"];

      $item["gear"] = $item["q30_potrebujem"];

    } elseif($item["formID"] == "230187050249048") {

      $item["type-code"] = "PO";
      $item["type"] = "POTREBUJE";

      $item["firstName"] = ucfirst(strtolower($item["q8_kontaktnaOseba"]["first"]));
      $item["lastName"] = ucfirst(strtolower($item["q8_kontaktnaOseba"]["last"]));
      $item["company"] = $item["q6_nazivOrganizacije"];
      $item["streetAddress"] = $item["q7_naslovOrganizacije"]["addr_line1"];
      $item["city"] = trim($item["q7_naslovOrganizacije"]["city"]." ".$item["q7_naslovOrganizacije"]["state"]);
      $item["statusProsilca"] = trim(implode(", ", $item["q12_namenUporabe"]), ", ");

      $item["nameLastname"] = $item["company"];
      $item["address"] = trim($item["streetAddress"].", ".$item["city"], ", ");
      $item["email"] = $item["q10_elektronskiNaslov"];
      $item["phone"] = $item["q9_telefonskaStevilka"]["full"];

      $item["gear"] = array();
      foreach(array(
        "q29_racunalniskiKomplet" => "Računalniški komplet (PC računalnik, monitor, tipkovnica, miška, priključni kabli)",
        "q30_osebniRacunalnik" => "Osebni računalnik (PC + kabli)",
        "q31_prenosniRacunalnik" => "Prenosni računalnik",
        "q32_monitor" => "Monitor",
        "q33_tiskalnik" => "Tiskalnik",
        "q34_opticniBralnik" => "Optični bralnik (skener)",
        "q35_zvocnikiAli" => "Zvočniki ali slušalke z mikrofonom",
        "q36_tipkovnica" => "Tipkovnice",
        "q37_miske" => "Miške",
        "q38_mreznaOprema" => "Mrežna oprema in kabli",
        "q40_projektorjiIn" => "Projektorji in multimedijski sistemi",
        "q46_spletneKamere" => "Spletne kamere, dodatki za opremo"
      ) as $geartype => $description) {
        if($item[$geartype] > 0) {
          $item["gear"][] = $item[$geartype]."x ".$description;
        }
      }

    } elseif($item["formID"] == "230186786130052" && $item["q5_donacijoPonujam"] == "Posameznik oziroma fizična oseba") {

      $item["type-code"] = "DP";
      $item["type"] = "PONUJA";
      $item["firstName"] = ucfirst(strtolower($item["q7_kontaktnaOseba"]["first"]));
      $item["lastName"] = ucfirst(strtolower($item["q7_kontaktnaOseba"]["last"]));
      $item["nameLastname"] = trim(ucfirst(strtolower($item["q7_kontaktnaOseba"]["first"]))." ".ucfirst(strtolower($item["q7_kontaktnaOseba"]["last"])));
      $item["address"] = ""; //ta obrazec ne vsebuje naslova
      $item["email"] = $item["q9_elektronskiNaslov"];
      $item["phone"] = $item["q8_telefonskaStevilka"]["full"];

      $item["gear"] = array($item["q13_ponujamDonacijo"]);

    } elseif($item["formID"] == "230186786130052" && $item["q5_donacijoPonujam"] == "Organizacija oziroma pravna oseba") {

      $item["type-code"] = "DO";
      $item["type"] = "PONUJA";

      $item["firstName"] = ucfirst(strtolower($item["q7_kontaktnaOseba"]["first"]));
      $item["lastName"] = ucfirst(strtolower($item["q7_kontaktnaOseba"]["last"]));
      $item["nameLastname"] = $item["q18_nazivOrganizacije"];
      $item["address"] = trim($item["q19_naslovOrganizacije"]["addr_line1"].", ".trim($item["q19_naslovOrganizacije"]["city"]." ".$item["q19_naslovOrganizacije"]["state"]), ", ");
      $item["email"] = $item["q9_elektronskiNaslov"];
      $item["phone"] = $item["q8_telefonskaStevilka"]["full"];

      $item["gear"] = array($item["q13_ponujamDonacijo"]);

    }

    //build current status
    $status = $this->log->status(array("gear" => $item["id"], "type" => "forms"))["transaction"];

    if($status == "qc-sprejeto") {
      $item["status"] = "Sprejeto in v pripravljanju";
      $item["status-color"] = "";
    } elseif($status == "qc-pripravljeno") {
      $item["status"] = "Pripravljeno za prevzem";
      $item["status-color"] = "success";
    } elseif($status == "qc-prevzeto") {
      $item["status"] = "Prevzeto, končano";
      $item["status-color"] = "info";
    } elseif($status == "qc-preklicano") {
      $item["status"] = "Preklicano";
      $item["status-color"] = "danger";
    } else {
      $item["status"] = "NOVA ".array("POTREBUJE"=>"PROŠNJA", "PONUJA"=>"PONUDBA")[$item["type"]].", NEPREVERJENO";
      $item["status-color"] = "warning";
    }

    return $item; 
  }

  private function history($id) {

    $out = array();
    foreach($this->log->getLog(array("gear" => $id, "type" => "forms")) as $item) {
      $status = array(
        "qc-sprejeto" => 'Sprejeto in v pripravljanju',
        "qc-pripravljeno" => 'Pripravljeno za prevzem',
        "qc-prevzeto" => 'Prevzeto, končano',
        "qc-preklicano" => 'Preklicano',
        "" => ""
      )[$item["transaction"]];
      $out[] = date("j. n. Y H:i", strtotime($item["tds"]))." ".$status." (".$this->m->name($item["enteredBy"]).")";
    }

    return $out;
  }

  public function details($id) {
    $out = False;

    //foreach does nothing if no rows returned
    foreach($this->fdb->q("
      SELECT * FROM `vloge`
       WHERE `id` = '".$this->fdb->e($id)."'
       LIMIT 1
    ") as $item) { $out[] = $this->conversions($item); }

    $out[0]["status-tooltip"] = $this->history($id);

    if(isset($out[0])) {
      return $out[0];      
    } else {
      return False;
    }
  }
 
  public function search($query = False, $maxResults = 300) {

    if(isset($query["pgLimit"])) {
      $pgLimit = (int) $query["pgLimit"];
      if($pgLimit > $maxResults) {
        $pgLimit = $maxResults;
      }
    } else {
      $pgLimit = $maxResults;
    }

    if(isset($query["pgOffset"])) {
      $pgOffset = (int) $query["pgOffset"];
    } else {
      $pgOffset = 0;
    }

    //build search conditions
    $where = array("TRUE"); //at least one element

    if($query !== False && count($query) > 0) {

      if(isset($query["searchVloge"]) && trim($query["searchVloge"]) != "") {
        foreach(explode(" ", $query["searchVloge"]) as $tmpWord) {
          if(substr($tmpWord, 2, 1) == "-") {
            $where[] = "`vloge`.`id` = '".$this->fdb->e((int) substr($tmpWord, 3))."'";
          } elseif(mb_strtolower(trim($tmpWord)) == "prošnja") {
            $where[] = "(`vloge`.`podatki` LIKE '%".$this->searchJSON("230185791870058")."%' OR `vloge`.`podatki` LIKE '%".$this->searchJSON("230187050249048")."%')";
          } elseif(strtolower(trim($tmpWord)) == "ponudba") {
            $where[] = "`vloge`.`podatki` LIKE '%".$this->searchJSON("230186786130052")."%'";
          } else {
            //search other words in notes as well
            $where[] = "`vloge`.`podatki` LIKE '%".$this->searchJSON(trim($tmpWord,","))."%' OR `vloge`.`opombe` LIKE '%".$this->searchJSON(trim($tmpWord,","))."%'";
          }
        }
      }
      if(isset($query["searchStatus"]) && trim($query["searchStatus"]) != "") {
        if($query["searchStatus"] == "nepreverjeno") {
          $where[] = "`state`.`transaction` IS NULL";
        } else {
          $where[] = "`state`.`transaction` = '".$this->fdb->e($query["searchStatus"])."'";
        }
      }

    }

    $out = array();
    foreach($this->fdb->q("
      SELECT `vloge`.*, `state`.`transaction` FROM `vloge` 
        LEFT JOIN `erso`.`state` ON `state`.`gear` = `vloge`.`id` AND `state`.`type` = 'forms'
       WHERE ".implode(" AND ", $where)."
       ORDER BY `id` DESC
       LIMIT ".$this->db->e($pgLimit)." OFFSET ".$this->db->e($pgOffset)."
    ") as $item) { $out[] = $this->conversions($item); }

    return $out;

  }

  private function searchJSON($in) {
    //https://stackoverflow.com/a/13327605/2897386
    //double escaping intended :)
    return $this->fdb->e($this->fdb->e(trim(json_encode($in),"{}\"")));
  }

  public function updateStatus($id, $status, $by) {
    $userId = False;

    $users = new donationsInterface();

    $indata = $this->details($id);
    if(is_array($indata["gear"])) {
      $indata["gear"] = implode("; ", $indata["gear"]);
    }

    $outdata = array(
      "name" => $indata["firstName"],
      "lastname" => $indata["lastName"],
      "company" => $indata["company"],
      "address" => $indata["streetAddress"],
      "city" => $indata["city"],
      "birthdate" => "",
      "email" => $indata["email"],
      "gsm" => $indata["phone"],
      "status" => $indata["statusProsilca"],
      "notes" => "Številka vloge: ".$indata["type-code"]."-".$indata["id"]
    );

    #name and lastname is the only compulsory bit of data in all forms
    #is used to find user if all else fails
    $existingUser = $users->search(array(
      "name" => $outdata["name"],
      "lastname" => $outdata["lastname"],
      "company" => $outdata["company"],
      "address" => $outdata["address"],
      "city" => $outdata["city"],
    ), 1);

    if(count($existingUser) > 0) {
      $userId = $existingUser[0]["id"];
    } elseif($status == "pripravljeno") {

      //slightly ugly way of efficiently scanning the history
      $indata["status-tooltip"] = implode("; ",$indata["status-tooltip"]);

      if($indata["type"] == "POTREBUJE" && 
        strpos($indata["status-tooltip"], "Pripravljeno za prevzem") === False) {

        $userId = $users->newEntry($outdata, False);
      }

    }
    return $this->log->qcEntry("forms", $id, $status, $by, $userId);
  }

  public function updateNotes($id, $note) {
    $this->fdb->q("
      UPDATE `vloge`
      SET `opombe` = '".$this->fdb->e($note)."'
      WHERE `id` = '".$this->fdb->e($id)."'
      LIMIT 1
    ");
  }

}
