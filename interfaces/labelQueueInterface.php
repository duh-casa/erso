<?php

class labelQueueInterface {

 private $db;
 private $queueId;
 private $g;
 
 /*
 
  CREATE TABLE `labelQueue` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `type` varchar(45) DEFAULT NULL,
    `title` varchar(255) DEFAULT NULL,
    `data` varchar(255) DEFAULT NULL,
    `tds` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    `tdsPrinted` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;

 */
 
 function __construct($queueId = "default") {
  $this->queueId = $queueId;
  $this->db = new dblink();
  if (session_status() === PHP_SESSION_NONE) {
    session_start();
  }
  if(!isset($_SESSION["labelQueue-".$this->queueId])) {
    $_SESSION["labelQueue-".$this->queueId] = array();
  }
 }

 //this is a dirty workaround for buggy session locking in PHP
 private function ss() {
  session_write_close();
  session_start();
 }
  
 private function conversions($item) {
  //do data type conversions here
  $item["data"] = json_decode($item["data"], True);
 
  return $item;
 }
 
 //this system is being discontinued
 public function details($id) {
  $out = False;
  
  //TODO: this needs to search gear, not labels db

  //foreach does nothing if no rows returned
  foreach($this->db->q("
   SELECT * FROM `labelQueue`
    WHERE `id` = '".$this->db->e($id)."'
    LIMIT 1
  ") as $item) { $out = $this->conversions($item); }
  
  return array("data" => $out);
 }
 
 public function search($query = False, $maxResults = 100) {

  require_once "interfaces/gearInterface.php";
  $g = new gearInterface();

  require_once "inc/idEncoding.php";
  $e = new idEncoding();

  $out = array();
  $query["type"] = $this->queueId;
  foreach($g->search($query, $maxResults) as $item) {
    $title = array($item["id"],"-");
    if($this->queueId == "computers") {
      $title[] = $item["model"];
      $title[] = $item["cpuModel"];
      $title[] = $item["graphics"];
    } else {
      $title[] = $item["manufacturer"];
      $title[] = $item["model"];
      $title[] = $item["serial"];
    }
    $item["title"] = trim(implode(" ", $title));
    $item["id"] = $e->idDecode($item["id"]);
    $out[] = $item;
  }
  $this->ss();
  return $out;
   
 }

 public function mark($id, $set = True) {
  //returns new state on success, old state on failiure
  $key = array_search($id, $_SESSION["labelQueue-".$this->queueId]);
  if($set) {
    if($key === False) { 
      $_SESSION["labelQueue-".$this->queueId][] = $id;
      $out = True;
    } else {
      $out = False;
    }
  } else {
    if($key !== False) { 
      unset($_SESSION["labelQueue-".$this->queueId][$key]);
      $out = False;
    } else {
      $out = True;
    }
  }
  $this->ss();
  return $out;
 }

 public function unmarkAll() {
  $_SESSION["labelQueue-".$this->queueId] = array();  
  $this->ss();
 }

 public function getMarked() {
  return $_SESSION["labelQueue-".$this->queueId];
 }

 public function checkMark($id) {
  return in_array($id, $_SESSION["labelQueue-".$this->queueId]);
 }

 //this system is being discontinued
 public function newEntry($data, $type = "other", $title = False) {
 
  if($title == False) {
   $title = $data["model"];
  }
 
  $this->db->insert("labelQueue", array(
   "type" => $type,
   "title" => $title,
   "data" => json_encode($data)
  ));
 }
 
 //this system is being discontinued
 public function setPrinted($id, $when = False) {
  if($when == False) {
   $when = time();
  } else {
   $when = strtotime($when);
  }
  $this->db->q("
   UPDATE `labelQueue`
      SET `tdsPrinted` = '".$this->db->e(date("Y-m-d H:i:s", $when))."'
    WHERE `id` = '".$this->db->e($id)."'
  ");
 }

 /**
 * https://www.php.net/manual/en/function.session-start.php#117157
 * Every time you call session_start(), PHP adds another
 * identical session cookie to the response header. Do this
 * enough times, and your response header becomes big enough
 * to choke the web server.
 *
 * This method clears out the duplicate session cookies. You can
 * call it after each time you've called session_start(), or call it
 * just before you send your headers.
 */
 private function clear_duplicate_cookies() {
  // If headers have already been sent, there's nothing we can do
  if (headers_sent()) {
    return;
  }

  $cookies = array();
  foreach (headers_list() as $header) {
    // Identify cookie headers
    if (strpos($header, 'Set-Cookie:') === 0) {
      $cookies[] = $header;
    }
  }
  // Removes all cookie headers, including duplicates
  header_remove('Set-Cookie');

  // Restore one copy of each cookie
  foreach(array_unique($cookies) as $cookie) {
    header($cookie, false);
  }
 }
 
 function __destruct() {
  session_write_close();
  $this->clear_duplicate_cookies();
 }
 
}
