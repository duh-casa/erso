<?php

require_once "inc/dblink.php";
require_once "interfaces/auditLogInterface.php";

class donationsInterface {

 /*

  CREATE TABLE `users` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(45) DEFAULT NULL,
    `lastname` varchar(45) DEFAULT NULL,
    `company` varchar(45) DEFAULT NULL,
    `address` varchar(45) DEFAULT NULL,
    `city` varchar(45) DEFAULT NULL,
    `birthdate` varchar(45) DEFAULT NULL,
    `email` varchar(45) DEFAULT NULL,
    `gsm` varchar(45) DEFAULT NULL,
    `status` varchar(255) DEFAULT NULL,
    `notes` text,
    `tds` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 */

 private $db;
 private $log;
 private $columns = array(
  "id" => 200,
  "name" => 45, "lastname" => 45, 
  "company" => 45,
  "address" => 45, "city" => 45,
  "birthdate" => 45,
  "email" => 45, "gsm" => 45,
  "status" => 255,
  "notes" => 65535
 );
 
 private $userCache;

 function __construct($a = False) {
  $this->db = new dblink();
  $this->log = new auditLogInterface($a);
 }

 private function conversions($item) {
  //do data type conversions here
  
  $item["code"] = strtoupper(base_convert($item["id"], 10, 32));
  return $item; 
 }

 public function details($id) { //details about this user
  $out = False;
  
  if($id > 0) {
  
   //foreach does nothing if no rows returned
   foreach($this->db->q("
    SELECT * FROM `users`
     WHERE `id` = '".$this->db->e($id)."'
     LIMIT 1
   ") as $item) { $out = $this->conversions($item); }
   
  } else {
  
   foreach($this->columns as $column => $discard) {
    $out[$column] = ""; //if new entry, clear all fields
    $out["code"] = "";
   }
   
  }
    
  return $out;
 }

 public function search($query = False, $maxResults = 100) {

  //build search conditions
  $where = array("TRUE"); //at least one element
  if($query !== False && count($query) > 0) {
   foreach(array(
    "name" => "`name`",
    "lastname" => "`lastname`",
    "company" =>  "`company`",
    "address" =>  "`address`",
    "city" =>  "`city`",
    "birthdate" =>  "`birthdate`",
    "email" =>  "`email`",
    "gsm" =>  "`gsm`",
    "status" =>  "`status`",
    "notes" =>  "`notes`",
   ) as $js => $sql) {
    if(isset($query[$js]) && $query[$js] != "") {
     $where[] = $sql." LIKE '".$this->db->e($query[$js])."'";
    }
   }
  }

  $out = array();
  foreach($this->db->q("
   SELECT * FROM `users`
    WHERE ".implode(" AND ", $where)."
    ORDER BY `tds` DESC
    LIMIT ".$this->db->e($maxResults)."
  ") as $item) { $out[] = $this->conversions($item); }

  return $out;

 }

 public function gear($userId) { //equipment recieved by the user
  return $this->log->getDonatedGear($userId);  
 }
 
 public function forms($userId) { //request forms made by the user
  return $this->log->getForms($userId);  
 }

 public function donated($gearId) {

  $tmp = $this->log->getDonatedUser($gearId);
  
  if($tmp !== False) {
      
   return $this->db->q("
    SELECT * FROM `users`
     WHERE `id` = '".$this->db->e($tmp)."'
     LIMIT 1
   ")[0];
  
  } else {
   return False;
  }
 }
 
 public function emptyUser() {
  $out = array();
  foreach($this->columns as $column => $notused) {
   $out[$column] = "";
  }
  return $out;
 }

 public function listAll() {
 
  return $this->db->q("
   SELECT * FROM `users`
   ORDER BY `lastname` ASC
  ");
 
 }
 
 public function newEntry($data, $donation = True) {
  $out = False;

  $values = array();
  foreach($this->columns as $column => $len) {
   if(isset($data[$column])) { //skip columns where data is missing -- let the database guess
    $values[$column] = $this->db->e(str_replace(array("\n", "\r"), " ", substr($data[$column], 0, $len)));
   }
  }

  $this->db->insert("users", $values);
  $out = $this->db->link->insert_id;
  
  if($donation) {  
   if(isset($data["addGearId"]) && isset($data["addGearType"])) {
    $this->log->setDonation(array("id" => $data["addGearId"], "type" => $data["addGearType"]), $out);
   }
  }
  
  return $out;
 
 }
 
 public function modifyEntry($data) {

  $set = array();
  foreach($this->columns as $column => $len) {
   if(isset($data[$column])) { //skip columns where data is missing -- let the database guess
    if(!in_array($column, array("id"))) {
     $set[] = "`".$column."` = '".$this->db->e(substr($data[$column], 0, $len))."'";
    }
   }
  }
 
  $this->db->q("
   UPDATE `users`
      SET ".implode(", ", $set)."
    WHERE `id` = '".$this->db->e($data["id"])."'
  ");

  if(isset($data["addGearId"]) && isset($data["addGearType"])) {
   $this->log->setDonation(array("id" => $data["addGearId"], "type" => $data["addGearType"]), $data["id"]);
  }
 
 }
 
 public function exists($id) {
  if(!isset($this->userCache)) {
   $this->userCache = $this->db->flatten($this->db->q("
    SELECT `id` FROM `users`
   "));
  }
  
  return in_array($id, $this->userCache);
 
 }

}
