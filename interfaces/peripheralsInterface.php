<?php

require_once "inc/dblink.php";
require_once "inc/idEncoding.php";
require_once "interfaces/auditLogInterface.php";
require_once "interfaces/labelQueueInterface.php";
require_once "inc/auth.php";

class peripheralsInterface {

 private $db;
 private $e;
 private $log;
 private $columns = array(
   "type" => 45,
   "manufacturer" => 45,
   "model" => 45,
   "serial" => 45,
   "legacyID" => 15,
   "notes" => 255,
   "enteredBy" => 45,
   "location" => 15,
   "tds" => 200
  );
 
 private $peripheralsCache;
 
 /*

   CREATE TABLE `peripherals` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `type` varchar(45) DEFAULT NULL,
    `manufacturer` varchar(45) DEFAULT NULL,
    `model` varchar(45) DEFAULT NULL,
    `serial` varchar(45) DEFAULT NULL,
    `legacyID` varchar(15) DEFAULT NULL,
    `notes` varchar(255) DEFAULT NULL,
    `location` varchar(15) NOT NULL,
    `tds` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    `enteredBy` varchar(45) DEFAULT NULL,
    PRIMARY KEY (`id`)
   ) ENGINE=InnoDB AUTO_INCREMENT=355 DEFAULT CHARSET=utf8;
   
 */

 function __construct() {
  $this->db = new dblink();
  $this->e = new idEncoding();
  $this->log = new auditLogInterface();  
 }
  
 private function conversions($item) {
  //do data type conversions here
  $status["tooltip"] = $this->log->getPrettyLog(array("gear" => $item["id"], "type" => "peripherals"));

  $s = $this->log->status(array("id" => $this->e->idEncode($item["id"]), "type" => "peripherals"))["transaction"];

  $status["text"] = array(
   "entry" => 'Nov',
   "edit" => 'Spremenjen',
   "donation" => 'Oddano',
   "return" => 'Vrnjen',
   "" => ""
  )[$s];
  
  $status["color"] = array(
   "entry" => 'success',
   "edit" => 'warning',
   "donation" => 'info',
   "return" => 'success',
   "" => ''
  )[$s];

  $item["status"][] = $status;

  $ownEntry = False;
  $entryTime = 3600;
  foreach($this->log->getRawLog(
    array("gear" => $item["id"], "type" => "peripherals")
  ) as $rawEntry) {
    if($rawEntry["ago"] < $entryTime) { $entryTime = $rawEntry["ago"]; }
    if($rawEntry["thisUser"]) { $ownEntry = true; }
  }
  if($entryTime < 60*3) {
    $item["status"][] = array(
      "tooltip" => "Pravkar spremenjen!",
      "text" => "Zdaj",
      "color" => "warning"
    );    
  } elseif($entryTime < 3600) {
    $item["status"][] = array(
      "tooltip" => "Nazadnje spremenjen pred ".floor($entryTime / 60)."min",
      "text" => floor($entryTime / 60)."min",
      "color" => "info"
    );
  }

  if($ownEntry) {
    $item["status"][] = array(
      "tooltip" => "Vpleten si v pripravi tega kosa",
      "text" => "Moj",
      "color" => "info"
    );
  }

  $item["id"] = $this->e->idEncode($item["id"]); //this is alphanumeric outside and an intiger in the database
  $item["type2"] = $item["type"];

  return $item; 
 }
 
 public function details($id) {
  $out = False;
  
  //foreach does nothing if no rows returned
  foreach($this->db->q("
   SELECT * FROM `peripherals`
    WHERE `id` = '".$this->db->e($this->e->idDecode($id))."'
    LIMIT 1
  ") as $item) { $out = $this->conversions($item); }
  
  return $out;
 }
 
 public function search($query = False, $maxResults = 100) {
 
  if(isset($query["pgLimit"])) {
    $pgLimit = (int) $query["pgLimit"];
    if($pgLimit > $maxResults) {
      $pgLimit = $maxResults;
    }
  } else {
    $pgLimit = $maxResults;
  }

  if(isset($query["pgOffset"])) {
    $pgOffset = (int) $query["pgOffset"];
  } else {
    $pgOffset = 0;
  }

  //build search conditions
  $where = array("TRUE"); //at least one element
  if($query !== False && count($query) > 0) {
   foreach(array(
    "searchType" => "`type`", 
    "searchModel" => "CONCAT(`manufacturer`,' ',`model`)",
    "searchNewID" => "`id`", 
    "searchLegacyID" => "`legacyID`",
    "searchSerial" => "`serial`", 
    "searchNotes" => "`notes`", 
    "searchLocation" => "`location`",
    "searchFrom" => "`id`",
    "searchUntil" => "`id`",
   ) as $js => $sql) {
    if(isset($query[$js]) && $query[$js] != "") {
    
     //this is alphanumeric outside and an intiger in the database
     if($js == "searchNewID") {
  	  $where[] = "CONV(".$sql.", 10, 32) LIKE '%".$this->db->e($query[$js])."%'";
     } elseif($js == "searchFrom") {
      $where[] = $sql." >= '".$this->db->e($this->e->idDecode($query[$js]))."'";
     } elseif($js == "searchUntil") {
      $where[] = $sql." <= '".$this->db->e($this->e->idDecode($query[$js]))."'";
     } else {
      $where[] = $sql." LIKE '%".$this->db->e($query[$js])."%'";
     }
     
    }
   }
  }
  
  $out = array();
  foreach($this->db->q("
   SELECT * FROM `peripherals`
    WHERE ".implode(" AND ", $where)."
    ORDER BY `tds` DESC
    LIMIT ".$this->db->e($pgLimit)." OFFSET ".$this->db->e($pgOffset)." 
  ") as $item) { $out[] = $this->conversions($item); }
  
  return $out;
 
 }
 
 public function newEntry($data) {
  
  if(isset($this->peripheralsCache) && isset($data["legacyID"]) && $data["legacyID"] != '') {
   $this->peripheralsCache[] = $data["legacyID"];
  }  
  
  $values = array();
  foreach($this->columns as $column => $len) {
   if(isset($data[$column])) { //skip columns where data is missing -- let the database guess
    $values[$column] = $this->db->e(str_replace(array("\n", "\r"), " ", substr($data[$column], 0, $len)));
   }
  }

  if (!isset($values["location"])) {
   	$a = new auth();  
    $values["location"] = $a->user["location"];
  }

  $this->db->insert("peripherals", $values);  

  if(isset($data["tds"])) { $tds = $data["tds"]; } else { $tds = False; }
  $this->log->newEntry("peripherals", $this->db->link->insert_id, $tds);
 
 }

 public function modifyEntry($data) {
   
  $set = array();
  foreach($this->columns as $column => $len) {
   if(isset($data[$column])) { //skip columns where data is missing -- let the database guess
    if(!in_array($column, array("id"))) {
     $set[] = "`".$column."` = '".$this->db->e(substr($data[$column], 0, $len))."'";
    }
   }
  }

  $decodedId = $this->e->idDecode($data["id"]);
 
  $this->db->q("
   UPDATE `peripherals`
      SET ".implode(", ", $set)."
    WHERE `id` = '".$this->db->e($decodedId)."'
  ");
  
  $this->log->modifyEntry("peripherals", $decodedId);
   
 }
 
 public function exists($legacyID) {
  if(!isset($this->peripheralsCache)) {
   $this->peripheralsCache = array();
   foreach($this->db->q("SELECT `legacyID` FROM `peripherals`") as $item) {
    $this->peripheralsCache[] = $item["legacyID"];
   }
  }
  return ($legacyID === '' || in_array($legacyID, $this->peripheralsCache));
 } 
 
 public function usernameChange($old, $new) {
  $this->db->q("
   UPDATE `peripherals`
      SET `enteredBy` = '".$this->db->e($new)."'
    WHERE `enteredBy` = '".$this->db->e($old)."' 
  ");
 }

}
