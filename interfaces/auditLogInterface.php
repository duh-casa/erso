<?php

require_once "inc/dblink.php";
require_once "inc/auth.php";
require_once "interfaces/gearInterface.php";
require_once "interfaces/formsInterface.php";

class auditLogInterface {

 /*

  CREATE TABLE `auditLog` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `type` varchar(45) DEFAULT NULL,
    `gear` int(11) DEFAULT NULL,
    `transaction` varchar(45) DEFAULT NULL,
    `enteredBy` varchar(45) DEFAULT NULL,
    `user` varchar(45) DEFAULT NULL,
    `tds` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  CREATE TABLE `state` (
    `type` varchar(45) NOT NULL,
    `gear` int(11) NOT NULL,
    `transaction` varchar(45) DEFAULT NULL,
    `tds` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`type`,`gear`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 */

 private $db;
 private $username;
 private $gear;
 private $rawLogCache = array();

 function __construct($a = False) {
  $this->db = new dblink();
  
  if($a === False) {
   $a = new auth();
  }
  
  $this->username = $a->user["username"];
  
  $this->gear = new gearInterface();
  
  //$this->maintenence();
  
 }
  
 function newEntry($gearType, $gear, $tds = False) { //gear ID is usually decoded here
  $data = array(
   "gear" => $gear,
   "type" => $gearType,
   "enteredBy" => $this->username,
   "transaction" => "entry"
  );
  
  if($tds !== False) {
   $data += array("tds" => $tds);
  }
 
  $this->db->insert("auditLog", $data); 
  $this->updateState($gearType, $gear);
 }

 function modifyEntry($gearType, $gear) { //gear ID is usually decoded here
  $this->db->insert("auditLog", array(
   "gear" => $gear,
   "type" => $gearType,
   "enteredBy" => $this->username,
   "transaction" => "edit"
  )); 
  $this->updateState($gearType, $gear);
 }

 function qcEntry($gearType, $gear, $qcStatus, $by = False, $userId = False) { //gear ID is usually decoded here
  if($by === False) {
   $by = $this->username;
  }

  $data = array(
   "gear" => $gear,
   "type" => $gearType,
   "enteredBy" => $by,
   "transaction" => "qc-".$qcStatus
  );

  if($userId !== False) {
   $data["user"] = $userId;
  }

  $this->db->insert("auditLog", $data);
  $this->updateState($gearType, $gear);
 }
 
 //get last state
 function status($gearId, $transaction = False) {  //gear ID is usually encoded here
  $where = array();

  /* This gets the last transaction of the type, but we need
     the last transaction of any type, if of matching type
  if($transaction !== False) {
   $where[] = "`transaction` = '".$this->db->e($transaction)."'";
  }
  */
 
  //"gear" is not encoded, "id" is encoded
  if(!isset($gearId["gear"])) {
    $id = $this->gear->decode($gearId)["id"];
  } else {
    $id = $gearId["gear"];
  }

  $where[] = "`gear` = '".$this->db->e($id)."'";
  $where[] = "`type` = '".$this->db->e($gearId["type"])."'";
  
  $tmp = $this->db->q("
   SELECT * FROM `auditLog`
    WHERE ".implode(" AND ", $where)."
    ORDER BY `tds` DESC
    LIMIT 1
  ");
  
  if(isset($tmp[0])) {
    if($transaction === False || $tmp[0]["transaction"] == $transaction) {
      return $tmp[0];
    } else {
      return False;
    } 
  } else {
   return False;
  }  
 }

 private function updateState($gearType, $gear) {
  //this is run on update and verifies only this entry

  $data = $this->status(array(
    "type" => $gearType,
    "gear" => $gear
  ));

  $this->db->q("
    INSERT INTO `state` (`type`, `gear`, `transaction`, `tds`)
    VALUES ('".$this->db->e($gearType)."',
            '".$this->db->e($gear)."',
            '".$this->db->e($data["transaction"])."',
            '".$this->db->e($data["tds"])."')
    ON DUPLICATE KEY UPDATE
     `transaction` = '".$this->db->e($data["transaction"])."',
     `tds` = '".$this->db->e($data["tds"])."'
  ");
 }

 public function verifyState() {
  //this is run once in a blue moon an verifies all entries
  //blue moon is at cron

  //add missing
  foreach($this->db->q("
    SELECT `auditLog`.`type`, `auditLog`.`gear` FROM `auditLog`
      LEFT JOIN `state`
      ON `auditLog`.`type` = `state`.`type` AND `auditLog`.`gear` = `state`.`gear`
      WHERE `state`.`transaction` IS NULL
      GROUP BY `auditLog`.`type`, `auditLog`.`gear`
  ") as $entry) {

    $this->updateState($entry["type"], $entry["gear"]);

  }

  //update changed
  foreach($this->db->q("
    SELECT `auditLog`.`type`, `auditLog`.`gear` FROM `auditLog`, `state`
      WHERE `auditLog`.`type` = `state`.`type`
        AND `auditLog`.`gear` = `state`.`gear`
        AND `auditLog`.`tds` > `state`.`tds`
  ") as $entry) {

    $this->updateState($entry["type"], $entry["gear"]);

  }

 }

 public function getLog($gearId) {
  $out = array();

  //"gear" is not encoded, "id" is encoded
  if(!isset($gearId["gear"])) {
    $id = $this->gear->decode($gearId)["id"];
  } else {
    $id = $gearId["gear"];
  }

  $where[] = "`gear` = '".$this->db->e($id)."'";
  $where[] = "`type` = '".$this->db->e($gearId["type"])."'";
  
  foreach($this->db->q("
   SELECT *, TIMESTAMPDIFF(SECOND, `tds`, NOW()) as `ago` FROM `auditLog`
    WHERE ".implode(" AND ", $where)."
    ORDER BY `tds` DESC
  ") as $item) {
    $out[] = $item;
  }

  return $out;

 }

 public function getPrettyLog($gearId) {

  $out = array();
  foreach($this->getRawLog($gearId) as $item) {
    $status = array(
     "entry" => 'Nov',
     "edit" => 'Spremenjen',
     "qc-working" => 'Delujoč',
     "qc-service" => 'Za servis',
     "qc-dismantle" => 'Za razgradnjo',
     "donation" => 'Oddano',
     "return" => 'Vrnjen',
     "" => ""
    )[$item["transaction"]];

    $out[] = date("j. n. Y", $item["tds"])." ".$status." (".$item["name"].")";    
  }

  return implode(",&#xA;", $out);
 }

 public function getRawLog($gearId) {
  if(isset($this->rawLogCache[$gearId])) {
    return $this->rawLogCache[$gearId];
  }

  require_once "interfaces/membersInterface.php";
  $m = new membersInterface();

  $out = array();
  foreach($this->getLog($gearId) as $item) {
    $out[] = array(
      "tds" => strtotime($item["tds"]),
      "ago" => (int) $item["ago"],
      "transaction" => $item["transaction"],
      "name" => $m->name($item["enteredBy"]),
      "thisUser" => ($item["enteredBy"] == $this->username)
    );
  }

  $this->rawLogCache[$gearId] = $out;
  return $out;
 }

 //get last state
 public function getStatistics($since = "last week") {
  $out = array();

  foreach($this->db->q("
   SELECT `type`, `transaction`, COUNT(*) AS `count` FROM `auditLog`
    WHERE `tds` > '".$this->db->e(date("Y-m-d H:i:s", strtotime($since)))."'
    GROUP BY `type`, `transaction`
  ") as $line) {
    $out[$line["transaction"]][$line["type"]] = $line["count"];
  }

  return $out;
 }

 //record a donation to a user
 function setDonation($gearId, $userId) { //gear ID is usually encoded here
  $gearId = $this->gear->decode($gearId);
 
  $this->db->insert("auditLog", array(
   "gear" => $gearId["id"],
   "type" => $gearId["type"],
   "enteredBy" => $this->username,
   "transaction" => "donation",
   "user" => $userId
  ));
  $this->updateState($gearId["type"], $gearId);
 }
 
 //get gear donated to specific user
 function getDonatedGear($userId) { 
  $out = array();
  
  if($userId > 0) {
   foreach($this->db->q("
    SELECT `type`, `gear` FROM `auditLog`
     WHERE `transaction` = 'donation'
       AND `user` = '".$this->db->e($userId)."'
     ORDER BY `tds` DESC
   ") as $item) {
    
    //do not list gear the last state of which is not donated
    if($this->status($item, "donation") !== False) {
      $out[] = $this->gear->details($item);
    }

   }
  }
  
  return $out; 
 
 }
 
 //get request forms filed by specific user
 function getForms($userId) { 
  $out = array();

  $f = new formsInterface();
  
  if($userId > 0) {
   foreach($this->db->q("
    SELECT DISTINCT `gear` FROM `auditLog`
     WHERE `type` = 'forms'
       AND `user` = '".$this->db->e($userId)."'
     ORDER BY `tds` DESC
   ") as $item) {

    $out[] = $f->details($item["gear"]);

   }
  }
  
  return $out; 
 
 }


 //get the last user (id) to which the specified gear was donated to
 function getDonatedUser($gearId) { //gear ID is usually encoded here
  $tmp = $this->status($gearId, 'donation'); //status decodes the ID itself
    
  if($tmp !== False) {
   return $tmp["user"];
  } else {
   return False;
  }

 }

 //record a return
 function setReturn($gearId) { //gear ID is usually encoded here
  $gearId = $this->gear->decode($gearId);
 
  $this->db->insert("auditLog", array(
   "gear" => $gearId["id"],
   "type" => $gearId["type"],
   "enteredBy" => $this->username,
   "transaction" => "return"
  ));
  $this->updateState($gearId["type"], $gearId);
 }

 public function usernameChange($old, $new) {
  $this->db->q("
   UPDATE `auditLog`
      SET `enteredBy` = '".$this->db->e($new)."'
    WHERE `enteredBy` = '".$this->db->e($old)."' 
  ");
 }
 
 /*
 private function maintenence() {
  $this->db->q("
   DELETE FROM `auditLog`
    WHERE `tds` < DATE_SUB(NOW(), INTERVAL 5 YEAR) 
  ");    
 }
 */

}
