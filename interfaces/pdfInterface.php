<?php

/* 

 NOTE: This script requires:
  * php-zip 
  * libreoffice
  * wkhtmltopdf
  
 This script is designed to work with OpenOffice writer documents 
 that have "[test]" type placeholders in the text, where dynamic 
 content is to be inserted.
 
 Outputs a PDF and can be used carelessly. :)

*/

require_once "pdfconfig.php";

class pdfInterface {

 private $isODT;

 private $zip;
 private $temporaryFile;
 private $temporaryName;
 private $renderPDF;
 
 private $content;
 
 //$pdf = new pdfInterface("file.odt")
 //can accept URLs
 function __construct($sourceFile = False, $renderPDF = True) {
  $this->renderPDF = $renderPDF;

  if($sourceFile !== False) {
    $this->isODT = True;
    $extension = ".odt";
  } else {
    $this->isODT = False;
    $extension = ".html";
  }

  $this->temporaryName = "/tmp/pdf-document-".uniqid();
  $this->temporaryFile = $this->temporaryName.$extension;
  $this->temporaryName = basename($this->temporaryName);

  if($this->isODT) {
    $c = new pdfconfig();
    $sourceLocation = $c->sourceLocation;
       
    copy($sourceLocation."/".$sourceFile, $this->temporaryFile);
   
    $this->zip = new ZipArchive();
    $this->zip->open($this->temporaryFile);
    
    $this->content = $this->zip->getFromName("content.xml");
  }

 }
 
 public function collectContent() {
  //only allowed if using HTML
  if(!$this->isODT) {
    $this->content = ob_get_clean();
  }
 }

 //$pdf->add("test", "works!");
 public function add($constant, $data) {
  $this->content = str_ireplace("[".$constant."]", $data, $this->content);
 }
 
 //$pdf->arrayAdd(array("test" => "works!", "message" => "boo"));
 public function arrayAdd($array) {
  foreach($array as $constant => $data) {
   $this->add($constant, $data);
  }
 }
 
 //outputs PDF file on destruct and cleans up temporary file
 function __destruct() {
  if($this->isODT) {
    $this->zip->addFromString("content.xml", $this->content);
    $this->zip->close();

    if(!$this->renderPDF && $this->isODT) {
      header("Content-Type: application/vnd.oasis.opendocument.text");
      header("Content-Transfer-Encoding: Binary"); 
      header("Content-disposition: inline; filename=izpis.odt");
      readfile($this->temporaryFile);
    } else {
      #setsebool -P httpd_unified on
      #setsebool -P httpd_tmp_exec on
      #setsebool -P httpd_execmem on

      header("Content-Type: application/pdf");
      header("Content-Transfer-Encoding: Binary"); 
      header("Content-disposition: inline; filename=izpis.pdf");
      exec("soffice --headless --writer --convert-to pdf:writer_pdf_Export ".escapeshellarg($this->temporaryFile)." --outdir /tmp");
      readfile("/tmp/".$this->temporaryName.".pdf");
      unlink("/tmp/".$this->temporaryName.".pdf");
    }
 
  } else {
    file_put_contents($this->temporaryFile, $this->content);

    header('Content-Type: application/pdf');
    header('Content-Disposition: inline; filename="izpis.pdf"');
    header('Content-Transfer-Encoding: binary');
    passthru("/usr/local/bin/wkhtmltopdf ".escapeshellarg($this->temporaryFile)." -");
  }
  unlink($this->temporaryFile);

 }
}
