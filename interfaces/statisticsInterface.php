<?php

require_once "interfaces/auditLogInterface.php";
require_once "interfaces/workHoursInterface.php";

class statisticsInterface {

 private $log;
 private $w;

 public function since($when = "last week") {
  if(!is_object($this->log)) { $this->log = new auditLogInterface(); }
  if(!is_object($this->w)) { $this->w = new workHoursInterface(); }
 
  return array(
  	"gear" => $this->log->getStatistics($when),
  	"workHours" => $this->w->getStatistics($when)
  );
 }
 
 public function computers($when = "2016-01-01") {
  if(!is_object($this->log)) { $this->log = new auditLogInterface(); }
  return $this->log->getStatistics($when);
 }

}
