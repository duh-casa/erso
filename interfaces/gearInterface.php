<?php

require_once "inc/idEncoding.php";
require_once "interfaces/computersInterface.php";
require_once "interfaces/screensInterface.php";
require_once "interfaces/peripheralsInterface.php";

class gearInterface {

 private $interfaces;
 private $e;
 
 function __construct() {
  $this->e = new idEncoding();
 }
 
 private function loadInterfaces() {
  if(!isset($this->interfaces)) {
   $this->interfaces["computers"] = new computersInterface();
   $this->interfaces["screens"] = new screensInterface();
   $this->interfaces["peripherals"] = new peripheralsInterface();
  }
 }

 function details($item) {
 
  $this->loadInterfaces();

  if(!isset($item["gear"])) { $item["gear"] = $item["id"]; }

  $out = $this->interfaces[$item["type"]]->details($this->e->idEncode($item["gear"]));
  $out["type"] = $item["type"];
  
  return $out;
 
 }

 function search($query, $maxResults) {

  $this->loadInterfaces();

  return $this->interfaces[$query["type"]]->search($query, $maxResults);

 }

 function readCode($code) {
  $code = trim($code);
  $codeSegment = substr($code, 0, 2);
  if(in_array($codeSegment, array("c-", "s-", "p-"))) {
 
   return $this->details($this->decode(array(
    "id" => substr($code, 2),
    "type" => array(
     "c-" => "computers",
     "s-" => "screens",
     "p-" => "peripherals"
    )[$codeSegment]
   )));

  } else {
   return False;
  }
 }
 
 function decode($item) {
  $item["id"] = $this->e->idDecode($item["id"]);
  return $item;
 }

}
