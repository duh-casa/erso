<?php

require_once "inc/dblink.php";
require_once "interfaces/membersInterface.php";
require_once "pdfconfig.php";

class workHoursInterface {

 private $db;
 private $m;
 private $activeMembersCache = array();

 /*
 
   CREATE TABLE `workHours` (
     `username` varchar(45) NOT NULL,
     `from` datetime NOT NULL,
     `until` datetime DEFAULT NULL,
     `workType` int(3) DEFAULT '0',
     PRIMARY KEY (`username`,`from`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   
 */

 function __construct($m = False) {
  $this->db = new dblink();
  
  $this->m = $m;
  if($this->m === False) {
   $this->m = new membersInterface();
  }
  
 }

 //trim username
 private function tU($u) {
  if($u !== False) {
   return substr($u, 0, 45); //45 for VARCHAR(45)
  } else {
   return False;
  }
 }
  
 private function conversions($item) {
  //do data type conversions here
  $minutes = (strtotime($item["until"]) - strtotime($item["from"])) / 60;
  $item["interval"] = floor($minutes / 60).":".str_pad($minutes % 60, 2, "0", STR_PAD_LEFT);
  $item["name"] = $this->m->name($item["username"]);
  $item["location"] = $this->m->getCurrentLocation($item["username"]);

  $item["intervalMinutes"] = $minutes;

  $item["code"] = $item["username"]; //TODO

  return $item; 
 }
 
 public function details($username, $from) { //warning: $from is in unix format
  $username = $this->tU($username);
 
  $out = False;
  
  //foreach does nothing if no rows returned
  foreach($this->db->q("
   SELECT * FROM `workHours`
    WHERE `username` = '".$this->db->e($username)."' AND `from` = '".$this->db->e(date("Y-m-d H:i:s", $from))."'
    LIMIT 1
  ") as $item) { $out = $this->conversions($item); }
  
  return $out;  
 }
 
 public function search($query = False, $maxResults = 10000) {
 
  if(isset($query["pgLimit"])) {
    $pgLimit = (int) $query["pgLimit"];
    if($pgLimit > $maxResults) {
      $pgLimit = $maxResults;
    }
  } else {
    $pgLimit = $maxResults;
  }

  if(isset($query["pgOffset"])) {
    $pgOffset = (int) $query["pgOffset"];
  } else {
    $pgOffset = 0;
  }

  //build search conditions
  $where = array("TRUE"); //at least one element
  if($query !== False && count($query) > 0) {
  
   if(isset($query["searchName"]) && $query["searchName"] != "") {  
    $where[] = "`username` IN ('".implode("','", $this->m->usernamesMatchingName($query["searchName"]))."')";  
   }
   
   if(isset($query["searchDate"]) && $query["searchDate"] != "") {
    $where[] = "DATE(`from`) = '".date("Y-m-d", strtotime(str_replace(" ", "", $query["searchDate"])))."'";
   }

   if(isset($query["username"]) && $query["username"] != "") {
    $where[] = "`username` = '".$this->db->e($query["username"])."'";
   }

   if(isset($query["workType"]) && $query["workType"] != "") {
    $where[] = "`workType` = '".$this->db->e($query["workType"])."'";
   }

   //Note: Search is exclusive
   if((isset($query["searchFrom"]) && $query["searchUntil"] != "") && (isset($query["searchUntil"]) && $query["searchUntil"] != "")) {
    $tmpFrom = date("Y-m-d", strtotime($query["searchFrom"]))." 00:00:00";
    $tmpUntil = date("Y-m-d", strtotime($query["searchUntil"]))." 00:00:00";
    $tmpUntil = date("Y-m-d H:i:s", strtotime("-1 second", strtotime($tmpUntil))); //edge case handling
    $where[] = "`from` BETWEEN '".$this->db->e($tmpFrom)."' AND '".$this->db->e($tmpUntil)."'";
   }
  } 

  $out = array();
  foreach($this->db->q("
   SELECT * FROM `workHours`
    WHERE ".implode(" AND ", $where)."
    ORDER BY `from` DESC
    LIMIT ".$this->db->e($pgLimit)." OFFSET ".$this->db->e($pgOffset)."
  ") as $item) { $out[] = $this->conversions($item); }

  return $out;
  
 }

 public function getStatistics($since = "last week") {
  $totalMinutes = 0;
  $users = array();

  foreach($this->search(array(
    "searchFrom" => $since,
    "searchUntil" => date("Y-m-d")
  )) as $item) {
    if($item["intervalMinutes"] > 5) {
      $totalMinutes += $item["intervalMinutes"];
      $users[$item["username"]] = true;
    }
  }

  return array(
    "workHours" => round($totalMinutes / 60),
    "users" => count($users)
  );
 }

 public function getHours($from = False, $until = False, $workType = False) {
  if($from === False) {
    $from = date("Y")."-01-01";
  }
  if($until === False) {
    $until = date("Y")."-12-31";
  }

  $searchCriteria = array(
    "searchFrom" => $from,
    "searchUntil" => $until
  );

  if($workType !== False) {
    $searchCriteria["workType"] = $workType;
  }

  $totalMinutes = 0;
  foreach($this->search($searchCriteria) as $item) {
    //only count minutes where more than 5 minutes is recorded
    if($item["intervalMinutes"] > 5) {
      $totalMinutes += $item["intervalMinutes"];
    }
  }

  return round($totalMinutes / 60);
 }
 
 public function modifyEntry($data) { //warning: originalFrom is in unix format
  return $this->db->q("
   UPDATE `workHours`
      SET `from` = '".$this->db->e(date("Y-m-d H:i:s", strtotime($data["from"])))."', `until` = '".$this->db->e(date("Y-m-d H:i:s", strtotime($data["until"])))."',
        `workType` = '".$this->db->e($data["workType"])."'
    WHERE `username` = '".$this->db->e($data["username"])."' AND `from` = '".$this->db->e(date("Y-m-d H:i:s", $data["originalFrom"]))."'
  ");
 }
 
 public function logon($code, $time = False, $workType = 0) {
 
  //this is okay because resolveCode is just validation
  $username = $this->resolveCode($code);

  if($username !== False) {
    if($time === False) {
     $from = $this->db->e(date("Y-m-d H:i:s"));
     $until = $this->db->e(date("Y-m-d H:i:s", strtotime("+1 day")));
    } else {
     $from = $this->db->e(date("Y-m-d H:i:s", strtotime($time)));
     $until = $this->db->e(date("Y-m-d H:i:s", strtotime("+1 day", strtotime($time))));
    }

    $this->db->insert("workHours", array(
     "username" => $username,
     "from" => $from,
     "until" => $until,
     "workType" => $workType
    ));
  }
 
 }
 
 private function lastLogon($username) {
  $username = $this->tU($username);
 
  $tmp = $this->db->q("
   SELECT `from` FROM `workHours`
    WHERE `username` = '".$this->db->e($username)."'
    ORDER BY `from` DESC
    LIMIT 1
  ");
  
  if(isset($tmp[0])) {
   return strtotime($tmp[0]["from"]);
  } else {
   return False;
  }
  
 }

 public function logoff($code, $time = False, $workType = 0) {
 
  //this is okay because resolveCode is just validation
  $username = $this->resolveCode($code);
  
  if($username !== False) {
    if($time === False) {
     $until = $this->db->e(date("Y-m-d H:i:s"));
    } else {
     $until = $this->db->e(date("Y-m-d H:i:s", strtotime($time)));
    }

    $from = $this->lastLogon($username);
    $this->modifyEntry(array(
     "username" => $username,
     "from" => date("Y-m-d H:i:s", $from),
     "originalFrom" => $from,
     "until" => $until,
     "workType" => $workType
    ));
  }
 
 }

 public function getWorkType($code) {

  $username = $this->resolveCode(trim($code));

  if($username !== false) {

    if($this->present($username)) {

      //if present return entered workType
      return $this->getEnteredWorkType($username);

    } else {

      //if not present, return user's default workType
      $tmp = $this->m->details($username);
      if($tmp !== False && isset($tmp["workType"])) {
       return $tmp["workType"];
      } else {
        return 0;
      }

    }

  } else {
    //user not found, go with defaults
    return 0;
  }

 }

 public function import($data) {
  $this->db->insert("workHours", $data);
 }
 
 //This used to be planned for code resolution
 //but is now just validation
 public function resolveCode($code) {
  if(strlen($code) > 0) {
    if($this->m->exists($code)) {
      return $code;
    } else {
      return False;
    }
  } else {
    return False;
  }
 }

 public function lookupCode($username) {
  $username = $this->tU($username);
 
  return $username;
 }
 
 public function present($username, $time = False) {
  $username = $this->tU($username);
 
  if($time === False) {
   $time = $this->db->e(date("Y-m-d H:i:s"));
  } else {
   $time = $this->db->e(date("Y-m-d H:i:s", strtotime($time)));
  }
  
  $tmp = $this->db->q("
   SELECT COUNT(*) AS `members` FROM `workHours`
    WHERE `username` = '".$this->db->e($username)."'
      AND `from` <= '".$time."'
      AND `until` >= '".$time."'
  ");
  
  return ($tmp[0]["members"] > 0);
 }

 private function getEnteredWorkType($username) {
  $username = $this->tU($username);
  
  $tmp = $this->db->q("
   SELECT `workType` FROM `workHours`
    WHERE `username` = '".$this->db->e($username)."'
    ORDER BY `from` DESC
    LIMIT 1
  ");
  
  if(isset($tmp[0])) {
    return $tmp[0]["workType"];
  } else {
    return 0;
  }
 }

 public function allPresent($time = False) {
  if($time === False) {
   $time = $this->db->e(date("Y-m-d H:i:s"));
  } else {
   $time = $this->db->e(date("Y-m-d H:i:s", strtotime($time)));
  }
  
  $out = array();
  foreach($this->db->q("
   SELECT * FROM `workHours`
    WHERE `from` <= '".$time."'
      AND `until` > '".$time."'
    ORDER BY `from` ASC
  ") as $item) { $out[] = $this->conversions($item); }
  
  return $out;
 }

 public function organizationData($jsonName = "organization.json") {

  $c = new pdfconfig();
  $jsonPath = $c->sourceLocation."/".$jsonName;

  if(file_exists($jsonPath)) {
    return json_decode(file_get_contents($jsonPath), true);
  } else {
    return array();
  }
 }

 private function membersActiveInYear($year = False) {
  if(isset($this->activeMembersCache[$year])) {
    return $this->activeMembersCache[$year];
  }

  if($year === False) { $year = date("Y"); }
  $tmp = array();
  foreach($this->search(array(
    "searchFrom" => $year."-01-01",
    "searchUntil" => $year."-12-31"
  ), 10000) as $item) {

   //if active for more than 5 minutes per entry
   if((float) $item["intervalMinutes"] > 5) {
    $tmp[(int) $item["username"]] = true;
   }
  }

  unset($tmp[0]); //invalid entries

  $out = array();
  foreach($tmp as $username => $discard) {
    $out[] = $username;
  }

  $this->activeMembersCache[$year] = $out;
  return $out;
 }

 public function activeMembersByGenderAge($year = False, $gender = "m", $ageFrom = 0, $ageUntil = 18, $ageOn = False) {

  //only calculate for active members
  $activeMembers = $this->membersActiveInYear($year);

  //calculate age on this date if provided
  if($ageOn !== False) {
    $this->m->ageOn = strtotime($ageOn);
  }

  return $this->m->membersByGenderAge($gender, $activeMembers, $ageFrom, $ageUntil);
 }

 public function usernameChange($old, $new) {
  $this->db->q("
   UPDATE `workHours`
      SET `username` = '".$this->db->e($new)."'
    WHERE `username` = '".$this->db->e($old)."' 
  ");
 }
  
}
