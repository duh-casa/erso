<?php

require_once "inc/dblink.php";

class legacyInterface {

 private $db;
 
 function __construct() {
  $this->db = new dblink("ersoold");
 }
 
 public function computersAll() {
  return $this->db->q("
   SELECT * 
     FROM `t_oprema` 
    WHERE `id_vrsta` IN ('1', '2', '10')
  ");  
//    ORDER BY `id_oprema` DESC
//    LIMIT 10
 }
 
 public function computersTransform($i, $out = array()) {
  foreach(array(
   "stevilka" => "legacyID",
   "id_uporabnik" => "enteredBy",
   "velja_od" => "tds" 
  ) as $from => $to) { 
   $out[$to] = $i[$from]; 
  }
  
  $out["type"] = $this->computerType($i);
  $out["cpuSpeed"] = $this->computerCpuSpeed($i);
  $out["cpuModel"] = $this->computerCpuModel($i);
  $out["model"] = $this->computerModel($i);
  $qc = $this->computerQC($i);
  if($qc !== False) {
   $out["qcBy"] = $qc;
  }
  $graphics = $this->computerGraphics($i);
  
  if($graphics !== False) {
   $out["graphics"] = $graphics;
  }
  
  $disk = $this->computerDisk($i);
  if($disk !== False) {
   $out["diskModel"] = $disk["model"];
   $out["diskSerial"] = $disk["serial"];
   $out["diskManufacturer"] = $disk["manufacturer"];
   $out["diskSize"] = $disk["size"]." GB";
  }
  
  $ram = $this->computerRAM($i);
  if($ram !== False) {
   $out["ramSpeed"] = $ram["model"];
   $out["ramSize"] = (round($ram["size"] / 10.24) / 100)." GB";
  }
 
  return $out;
  
 }
 
 function computerType($i) {
 
  if($i["id_vrsta"] == 1) {
   return "desktop";
  } elseif($i["id_vrsta"] == 2) {
   return "laptop";
  } elseif($i["id_vrsta"] == 10) {
   return "server";
  } else {
   return "other";
  }
  
 }
 
 function computerCpuSpeed($i) {
  return $i["hitrost"]." MHz";
 }

 function computerCpuModel($i) {
  return $this->db->q("
   SELECT CONCAT(`sif_oprema_tip`.`naziv`, ' ', `t_oprema`.`model`) AS `model`
     FROM `t_oprema`, `sif_oprema_tip`
    WHERE `t_oprema`.`id_oprema` = '".$this->db->e($i["id_oprema"])."'
      AND `sif_oprema_tip`.`id_tip` = `t_oprema`.`id_tip`
  ")[0]["model"];
 }
 
 function computerModel($i) {
 
  if(isset($i["opomba"]) && $i["opomba"] != '') {
   return str_replace("product: ", "", str_replace(array("\xC3\x84", "\xC3\x85", "\xC2\x8D", "\xC2\xBE"), "", utf8_encode($i["opomba"])));
  } else {
   return $i["model"];
  }
 
 }
 
 function computerQC($i) {
  $out = $this->db->q("
   SELECT `id_pregledal` FROM `rel_oprema_podrobno`
    WHERE `id_oprema` = '".$this->db->e($i["id_oprema"])."'
    LIMIT 1
  ");
  if(isset($out[0])) {
   return $out[0]["id_pregledal"];
  } else {
   return False;
  }
  
 }
 
 function computerGraphics($i) {
  $out = $this->db->q("
   SELECT CONCAT(`sif_oprema_tip`.`naziv`, ' ', `t_oprema`.`model`) AS `model`
     FROM `t_oprema`, `sif_oprema_tip`, `rel_oprema_podrobno`
    WHERE `t_oprema`.`id_vrsta` = '8'
      AND `rel_oprema_podrobno`.`id_povezava` = '".$this->db->e($i["id_oprema"])."' 
      AND `t_oprema`.`id_oprema` = `rel_oprema_podrobno`.`id_oprema`
      AND `sif_oprema_tip`.`id_tip` = `t_oprema`.`id_tip`
  ");
  if(isset($out[0])) {
   return $out[0]["model"];
  } else {
   return False;
  }
 }
 
 function computerDisk($i) {
  $out = $this->db->q("
   SELECT CONCAT(`sif_oprema_proizvajalec`.`naziv`, ' ', `t_oprema`.`model`) AS `model`, 
          `t_oprema`.`serijska` AS `serial`,
          `sif_oprema_proizvajalec`.`naziv` AS `manufacturer`,
          `t_oprema`.`kapaciteta` AS `size`
     FROM `t_oprema`, `sif_oprema_proizvajalec`, `rel_oprema_podrobno`
    WHERE `t_oprema`.`id_vrsta` = '6'
      AND `rel_oprema_podrobno`.`id_povezava` = '".$this->db->e($i["id_oprema"])."' 
      AND `t_oprema`.`id_oprema` = `rel_oprema_podrobno`.`id_oprema`
      AND `sif_oprema_proizvajalec`.`id_proizvajalec` = `t_oprema`.`id_proizvajalec`
  ");
  
  if(isset($out[0])) {
   return $out[0];
  } else {
   return False;
  }
 }

 function computerRAM($i) {
  $out = $this->db->q("
   SELECT `t_oprema`.`id_tip` AS `id_tip`,
          `t_oprema`.`hitrost` AS `hitrost`,
          `t_oprema`.`kapaciteta` AS `size`
     FROM `t_oprema`, `rel_oprema_podrobno`
    WHERE `t_oprema`.`id_vrsta` = '7'
      AND `rel_oprema_podrobno`.`id_povezava` = '".$this->db->e($i["id_oprema"])."' 
      AND `t_oprema`.`id_oprema` = `rel_oprema_podrobno`.`id_oprema`
  ");
    
  if(isset($out[0])) {
   $out = $out[0];
  
   if($out["id_tip"] == 13) {
    $out["model"] = "SDRAM";
   } elseif($out["id_tip"] == 14) {
    $out["model"] = "DDR1 ";
   } elseif($out["id_tip"] == 15) {
    $out["model"] = "DDR2 ";
   } elseif($out["id_tip"] == 16) {
    $out["model"] = "DDR3 ";
   }
     
   if(isset($out["hitrost"]) && $out["hitrost"] != '') {
    $out["model"] .= $out["hitrost"]." MHz";
   }

   return $out;

  } else {
   
   return False;
   
  }
  
 }



 public function screensAll() {
  return $this->db->q("
   SELECT * 
     FROM `t_oprema` 
    WHERE `id_vrsta` = '3'
  ");  
//    ORDER BY `id_oprema` DESC
//    LIMIT 10
 }

 public function screensTransform($i, $out = array()) {
  foreach(array(
   "stevilka" => "legacyID",
   "serijska" => "serial",
   "kapaciteta" => "size",
   "opomba" => "notes",
   "id_uporabnik" => "enteredBy",
   "velja_od" => "tds" 
  ) as $from => $to) { 
   $out[$to] = $i[$from]; 
  }
  
  $out["model"] = $this->screenModel($i);
  $out["manufacturer"] = $this->screenManufacturer($i);

  return $out;
 }
 
 private function screenModel($i) {
  $out = "";
  foreach($this->db->q("
   SELECT `naziv`
     FROM `sif_oprema_tip`
    WHERE `id_tip` = '".$this->db->e($i["id_tip"])."'
  ") as $tmp) {
   $out = trim($tmp["naziv"]);
  }
  
  return trim($out." ".$i["model"]);
 }

 private function screenManufacturer($i) {
  $out = False;
  foreach($this->db->q("
   SELECT `naziv` FROM `sif_oprema_proizvajalec`
    WHERE `id_proizvajalec` = '".$this->db->e($i["id_proizvajalec"])."'
  ") as $tmp) {
   $out = $tmp["naziv"];
  }
  
  return $out;
 }




 public function peripheralsAll() {
  return $this->db->q("
   SELECT * 
     FROM `t_oprema` 
    WHERE `id_vrsta` IN ('4', '5', '11', '12')
  ");  
//    ORDER BY `id_oprema` DESC
//    LIMIT 10
 }

 public function peripheralsTransform($i, $out = array()) {
  foreach(array(
   "stevilka" => "legacyID",
   "serijska" => "serial",
   "opomba" => "notes",
   "id_uporabnik" => "enteredBy",
   "velja_od" => "tds" 
  ) as $from => $to) { 
   $out[$to] = $i[$from]; 
  }
  
  $out["type"] = $this->peripheralsType($i);
  $out["manufacturer"] = $this->screenManufacturer($i);
  $out["model"] = $this->screenModel($i);

  return $out;  
 }

 private function peripheralsType($i) {
  $out = False;
  foreach($this->db->q("
   SELECT `naziv` FROM `sif_oprema_vrsta`
    WHERE `id_vrsta` = '".$this->db->e($i["id_vrsta"])."'
  ") as $tmp) {
   $out = $tmp["naziv"];
  }
  
  return $out;
 }

 
 public function membersAll() {
  return $this->db->q("SELECT * FROM `t_uporabnik`");
 }
 
 public function membersTransform($i, $out = array()) {  
  foreach(array(
   "id_uporabnik" => "username",
   "ime" => "name",
   "priimek" => "lastname",
   "naslov" => "address",
   "telefon" => "gsm",
   "eposta" => "email",
   "datum_rojstva" => "birthdate",
   "davcna" => "vat",
   "izobrazba" => "education",
   "trr" => "sepa",
   "opomba" => "notes",
   "velja_od" => "tds" 
  ) as $from => $to) { 
   $out[$to] = $i[$from]; 
  }

  $out["city"] = $this->memberCity($i);
  $out["gender"] = $this->memberGender($i);
  $out["career"] = $this->memberCareer($i);
  $out["subsidiary"] = $this->memberSubsidiary($i);
  $out["acl"] = $this->memberACL($i);
  
  $out["status"] = $this->memberStatus($i);
  $out["mentor"] = $this->memberMentor($i);
   
  $out["password"] = "qqq"; //import default password

  return $out;
 }
 
 public function memberCity($i) {
  $out = False;
  foreach($this->db->q("
   SELECT `naziv` FROM `sif_posta`
    WHERE `id_posta` = '".$this->db->e($i["id_posta"])."'
  ") as $tmp) {
   $out = $i["id_posta"]." ".$tmp["naziv"];
  }
  return $out;
 }
 
 public function memberGender($i) {
  if($i["spol"] == 1) {
   return "moški";
  } elseif($i["spol"] == 0) {
   return "ženski";
  } else {
   return False;
  }
 }
 
 public function memberCareer($i) {
  $out = False;
  foreach($this->db->q("
   SELECT `naziv` FROM `sif_uporabnik_kariera`
    WHERE `id_kariera` = '".$this->db->e($i["id_kariera"])."'
  ") as $tmp) {
   $out = $tmp["naziv"];
  }
  return $out;
 }

 public function memberSubsidiary($i) {
  $out = False;
  foreach($this->db->q("
   SELECT `naziv` FROM `sif_podruznica`
    WHERE `id_podruznica` = '".$this->db->e($i["id_podruznica"])."'
  ") as $tmp) {
   $out = $tmp["naziv"];
  }
  return $out;
 }
 
 public function memberACL($i) {
  $out = False;
  foreach($this->db->q("
   SELECT `naziv` FROM `sif_uporabnik_pravica`
    WHERE `id_pravica` = '".$this->db->e($i["id_pravica"])."'
  ") as $tmp) {
   $out = array(
    "Administrator" => "admin",
    "Uporabnik - oddaja" => "donation",
    "Uporabnik - vnos" => "entry",
    "Uporabnik - kontrola" => "qc",
    "Terminal - prijava" => ""
   )[$tmp["naziv"]];
  }
  return $out;
 }

 public function memberStatus($i) {
  $out = False;
  foreach($this->db->q("
   SELECT `sif_oseba_zaposlitev_status`.`naziv` AS `naziv` FROM `rel_uporabnik_prostovoljstvo`, `sif_oseba_zaposlitev_status`
    WHERE `rel_uporabnik_prostovoljstvo`.`id_uporabnik` = '".$this->db->e($i["id_uporabnik"])."'
      AND `sif_oseba_zaposlitev_status`.`id_zaposlitev_status` = `rel_uporabnik_prostovoljstvo`.`id_zaposlitev_status`
    LIMIT 1
  ") as $tmp) {
   $out = $tmp["naziv"];
  }
  return $out; 
 }

 public function memberMentor($i) {
  $out = False;
  foreach($this->db->q("
   SELECT `id_mentor` FROM `rel_uporabnik_prostovoljstvo`
    WHERE `id_uporabnik` = '".$this->db->e($i["id_uporabnik"])."'
    LIMIT 1
  ") as $tmp) {
   $out = $tmp["id_mentor"];
  }
  return $out; 
 }
 
 
 public function workHoursAll() {
  return $this->db->q("SELECT * FROM `rel_uporabnik_prijava`");
 }
 
 public function workHoursTransform($i, $out = array()) {  
  foreach(array(
   "id_uporabnik" => "username",
   "velja_od" => "from", 
   "velja_do" => "until"
  ) as $from => $to) { 
   $out[$to] = $i[$from]; 
  }

  return $out;
 } 



 public function usersAll() {
  return $this->db->q("SELECT * FROM `t_klient`");
 }
 
 public function usersTransform($i, $out = array()) {  
  foreach(array(
   "id_klient" => "id",
   "ime" => "name", 
   "priimek" => "lastname",
   "naslov" => "address",
   "telefon" => "gsm",
   "eposta" => "email",
   "datum_rojstva" => "birthdate",
   "naziv" => "company",
   "opomba" => "notes",
   "velja_od" => "tds"
  ) as $from => $to) { 
   $out[$to] = $i[$from]; 
  }
  
  $out["city"] = $this->memberCity($i);
  $out["status"] = $this->userStatus($i);

  return $out;
 }
  
 private function userStatus($i) {
  $out = False;
  foreach($this->db->q("
   SELECT `naziv` AS `naziv` FROM `sif_oseba_zaposlitev_status`
    WHERE `id_zaposlitev_status` = '".$this->db->e($i["id_zaposlitev_status"])."'
    LIMIT 1
  ") as $tmp) {
   $out = $tmp["naziv"];
  }
  return $out; 
 } 

}
