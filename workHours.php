<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewWorkHours") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Prisotnost (ure)", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));


require_once "interfaces/workHoursInterface.php";
$w = new workHoursInterface();

$document->add("header", array("auth" => $a));

?><h2>Prisotnost (ure)</h2>

<a href="workHoursTerminal.php" target="_blank" class="btn btn-default">
 <span class="glyphicon glyphicon-barcode" aria-hidden="true"></span> Terminal za prijavo</a>
<?php if($a->verify(False, "viewWorkHoursReport") !== False) { ?>
<a href="workHoursReport.php" target="_blank" class="btn btn-default">
 <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Letno poročilo</a>
<?php } ?>

<?php ob_start(); ?>
<script>
 $.ajaxSetup({ cache: false });
 const bc = new BroadcastChannel("erso-workHours");
 bc.onmessage = function (ev) { refreshAjax(); } 

 window.pgLimit = 10;
 window.pgOffset = 0;

 function appendAjax() {
  $.ajax({
   data: {
    o: "workHours",
    q: {
     searchName : $("#searchName").val(),
     searchDate : $("#searchDate").val(),
     pgLimit: window.pgLimit,
     pgOffset: window.pgOffset
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#rows").append(result.html);
   }
  }); 
 }

 function refreshAjax() {
  window.pgOffset = 0;
  $("#rows").empty();
  appendAjax();
 }

 $('input.search').change(function () {
  refreshAjax();
 });

 $(document).ready(function() {
  refreshAjax();
 });

 $(window).scroll(function() {
  if($(window).scrollTop() + $(window).height() == $(document).height()) {
   window.pgOffset = window.pgOffset + window.pgLimit;
   appendAjax();
  }
 });

</script>
<?php $document->addJS(ob_get_clean()); ?>

<table class="table table-striped">
 <thead>
  <tr>
   <th>Ime in Priimek</th>
   <th>Od</th>
   <th>Do</th>
   <th>Čas</th>
   <th></th>
  </tr>
  <tr>
   <td><input type="text" class="form-control search" id="searchName"></td>
   <td colspan="2"><input type="text" class="form-control search" id="searchDate" placeholder="<?php echo date("d. m. Y"); ?>"></td>
   <td></td>
   <td><a href="javascript:refreshAjax();" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></td>
  </tr>
 </thead>
 <tbody id="rows">
 </tbody>
 <tfoot>
  <tr>
   <td></td>
   <td colspan="2"></td>
   <td></td>
   <td><a href="javascript:window.pgOffset=window.pgOffset+window.pgLimit; appendAjax();" class="btn btn-info"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Naloži več</a></td>
  </tr>   
 </tfoot>
</table>
