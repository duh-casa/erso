<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "editMembers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Nov član", array(
 "bootstrap" => True,
 "chosen" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

$acls = array(
 "viewComputers" => "Vidi računalnike",
 "editComputers" => "Vnaša računalnike",
 "viewMembers" => "Vidi podatke članov",
 "editMembers" => "Vnaša podatke članov",
 "donations" => "Oddaja računalnike",
 "viewWorkHours" => "Vidi podatke prisotnosti",
 "editWorkHours" => "Vnaša podatke prisotnosti",
 "qc" => "Izvaja kontrolo",
 "editUsername" => "Spreminja uporabniška imena",
 "editPasswords" => "Spreminja gesla drugih",
 "admin" => "Administrator (dostop do vsega)",
);

$displayForm = True;
if($_SERVER['REQUEST_METHOD'] === 'POST') {

 require_once "interfaces/membersInterface.php";
 $m = new membersInterface($a);
 $data = $_POST;
 $data["acl"] = implode(" ", $data["acls"]);
 $data["password"] = "qqq"; //default password, can be changed later
 $m->newEntry($data);
 header("Location: members.php", true, 307);

}

$document->add("header", array("auth" => $a));

?>
<h2>Člani</h2>

<h3>Nov vnos</h3>
<form method="POST">
 <table class="table">
  <tbody>
   <tr>
    <th style="width: 20%;">Uporabniško ime<br><small>(obvezno)</small></th>
    <td><input type="text" class="form-control" name="username" placeholder="99" required></td>
   </tr>
   <tr>
    <th>Ime<br><small>(obvezno)</small></th>
    <td><input type="text" class="form-control" name="name" placeholder="Janez" required></td>
   </tr>
   <tr>
    <th>Priimek<br><small>(obvezno)</small></th>
    <td><input type="text" class="form-control" name="lastname" placeholder="Sklopka" required></td>
   </tr>
   <tr>
    <th>Ulica<br><small>(obvezno)</small></th>
    <td><input type="text" class="form-control" name="address" placeholder="Napačna ulica 13" required></td>
   </tr>
   <tr>
    <th>Mesto<br><small>(obvezno)</small></th>
    <td><input type="text" class="form-control" name="city" placeholder="1234 Neka poljana" required></td>
   </tr>
   <tr>
    <th>Spol</th>
    <td><input type="text" class="form-control" name="gender" placeholder="moški"></td>
   </tr>
   <tr>
    <th>eMail</th>
    <td><input type="text" class="form-control" name="email" placeholder="janez.sklopka@gmail.com"></td>
   </tr>
   <tr>
    <th>GSM</th>
    <td><input type="text" class="form-control" name="gsm" placeholder="040 123 456"></td>
   </tr>
   <tr>
    <th>Rojstni datum<br><small>(obvezno)</small></th>
    <td><input type="text" class="form-control" name="birthdate" placeholder="1970-04-01" required></td>
   </tr>
   <tr>
    <th>Podružnica</th>
    <td><input type="text" class="form-control" name="subsidiary" placeholder="LJRSO" value="LJRSO"></td>
   </tr>
   <tr>
    <th>Dostop</th>
    <td>
     <select multiple class="form-control" name="acls[]">
      <?php foreach($acls as $acl => $title) { ?>
       <option value="<?php echo $acl; ?>"><?php echo $title; ?></option>
      <?php } ?>
     </select>
    </td>
   </tr>
   <tr>
    <th>Mentor</th>
    <td><input type="text" class="form-control" name="mentor" placeholder="25"></td>
   </tr>
   <tr>
    <th>Davčna</th>
    <td><input type="text" class="form-control" name="vat" placeholder="12345678"></td>
   </tr>
   <tr>
    <th>Bančni račun</th>
    <td><input type="text" class="form-control" name="sepa" placeholder="SI56 ..."></td>
   </tr>
   <tr>
    <th>Izobrazba</th>
    <td><input type="text" class="form-control" name="education" placeholder="Gimnazijski maturant"></td>
   </tr>
   <tr>
    <th>Kariera</th>
    <td><input type="text" class="form-control" name="career" placeholder="Guru"></td>
   </tr>
   <tr>
    <th>Status</th>
    <td><input type="text" class="form-control" name="status" placeholder="Brezposeln"></td>
   </tr>
   <tr>
    <th>Opombe</th>
    <td><input type="text" class="form-control" name="notes" placeholder="Ljubitelj korenja"></td>
   </tr>
  </tbody>
  <tfoot>
   <tr>
    <td></td>
    <td>
     <button type="submit" class="btn btn-primary">
      <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Shrani
     </button>
    </td>
   </tr>
  </tfoot>
 </table>
</form>
