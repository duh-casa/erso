<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "qc") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Kontrola", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

require_once "interfaces/computersInterface.php";
$c = new computersInterface();


if($_SERVER['REQUEST_METHOD'] === 'POST') {

 $c->updateStatus($_POST["q"], $_POST["status"], $a->user["username"]);
 
 ?><script>
  const bc = new BroadcastChannel("erso-computers");
  bc.postMessage("reload");
  <?php if (http_response_code() == 200) { ?>window.close();<?php } ?>
 </script><?php

 
}

if(isset($_REQUEST["q"])) { //please don't mismatch GET and POST requests
 
 $i = $c->details($_REQUEST["q"]);
 
 if(is_null($i["status"])) {
 
  $statusColor = "qc";
  $statusText = "Kontrola";
 
 } else {
 
  if($i["status"] == "qc-working") {
   $status = "Delujoče";
  } elseif($i["status"] == "qc-service") {
   $status = "Servis";
  } elseif($i["status"] == "qc-dismantle") {
   $status = "Zanič";
  } else {
   $status = $i["status"];
  }
 }

 $document->add("header", array("auth" => $a));
 ?><h2>Oprema</h2>
 <h3>Kontrola</h3>
 <table class="table">
  <tbody>
   <tr>
    <td>
     <table class="table table-striped">
      <tbody>
       <tr>
        <th>Trenutno stanje</th>
        <td>
          <h3><label class="label label-big label-<?php echo $status[0]["color"]; ?>">
            <?php echo $status[0]["text"]; ?>
          </label></h3>
        </td>
       </tr>  
       <tr>
        <th>Tip</th>
        <td><?php echo $i["type"]; ?></td>
       </tr>  
       <tr>
        <th>Model</th>
        <td><?php echo $i["model"]; ?></td>
       </tr>  
       <tr>
        <th>Oznaka</th>
        <td><?php echo $i["id"]; ?></td>
       </tr>  
       <tr>
        <th>Stara oznaka</th>
        <td><?php echo $i["legacyID"]; ?></td>
       </tr>  
       <tr>
        <th>Grafična</th>
        <td><?php echo $i["graphics"]; ?></td>
       </tr>  
       <tr>
        <th>CPU</th>
        <td><?php echo trim($i["cpuModel"]." ".$i["cpuSpeed"]." ".$i["cpuCache"]); ?></td>
       </tr>  
       <tr>
        <th>Disk</th>
        <td><?php echo trim($i["diskModel"]." ".$i["diskSerial"]." ".$i["diskSize"]); ?></td>
       </tr>  
       <tr>
        <th>RAM</th>
        <td><?php echo trim($i["ramSpeed"]." ".$i["ramSize"]); ?></td>
       </tr>  
      </tbody>
     </table>
    </td>
    <td>
     <table class="table">
      <thead>
       <tr>
        <th>Spremeni stanje</th>
       </tr>
      </thead>
      <tbody>
       <tr>
        <td>
         <form method="POST">
          <input type="hidden" name="q" value="<?php echo $i["id"]; ?>">
          <input type="hidden" name="status" value="working">
          <input type="submit" class="btn btn-success" value="Delujoč">
         </form>
        </td>
       </tr>
       <tr>
        <td>
         <form method="POST">
          <input type="hidden" name="q" value="<?php echo $i["id"]; ?>">
          <input type="hidden" name="status" value="service">
          <input type="submit" class="btn btn-hardware" value="Servis">
         </form>
        </td>
       </tr>
       <tr>
        <td>
         <form method="POST">
          <input type="hidden" name="q" value="<?php echo $i["id"]; ?>">
          <input type="hidden" name="status" value="dismantle">
          <input type="submit" class="btn btn-danger" value="Razgradnja">
         </form>
        </td>
       </tr>
      </tbody>
     </table>
    </td>
   </tr>
  </tbody>
 </table>
<?php }
