<?php

//the terminal has no authentication, for now

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Terminal za prijavo na prisotnost", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

require_once "inc/auth.php";
$a = new auth();

require_once "interfaces/membersInterface.php";
$m = new membersInterface();

$redirect = (isset($_GET["login"]) && $_GET["login"] > 0);
if($a->verify($redirect) !== False) {

 require_once "interfaces/workHoursInterface.php";
 $w = new workHoursInterface($m);
 
 $myCode = $w->lookupCode($a->user["username"]);
 
} else {

 $myCode = "";
 
}

$workTypes = $m->workTypes;
unset($m);


?><h1>Aplikacija RSO</h1>
<h2>Prisotnost (ure)</h2>
<h3>Prijava</h3>

<?php ob_start(); ?>
<script>
 $.ajaxSetup({ cache: false });
 function refreshAjax() {
  $.ajax({
   data: {
    o: "workHoursPresent"
   },
   url: "ajaxNoAuth.php",
   success: function(result) {
    $("#rows").html(result.html);
   }
  }); 
 }
 
 function logon() {
  var code = $("#code").val();
  $("#code").val("<?php echo $myCode; ?>");
  if(code !== "") {
   $.ajax({
    data: {
     o: "workHoursLogon",
     q: {
      code: code,
      workType: $("#workType").val()
     }
    },
    url: "ajaxNoAuth.php",
    success: function(result) {
     $("#rows").html(result.html);
    }
   });
  }
 }

 function checkWorkType() {
  var code = $("#code").val();
  if(code !== "") {
   $.ajax({
    data: {
     o: "workType",
     q: {
      code: code
     }
    },
    url: "ajaxNoAuth.php",
    success: function(result) {
     $("#workType").val(result.workType).trigger("chosen:updated");
    }
   });
  }
 }

 $("#code").on('keyup', function (e) {
  if (e.keyCode == 13) {
    logon();
  } else {
    checkWorkType();
  }
 });
 
 $(document).ready(function() {
  refreshAjax();
  checkWorkType();
 });
 
</script>
<?php $document->addJS(ob_get_clean()); ?>

<table class="table" style="height: calc(100% - 400px);">
 <tbody style="height: 100%;">
  <tr style="height: 100%;">
   <td style="height: 100%;">
    <table class="table" style="height: 100%;">
     <tbody style="height: 100%;">
      <tr style="height: 100%;">
       <td>
        <input type="text" class="form-control form-control-lg" style="height: 100%; text-align: center; font-size: 50pt" id="code" value="<?php echo $myCode; ?>">
       </td>
      </tr>
      <tr>
        <td>
         <select class="form-control" name="workType" id="workType">
          <?php foreach($workTypes as $workType => $title) { ?>
           <option value="<?php echo $workType; ?>"><?php echo $workType." - ".$title; ?></option>
          <?php } ?>
         </select>
        </td>
      </tr>
     </tbody>
     <tfoot>
      <tr>
       <td style="text-align: center;">
        <button type="button" class="btn btn-primary btn-lg" onclick="logon();">
         <span class="glyphicon glyphicon-time" aria-hidden="true"></span> Prijavi / Odjavi
        </button>
       </td>
      </tr>
     </tfoot>
    </table>
   </td>
   <td>
    <div style="text-align: right; width: 100%;">
     <a href="javascript:refreshAjax();" class="btn btn-info"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a>
    </div>
    <table class="table table-striped">
     <thead>
      <tr>
       <th>Koda</th>
       <th>Ime in priimek</th>
       <th>Izpostava</th>
      </tr>
     </thead>
     <tbody id="rows">
     </tbody>
     <tfoot>
      <tr>
       <td colspan="3">
        <img alt="qr-code" src="qr.png" style="max-width: 100%;" />
       </td>
      </tr>
     </tfoot>
    </table>
   </td>
  </tr>
 </tbody>
</table>
