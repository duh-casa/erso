<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "editComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Uredi monitor", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

require_once "interfaces/screensInterface.php";
$c = new screensInterface();

if(!isset($_GET["q"])) {
 header("Location: index.php", true, 307); //invalid request, redirect back
} else {

if($_SERVER['REQUEST_METHOD'] === 'POST') {

 $data = $_POST;
 $data["enteredBy"] = $a->user["username"];
 $c->modifyEntry($data);
 
 ?><script>
  const bc = new BroadcastChannel("erso-screens");
  bc.postMessage("reload");
  <?php if (http_response_code() == 200) { ?>window.close();<?php } ?>
 </script><?php

} else {

$d = $c->details($_GET["q"]);

$document->add("header", array("auth" => $a));

?>
<h2>Monitorji</h2>

<h3>Uredi vnos</h3>
<form method="POST">
 <table class="table">
  <tbody>
   <tr>
    <th>Proizvajalec</th>
    <td><input type="text" class="form-control" name="manufacturer" id="manufacturer" value="<?php echo $d['manufacturer']; ?>"></td>
   </tr>
   <tr>
    <th>Model</th>
    <td><input type="text" class="form-control" name="model" id="model" value="<?php echo $d['model']; ?>"></td>
   </tr>
   <tr>
    <th>Serijska</th>
    <td><input type="text" class="form-control" name="serial" id="serial" value="<?php echo $d['serial']; ?>"></td>
   </tr>
   <tr>
    <th>Diagonala</th>
    <td><input type="text" class="form-control" name="size" id="size" value="<?php echo $d['size']; ?>"></td>
   </tr>
   <tr>
    <th>Stara oznaka</th>
    <td><input type="text" class="form-control" name="legacyID" value="<?php echo $d['legacyID']; ?>"></td>
   </tr>
   <tr>
    <th>Opombe</th>
    <td><input type="text" class="form-control" name="notes" value="<?php echo $d['notes']; ?>"></td>
   </tr>
   <tr class="warning">
    <th>Lokacija</th>
    <td>
    <p class="text-warning">Pozor: če monitor premaknete v drugo izpostavo, ga ne boste več videli</p>
        <?php
        require_once "inc/locationDropdown.php";
        locationDropdown("location", $d["location"])
        ?> 
    </td>
   </tr>
  </tbody>
  <tfoot>
   <tr>
    <td></td>
    <td>
     <button type="submit" class="btn btn-primary">
      <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Shrani
     </button>
    </td>
   </tr>
  </tfoot>
 </table>
 <input type="hidden" name="id" value="<?php echo $_GET["q"]; ?>">
</form>
<?php }}
