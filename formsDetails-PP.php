<div class="bee-page-container">
<div class="bee-row bee-row-1">
	<div class="bee-row-content">
		<div class="bee-col bee-col-1 bee-col-w12">
			<div class="bee-block bee-block-1 bee-paragraph">
				<p><strong>PROŠNJA - POSAMEZNIK - REFERENČNA ŠTEVILKA: PP-<?php echo $i["id"]; ?> - PODATKI O PROSILCU:</strong></p>
				<p> </p>
			</div>
			<div class="bee-block bee-block-2 bee-divider">
				<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
			</div>
			<div class="bee-block bee-block-3 bee-html-block">
				<style type="text/css">
					.tg {
						border-collapse: collapse;
						border-spacing: 0;
					}

					.tg td {
						border-color: black;
						border-style: solid;
						border-width: 1px;
						font-family: Arial, sans-serif;
						font-size: 14px;
						overflow: hidden;
						padding: 10px 5px;
						word-break: normal;
					}

					.tg th {
						border-color: black;
						border-style: solid;
						border-width: 1px;
						font-family: Arial, sans-serif;
						font-size: 14px;
						font-weight: normal;
						overflow: hidden;
						padding: 10px 5px;
						word-break: normal;
					}

					.tg .tg-lqy6 {
						text-align: right;
						vertical-align: top
					}

					.tg .tg-0lax {
						text-align: left;
						vertical-align: top
					}

					@media screen and (max-width: 767px) {
						.tg {
							width: auto !important;
						}

						.tg col {
							width: auto !important;
						}

						.tg-wrap {
							overflow-x: auto;
							-webkit-overflow-scrolling: touch;
						}
					}
				</style>
				<div class="tg-wrap">
					<table class="tg table table-striped">
						<tbody>
							<tr>
								<td class="tg-lqy6" style="width: 20%;">Ime<br /></td>
								<td class="tg-0lax"><?php echo $i["q12_podatkiPrejemnika"]["first"]; ?></td>
							</tr>
							<tr>
								<td class="tg-lqy6">Priimek</td>
								<td class="tg-0lax"><?php echo $i["q12_podatkiPrejemnika"]["last"]; ?></td>
							</tr>
							<tr>
								<td class="tg-lqy6">Naslov</td>
								<td class="tg-0lax"><?php echo $i["q18_naslovPrejemnika"]["addr_line1"]; ?></td>
							</tr>
							<tr>
								<td class="tg-lqy6">Poštna številka</td>
								<td class="tg-0lax"><?php echo $i["q18_naslovPrejemnika"]["city"]; ?></td>
							</tr>
							<tr>
								<td class="tg-lqy6">Kraj</td>
								<td class="tg-0lax"><?php echo $i["q18_naslovPrejemnika"]["state"]; ?></td>
							</tr>
							<tr>
								<td class="tg-lqy6">Telefonska številka</td>
								<td class="tg-0lax"><?php echo $i["q19_telefonskaStevilka"]["full"]; ?></td>
							</tr>
							<tr>
								<td class="tg-lqy6">Elektronski naslov</td>
								<td class="tg-0lax"><?php echo $i["q20_elektronskiNaslov"]; ?></p><br /></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<p> </p>
<div class="bee-row bee-row-2">
	<div class="bee-row-content">
		<div class="bee-col bee-col-1 bee-col-w12">
			<div class="bee-block bee-block-1 bee-divider">
				<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
			</div>
			<div class="bee-block bee-block-2 bee-paragraph">
				<p><strong>STATUS PROSILCA: <?php echo $i["q37_oznaciteVas"]; ?><br /></strong></p>
			</div>
			<div class="bee-block bee-block-2 bee-paragraph">
				<p><strong>STATUS PROSILCA - DRUGO: <?php echo $i["q26_drugo"]; ?><br /></strong></p>
			</div>
			<div class="bee-block bee-block-3 bee-divider">
				<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
			</div>
		</div>
	</div>
</div>
<p> </p>
<div class="bee-row bee-row-3">
	<div class="bee-row-content">
		<div class="bee-col bee-col-1 bee-col-w12">
			<div class="bee-block bee-block-1 bee-paragraph">
				<p><strong>Prosilec prosi za sledečo opremo:<br /></strong></p>
			</div>
			<div class="bee-block bee-block-2 bee-paragraph">
				<p><?php foreach($i["q30_potrebujem"] as $g) { ?>
          						<li><?php echo $g; ?></li>
        						<?php } ?></p>
			</div>
			<div class="bee-block bee-block-3 bee-paragraph">
				<p>Druga oprema: <?php echo $i["q44_drugo44"]; ?></p>
			</div>
			<div class="bee-block bee-block-4 bee-divider">
				<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
			</div>
		</div>
	</div>
</div>
<p> </p>
<div class="bee-row bee-row-4">
	<div class="bee-row-content">
		<div class="bee-col bee-col-1 bee-col-w12">
			<div class="bee-block bee-block-1 bee-paragraph">
				<p><strong>Podatki o zastopniku prosilca:<br /></strong></p>
				<p> </p>
			</div>
			<div class="bee-block bee-block-2 bee-html-block">
				<style type="text/css">
					.tg {
						border-collapse: collapse;
						border-spacing: 0;
					}

					.tg td {
						border-color: black;
						border-style: solid;
						border-width: 1px;
						font-family: Arial, sans-serif;
						font-size: 14px;
						overflow: hidden;
						padding: 10px 5px;
						word-break: normal;
					}

					.tg th {
						border-color: black;
						border-style: solid;
						border-width: 1px;
						font-family: Arial, sans-serif;
						font-size: 14px;
						font-weight: normal;
						overflow: hidden;
						padding: 10px 5px;
						word-break: normal;
					}

					.tg .tg-lqy6 {
						text-align: right;
						vertical-align: top
					}

					.tg .tg-0lax {
						text-align: left;
						vertical-align: top
					}

					@media screen and (max-width: 767px) {
						.tg {
							width: auto !important;
						}

						.tg col {
							width: auto !important;
						}

						.tg-wrap {
							overflow-x: auto;
							-webkit-overflow-scrolling: touch;
						}
					}
				</style>
				<div class="tg-wrap">
					<table class="tg table table-striped">
						<tbody>
							<tr>
								<td class="tg-lqy6" style="width: 20%;">Ime<br /></td>
								<td class="tg-0lax"><?php echo $i["q62_podatkiKontaktne"]["first"]; ?></td>
							</tr>
							<tr>
								<td class="tg-lqy6">Priimek</td>
								<td class="tg-0lax"><?php echo $i["q62_podatkiKontaktne"]["last"]; ?></td>
							</tr>
							<tr>
								<td class="tg-lqy6">Telefonska številka</td>
								<td class="tg-0lax"><?php echo $i["q63_telefonskaStevilka63"]["full"]; ?></td>
							</tr>
							<tr>
								<td class="tg-lqy6">Elektronski naslov</td>
								<td class="tg-0lax"><?php echo $i["q64_elektronskiNaslov64"]; ?><br /></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<p> </p>
			<div class="bee-block bee-block-3 bee-divider">
				<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
			</div>
		</div>
	</div>
</div>
<div class="bee-row bee-row-4">
	<div class="bee-row-content">
		<div class="bee-col bee-col-1 bee-col-w12">
			<div class="bee-block bee-block-1 bee-paragraph">
				<p><strong>Prejemniki socialnih in drugih oblik pomoči - Kontaktna oseba:<br /></strong></p>
				<p> </p>
			</div>
			<div class="bee-block bee-block-2 bee-html-block">
				<style type="text/css">
					.tg {
						border-collapse: collapse;
						border-spacing: 0;
					}

					.tg td {
						border-color: black;
						border-style: solid;
						border-width: 1px;
						font-family: Arial, sans-serif;
						font-size: 14px;
						overflow: hidden;
						padding: 10px 5px;
						word-break: normal;
					}

					.tg th {
						border-color: black;
						border-style: solid;
						border-width: 1px;
						font-family: Arial, sans-serif;
						font-size: 14px;
						font-weight: normal;
						overflow: hidden;
						padding: 10px 5px;
						word-break: normal;
					}

					.tg .tg-lqy6 {
						text-align: right;
						vertical-align: top
					}

					.tg .tg-0lax {
						text-align: left;
						vertical-align: top
					}

					@media screen and (max-width: 767px) {
						.tg {
							width: auto !important;
						}

						.tg col {
							width: auto !important;
						}

						.tg-wrap {
							overflow-x: auto;
							-webkit-overflow-scrolling: touch;
						}
					}
				</style>
				<div class="tg-wrap">
					<table class="tg table table-striped">
						<tbody>
							<tr>
								<td class="tg-lqy6" style="width: 20%;">Naziv organizacije<br /></td>
								<td class="tg-0lax"><?php echo $i["q39_nazivOrganizacije"]; ?></td>
							</tr>
							<tr>
								<td class="tg-lqy6">Kontaktna oseba</td>
								<td class="tg-0lax"><?php echo $i["q40_kontaktnaOseba"]; ?></td>
							</tr>
							<tr>
								<td class="tg-lqy6">Telefonska številka</td>
								<td class="tg-0lax"><?php echo $i["q67_telefonskaStevilka67"]["full"]; ?></td>
							</tr>
							<tr>
								<td class="tg-lqy6">Elektronski naslov</td>
								<td class="tg-0lax"><?php echo $i["q68_elektronskiNaslov68"]; ?><br /></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<p> </p>
			<div class="bee-block bee-block-3 bee-divider">
				<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
			</div>
		</div>
	</div>
</div>
<p> </p>
<div class="bee-row bee-row-5">
	<div class="bee-row-content">
		<div class="bee-col bee-col-1 bee-col-w12">
			<div class="bee-block bee-block-1 bee-paragraph">
				<p><strong>Dodatne opombe k prošnji:<br /></strong></p>
			</div>
			<div class="bee-block bee-block-2 bee-paragraph">
				<p><?php echo $i["q33_biNam"]; ?></p>
			</div>
		</div>
	</div>
</div>
<p> </p>
<div class="bee-block bee-block-3 bee-divider">
				<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
			</div>
		</div>
	</div>
</div>
<p> </p>
<div class="bee-row bee-row-5">
	<div class="bee-row-content">
		<div class="bee-col bee-col-1 bee-col-w12">
			<div class="bee-block bee-block-1 bee-paragraph">
				<p><strong>Zgodovina obravnave te prošnje<br /></strong></p>
			</div>
			<div class="bee-block bee-block-2 bee-paragraph">
				<p><?php foreach($i["status-tooltip"] as $g) { ?>
          						<li><?php echo $g; ?></li>
        						<?php } ?></p>
			</div>
		</div>
	</div>
</div>
