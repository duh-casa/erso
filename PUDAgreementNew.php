<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "editComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Novo potrdilo PUD", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

if(!isset($_GET["q"])) {
 header("Location: members.php", true, 307); //invalid request, redirect back
}
?>

<script>
function printout(docType) {
  var url = "printouts.php?" + $.param({
    t: docType,
    o: "PUDAgreement",
    q: {
      username: $("#username").val(),
      from: $("#from").val(),
    }
  });
  window.open(url, '_blank');
}
</script>

<h1>Aplikacija RSO</h1>
<h2>Novo potrdilo PUD</h2>

<h3>Uredi vnos</h3>
<form method="POST">
 <table class="table">
  <tbody>
   <tr>
    <th style="width: 20%;">Uporabniško ime</th>
    <td><input type="text" class="form-control" id="username" name="username" readonly value="<?php echo $_GET["q"]; ?>"></td>
   </tr>
   <tr>
    <th>Začetek sodelovanja</th>
    <td><input type="date" class="form-control" id="from" name="from"></td>
   </tr>
  </tbody>
  <tfoot>
   <tr>
    <td></td>
    <td>
     <a href="javascript:printout('pdf')" type="submit" class="btn btn-success">
      <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Ustvari za tisk
     </a>
     <a href="javascript:printout('odt')" type="submit" class="btn btn-success">
      <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Ustvari za urejanje
     </a>
    </td>
   </tr>
  </tfoot>
 </table>
 <input type="hidden" name="id" value="<?php echo $_GET["q"]; ?>">
</form>
