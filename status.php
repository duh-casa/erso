<?php

header('Content-Type: application/json');
ob_start();

//authentication is not required to access this API

require_once "interfaces/auditLogInterface.php";
$log = new auditLogInterface();

$data = array();
foreach($log->getLog($_REQUEST) as $entry) {
	unset($entry["enteredBy"]);
	unset($entry["user"]);
	unset($entry["id"]);

	$data[] = $entry;
}

//output log in ascending order (newer at the bottom)
$data = array_reverse($data);

ob_end_clean();

//output valid JSON
echo json_encode($data);
