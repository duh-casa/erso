<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "editWorkHours") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Urejanje vnosa prisotnosti", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

require_once "interfaces/workHoursInterface.php";
$w = new workHoursInterface();

if(!isset($_GET["username"]) || !isset($_GET["from"])) {
 header("Location: workHours.php", true, 307); //invalid request, redirect back
} else {

if($_SERVER['REQUEST_METHOD'] === 'POST') {

 $w->modifyEntry($_POST);
 ?><script>
  const bc = new BroadcastChannel("erso-workHours");
  bc.postMessage("reload");
  <?php if (http_response_code() == 200) { ?>window.close();<?php } ?>
 </script><?php
 
} else { 

$d = $w->details($_GET["username"], $_GET["from"]);

$document->add("header", array("auth" => $a));

?>
<h2>Prisotnost</h2>

<h3>Uredi vnos</h3>
<form method="POST">
 <table class="table">
  <tbody>
   <tr>
    <th style="width: 20%;">Uporabniško ime</th>
    <td><input type="text" class="form-control" name="username" readonly value="<?php echo $d['username']; ?>"></td>
   </tr>
   <tr>
    <th>Ime<br><small>(obvezno)</small></th>
    <td><input type="text" class="form-control" name="name" readonly value="<?php echo $d['name']; ?>"></td>
   </tr>
   <tr>
    <th>Od</th>
    <td><input type="text" class="form-control" name="from" placeholder="<?php echo date('Y-m-d', strtotime('yesterday')).' '.date('H:i'); ?>" value="<?php echo $d['from']; ?>"></td>
   </tr>
   <tr>
    <th>Do</th>
    <td><input type="text" class="form-control" name="until" placeholder="<?php echo date('Y-m-d', strtotime('yesterday')).' '.date('H:i', strtotime('+2 hours')); ?>" value="<?php echo $d['until']; ?>"></td>
   </tr>
  </tbody>
  <tfoot>
   <tr>
    <td></td>
    <td>
     <button type="submit" class="btn btn-primary">
      <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Shrani
     </button>
    </td>
   </tr>
  </tfoot>
 </table>
 <input type="hidden" name="originalFrom" value="<?php echo $_GET['from']; ?>">
</form><?php }}
