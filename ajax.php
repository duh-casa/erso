<?php

header('Content-Type: application/json');
$out = array(); ob_start();

require_once "inc/auth.php";
$a = new auth();
if($a->verify(False) === False) {
 $out["error"] = "Login required";
 $out["redirect"] = "login.php";
} else {

 //import Query data
 if(isset($_GET["q"])) {
  $q = $_GET["q"];
 } else {
  $q = array();
 }

 //switch by Operation
 if(isset($_GET["o"])) {
  if($_GET["o"] == "computers") {
   
   require_once "interfaces/computersInterface.php";
   $c = new computersInterface();

   ob_start();   
   foreach($c->search($q) as $i) {
    ?>
     <tr class="<?php echo $i["status"][0]["color"]; ?>">
      <td><?php echo $i["type"]; ?></td>
      <td><?php echo $i["model"]; ?></td>
      <td><?php echo $i["id"]; ?></td>
      <td class="hide5"><?php echo $i["legacyID"]; ?></td>
      <td class="hide3"><?php echo $i["graphics"]; ?></td>
      <td class="hide2"><?php echo trim($i["cpuModel"]." ".$i["cpuSpeed"]." ".$i["cpuCache"]); ?></td>
      <td class="hide4"><?php echo trim($i["diskModel"]." ".$i["diskSerial"]." ".$i["diskSize"]); ?></td>
      <td class="hide1"><?php echo trim($i["ramSpeed"]." ".$i["ramSize"]); ?></td>
      <td>
        <?php foreach($i["status"] as $status) { ?>
        <label title="<?php echo $status["tooltip"]; ?>" class="label label-<?php echo $status["color"]; ?>"><?php echo $status["text"]; ?></label>
        <?php } ?>
      </td>
      <td>
       <?php if($a->verify(False, "qc") !== False) { ?>
        <a href="javascript:window.open('qc.php?q=<?php echo rawurlencode($i["id"]); ?>', '_blank'); void(0);" class="btn btn-warning"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Kontrola</a>
       <?php } ?>
      </td>
      <td>
       <?php if($a->verify(False, "donations") !== False) { ?>
        <a href="donations.php?q[id]=<?php echo rawurlencode($i["id"]); ?>&q[type]=computers" target="_blank" class="btn btn-success"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span> Oddaja</a>
       <?php } ?>
      </td>
      <td>
       <?php if($a->verify(False, "editComputers") !== False) { ?>      
        <a href="javascript:window.open('computersEdit.php?q=<?php echo rawurlencode($i["id"]); ?>', '_blank'); void(0);" class="btn btn-default">
         <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Uredi</a>
       <?php } ?>
      </td>
     </tr> 
    <?php
   }
   $out["html"] = ob_get_clean();

  } elseif($_GET["o"] == "screens") {
  
   require_once "interfaces/screensInterface.php";
   $c = new screensInterface();

   ob_start();
   foreach($c->search($q) as $i) {
    ?>
     <tr class="<?php echo $i["status"][0]["color"]; ?>">
      <td><?php echo $i["manufacturer"]; ?></td>
      <td><?php echo $i["model"]; ?></td>
      <td class="hide5"><?php echo $i["serial"]; ?></td>
      <td><?php echo $i["size"]; ?></td>
      <td><?php echo $i["id"]; ?></td>
      <td><?php echo $i["legacyID"]; ?></td>
      <td class="hide3"><?php echo $i["notes"]; ?></td>
      <td>
        <?php foreach($i["status"] as $status) { ?>
        <label title="<?php echo $status["tooltip"]; ?>" class="label label-<?php echo $status["color"]; ?>"><?php echo $status["text"]; ?></label>
        <?php } ?>
      </td>
      <td>
       <?php if($a->verify(False, "donations") !== False) { ?>
        <a href="donations.php?q[id]=<?php echo rawurlencode($i["id"]); ?>&q[type]=screens" target="_blank" class="btn btn-success"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span> Oddaja</a>
       <?php } ?>
      </td>
      <td>
       <?php if($a->verify(False, "editComputers") !== False) { ?>      
        <a href="javascript:window.open('screensEdit.php?q=<?php echo rawurlencode($i["id"]); ?>', '_blank'); void(0);" class="btn btn-default">
         <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Uredi</a>
       <?php } ?>
      </td>
     </tr> 
    <?php
   }
   $out["html"] = ob_get_clean();  

  } elseif($_GET["o"] == "peripherals") {

   require_once "interfaces/peripheralsInterface.php";
   $c = new peripheralsInterface();

   ob_start();   
   foreach($c->search($q) as $i) {
    ?>
     <tr class="<?php echo $i["status"][0]["color"]; ?>">
      <td><?php echo $i["type"]; ?></td>
      <td><?php echo trim($i["manufacturer"]." ".$i["model"]); ?></td>
      <td><?php echo $i["serial"]; ?></td>
      <td><?php echo $i["id"]; ?></td>
      <td><?php echo $i["legacyID"]; ?></td>
      <td><?php echo $i["notes"]; ?></td>
      <td>
        <?php foreach($i["status"] as $status) { ?>
        <label title="<?php echo $status["tooltip"]; ?>" class="label label-<?php echo $status["color"]; ?>"><?php echo $status["text"]; ?></label>
        <?php } ?>
      </td>
      <td>
       <?php if($a->verify(False, "donations") !== False) { ?>
        <a href="donations.php?q[id]=<?php echo rawurlencode($i["id"]); ?>&q[type]=peripherals" target="_blank" class="btn btn-success"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span> Oddaja</a>
       <?php } ?>
      </td>
      <td>
       <?php if($a->verify(False, "editComputers") !== False) { ?>      
        <a href="javascript:window.open('peripheralsEdit.php?q=<?php echo rawurlencode($i["id"]); ?>', '_blank'); void(0);" class="btn btn-default">
         <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Uredi</a>
       <?php } ?>
      </td>
     </tr> 
    <?php
   }
   $out["html"] = ob_get_clean();  

  } elseif($_GET["o"] == "disks") {
  
   require_once "interfaces/disksInterface.php";
   $c = new disksInterface();

   ob_start();
   foreach($c->search($q) as $i) {
    ?>
     <tr>
      <td><?php echo $i["model"]; ?></td>
      <td><?php echo $i["serial"]; ?></td>
      <td><?php echo $i["size"]; ?></td>
      <td><?php echo $i["legacyID"]; ?></td>
      <td><?php echo $i["status"]; ?></td>
      <td></td>
     </tr> 
    <?php
   }
   $out["html"] = ob_get_clean();  

  } elseif($_GET["o"] == "members") {

   require_once "interfaces/membersInterface.php";
   $m = new membersInterface();

   require_once "interfaces/volunteerAgreementsInterface.php";
   $va = new volunteerAgreementsInterface();

   ob_start();   
   foreach($m->search($q) as $i) {
    ?>
     <tr class="<?php echo $i["color"]; ?>">
      <td><?php echo $i["username"]; ?></td>
      <td><?php echo $i["name"]; ?></td>
      <td><?php echo $i["address"]; ?></td>
      <td><?php echo $i["email"]; ?></td>
      <td><?php echo $i["gsm"]; ?></td>
      <td><?php echo $i["age"]; ?></td>
      <td>
       <?php if($a->verify(False, "editMembers") !== False) { ?>      
        <a href="javascript:window.open('membersEdit.php?q=<?php echo rawurlencode($i["username"]); ?>', '_blank'); void(0);" class="btn btn-default">
         <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Uredi</a>
       <?php } ?>
      </td>
      <td>
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
            Obrazci
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
            <li>
              <?php if($a->verify(False, "viewMembers") !== False) { ?>
                <?php if($va->valid($i["username"])) { ?>
                <a href="volunteerAgreements.php?q=<?php echo rawurlencode($i["username"]); ?>" target="_blank">
                  <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Poglej dogovore o prostovoljstvu</a>
                <?php } else { ?>
                <a href="volunteerAgreementsNew.php?q=<?php echo rawurlencode($i["username"]); ?>" target="_blank">
                  <span class="glyphicon glyphicon-star" aria-hidden="true"></span> Ustvari dogovor o prostovoljstvu</a>
                <?php } ?>
              <?php } ?>
            </li>
            <li>
              <?php if($a->verify(False, "viewMembers") !== False) { ?>
                <a href="PUDAgreementNew.php?q=<?php echo rawurlencode($i["username"]); ?>" target="_blank">
                  <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Potrdilo PUD</a>
              <?php } ?>
            </li>
            <li>
              <?php if($a->verify(False, "viewMembers") !== False) { ?>
                <a href="timeRecordNew.php?q=<?php echo rawurlencode($i["username"]); ?>" target="_blank">
                  <span class="glyphicon glyphicon-time" aria-hidden="true"></span> Evidenca ur</a>
              <?php } ?>
            </li>
          </ul>
        </div>
      </td>
     </tr> 
    <?php
   }
   $out["html"] = ob_get_clean();
      
  } elseif($_GET["o"] == "autoEntry") {
   
   require_once "interfaces/autoEntryInterface.php";
   $ae = new autoEntryInterface();
   
   ob_start();
   foreach($ae->recent() as $item) {
    ?>
     <tr>
      <td><?php echo $item["id"]; ?>.</td>
      <td><?php echo $item["model"]; ?></td>
      <td><?php echo $item["when"]; ?></td>
      <td><a href="javascript:ajaxFill(<?php echo $item["id"]; ?>);" class="btn btn-warning"><span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span> Uporabi</a></td>
     </tr>   
    <?php
   }
   $out["html"] = ob_get_clean();

  } elseif($_GET["o"] == "workHours") {
   
   require_once "interfaces/workHoursInterface.php";
   $w = new workHoursInterface();
   
   ob_start();
   foreach($w->search($q) as $item) {
    if($item["interval"] == "24:00") {
     $item["interval"] = "Ni odjave";
    }
   
    ?>
     <tr>
      <td><?php echo $item["name"]; ?></td>
      <td><?php echo date("d. m. Y - H:i:s", strtotime($item["from"])); ?></td>
      <td><?php echo date("d. m. Y - H:i:s", strtotime($item["until"])); ?></td>
      <td><?php echo $item["interval"]; ?></td>
      <td>
       <?php if($a->verify(False, "editWorkHours") !== False) { ?>       
        <a href="javascript:window.open('workHoursEdit.php?username=<?php echo rawurlencode($item["username"]); ?>&from=<?php echo rawurlencode(strtotime($item["from"])); ?>', '_blank'); void(0);" class="btn btn-default">
         <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Uredi
        </a>
       <?php } ?>
      </td>
     </tr>   
    <?php
   }
   $out["html"] = ob_get_clean();

  } elseif($_GET["o"] == "volunteerAgreements") {
   
   require_once "interfaces/volunteerAgreementsInterface.php";
   $w = new volunteerAgreementsInterface();
   
   ob_start();
   foreach($w->search($q) as $item) {
   
    ?>
     <tr>
      <td><?php echo $item["name"]; ?></td>
      <td><?php echo date("d. m. Y", strtotime($item["from"])); ?></td>
      <td><?php echo date("d. m. Y", strtotime($item["until"])); ?></td>
      <td>
       <?php if($a->verify(False, "viewMembers") !== False) { ?>
        <a href="javascript:window.open('volunteerAgreementsEdit.php?username=<?php echo rawurlencode($item["username"]); ?>&from=<?php echo rawurlencode(strtotime($item["from"])); ?>&until=<?php echo rawurlencode(strtotime($item["until"])); ?>', '_blank'); void(0);" class="btn btn-warning">
         <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Uredi
        </a>
       <?php } ?>
      </td>
      <td>
       <a href="javascript:printout(<?php echo rawurlencode(json_encode($item)); ?>, 'pdf')" class="btn btn-info">
        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Izpis za tisk
       </a>
      </td>
      <td>
       <a href="javascript:printout(<?php echo rawurlencode(json_encode($item)); ?>, 'odt')" class="btn btn-info">
        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Izpis za urejanje
       </a>
      </td>
     </tr>   
    <?php
   }
   $out["html"] = ob_get_clean();

  } elseif($_GET["o"] == "donationsUserList") {
   
   require_once "interfaces/donationsInterface.php";
   $d = new donationsInterface();
   
   ob_start();
   ?><option value="0">Nov vnos</option><?php
   foreach($d->listAll() as $item) {
    $text = trim(trim($item["name"]." ".$item["lastname"]." (".$item["birthdate"].") ".$item["company"]." ".$item["gsm"]." ".$item["email"]." - ".$item["address"].", ".$item["city"],"(),-"));
    ?><option value="<?php echo $item['id']; ?>"><?php echo $text; ?></option><?php
   }
   $out["html"] = ob_get_clean();

  } elseif($_GET["o"] == "donationsUserSearch") {

   require_once "interfaces/donationsInterface.php";
   $d = new donationsInterface();
   $out = $d->details($q["id"]);

   ob_start();
   foreach($d->gear($q["id"]) as $item) {
    ?>
     <tr>
      <td><?php echo $item["id"]; ?></td>
      <td><?php echo $item["model"]; ?></td>
      <td><?php echo $item["type"]; ?></td>
      <td><input type="checkbox" value="<?php echo substr($item["type"], 0, 1)."-".$item["id"]; ?>" checked></td>
     </tr>
    <?php
   }
   $out["html"] = ob_get_clean();

   ob_start();
   foreach($d->forms($q["id"]) as $item) {
    if(is_array($item["gear"])) {
      $item["gear"] = implode("<br>", $item["gear"]);
    }
    ?>
     <tr>
      <td><?php echo $item["type-code"]."-".$item["id"]; ?></td>
      <td><?php echo $item["status"]; ?></td>
     </tr>
     <tr>
      <td colspan="2">Potrebuje:<br><?php echo $item["gear"]; ?></td>
     </tr>
     <tr>
      <td colspan="2"><hr></td>
     </tr>
    <?php
   }
   $out["forms"] = ob_get_clean();
   
  } elseif($_GET["o"] == "labels") {

   require_once "interfaces/labelQueueInterface.php";
   $lq = new labelQueueInterface($q["type"]);
   foreach($lq->search($q) as $item) { ?>
    <tr id="row<?php echo $item["id"]; ?>" class="<?php if($lq->checkMark($item["id"])) { echo "success"; } ?>" onclick="labelMark(<?php echo $item["id"]; ?>);">
     <td colspan="4"><?php echo $item["title"]; ?></td>
     <td><?php echo date("j. n. Y H:i:s", strtotime($item["tds"])); ?></td>
    </tr>
    <?php }

    $out["html"] = ob_get_clean();

  } elseif($_GET["o"] == "labelMark") {

   $out["id"] = $q["id"];
   $set = ($q["class"] != "success");

   require_once "interfaces/labelQueueInterface.php";
   $lq = new labelQueueInterface($q["type"]);
 
   if($lq->mark($q["id"], $set)) {
    $out["class"] = "success";    
   } else {
    $out["class"] = "";
   }

  } elseif($_GET["o"] == "labelGroupMark") {

   require_once "interfaces/labelQueueInterface.php";
   $lq = new labelQueueInterface($q["type"]);
   foreach($lq->search($q) as $item) {
    $lq->mark($item["id"], filter_var($q["state"], FILTER_VALIDATE_BOOLEAN));
    ?>
    <tr id="row<?php echo $item["id"]; ?>" class="<?php if($lq->checkMark($item["id"])) { echo "success"; } ?>" onclick="labelMark(<?php echo $item["id"]; ?>);">
     <td colspan="4"><?php echo $item["title"]; ?></td>
     <td><?php echo date("j. n. Y H:i:s", strtotime($item["tds"])); ?></td>
    </tr>
    <?php }

    $out["html"] = ob_get_clean();

  } elseif($_GET["o"] == "labelReset") {

   require_once "interfaces/labelQueueInterface.php";
   $lq = new labelQueueInterface($q["type"]);
   $lq->unmarkAll();

  } elseif($_GET["o"] == "labelFromChange") {

    $out = $q;

    require_once "inc/idEncoding.php";
    $e = new idEncoding();

    $tmpUntil = $e->idDecode($out["searchUntil"]);
    $tmpFromOld = $e->idDecode($out["searchFromOld"]);
    if($tmpUntil > 0 && $tmpFromOld > 0) {
      $tmpChange = $tmpUntil - $tmpFromOld;
    } else {
      $tmpChange = 20;
    }

    $tmpMax = 0;

    $tmpFrom = $e->idDecode($out["searchFrom"]);

    require_once "interfaces/gearInterface.php";
    $g = new gearInterface();

    $tmpSearch = $g->search(array("type" => $q["type"]), $tmpChange+1);
    $tmpMax = $e->idDecode($tmpSearch[0]["id"]);

    if($tmpFrom == 0) {
      $out["searchFrom"] = $tmpSearch[$tmpChange]["id"];
      $tmpFrom = $e->idDecode($out["searchFrom"]);
    }

    unset($g);

    $tmpUntil = $tmpFrom + $tmpChange;
    if($tmpUntil > $tmpMax) { $tmpUntil = $tmpMax; }
    $out["searchUntil"] = $e->idEncode($tmpUntil);
 
  } elseif($_GET["o"] == "autoEntryFill") {
  
   require_once "interfaces/autoEntryInterface.php";
   $ae = new autoEntryInterface();
  
   $out["fields"] = $ae->details((int) $q["id"]);

  } elseif($_GET["o"] == "enableUser") {

   $a->changePassword("qqq", $q["username"]);
   if($a->enabled($q["username"])) {
    ?><label class="label label-success">Da</label><?php
   } else {
    ?><label class="label label-danger">Ne</label><?php
   }
   $out["html"] = ob_get_clean();
  
  } elseif($_GET["o"] == "disableUser") {
  
   $a->changePassword(False, $q["username"]);
   if($a->enabled($q["username"])) {
    ?><label class="label label-success">Da</label><?php
   } else {
    ?><label class="label label-danger">Ne</label><?php
   }
   $out["html"] = ob_get_clean();
  
  } elseif($_GET["o"] == "logoutUser") {
  
   if($q["username"] == $a->user["username"]) {
    $out["redirect"] = "login.php";
   }
  
   $a->logout($q["username"]);
   
   $out["success"] = !$a->loggedIn($q["username"]);
  
  } elseif($_GET["o"] == "returnsLookupCode") {

   require_once "interfaces/gearInterface.php";
   $g = new gearInterface();

   $code = $g->readCode($q["code"]);
   if($code !== False && isset($code["model"])) {
 
    //this is some kind of internal ID for tracking items
    //ID is encoded here
    $out["item"] = $code;
 
    ob_start();
    ?>
    <tr>
      <td><?php 
       echo array(
        "computers" => "Računalnik",
        "screens" => "Ekran",
        "peripherals" => "Periferija"
       )[$code["type"]]; 
      ?></td>
      <td><?php echo $code["id"]; ?></td>
      <td><?php echo $code["model"]; ?></td>
    </tr>
    <?php

    $out["html"] = ob_get_clean();
    $out["success"] = true;
   } else {
    //code segment not recognised
    $out["success"] = false;
   }

  } elseif($_GET["o"] == "returnsDo") {

    if(count($q) > 0) {

      require_once "interfaces/auditLogInterface.php";
      $l = new auditLogInterface($a);

      //$itemId = array of id and type
      foreach($q["itemIds"] as $itemId) {
        $l->setReturn($itemId);
      }

      //this only returns success if... succesful
      $out["success"] = true;
    } else {
      $out["success"] = false;
    }

  } elseif($_GET["o"] == "forms") {
  
   require_once "interfaces/formsInterface.php";
   $c = new formsInterface();

   ob_start();
   foreach($c->search($q) as $i) {
    ?>
     <tr class="<?php echo $i["status-color"]; ?>">
      <td>
        <p style="font-weight: bold;">Referenčna številka: <?php echo $i["type-code"]; ?>-<?php echo $i["id"]; ?></p>
        <hr>
        <p><?php
          echo implode(", ", array_filter(
            array($i["nameLastname"], $i["address"], $i["email"], $i["phone"])
          ));
        ?></p>
        <p><?php echo $i["status"]; ?></p>
      </td>
      <td>
        <p><?php echo $i["type"]; ?> sledečo računalniško opremo:</p>
        <ul>
        <?php foreach($i["gear"] as $g) { ?>
          <li><?php echo $g; ?></li>
        <?php } ?>
        </ul>
        <?php 
          $tmpOpombe = trim($i["opombe"]);
          if(strlen($tmpOpombe) > 0) {
        ?>
        <p>Interna opomba: <?php echo $tmpOpombe; ?></p>
        <?php 
          }
          unset($tmpOpombe);
        ?>
      </td>
      <td>
       <?php if($a->verify(False, "editForms") !== False) { ?>      
        <a href="formsEdit.php?q=<?php echo rawurlencode($i["id"]); ?>" class="btn btn-info" target="_blank">
         <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Podrobnosti</a>
       <?php } ?>
      </td>
     </tr> 
    <?php
   }
   $out["html"] = ob_get_clean();  

  } elseif($_GET["o"] == "authPing") {
 
   //this doesn't actually have to do anything
   $out["success"] = true;

  } else {
   $out["error"] = "Command not recognised";
  }
 } else {
  $out["error"] = "Command missing";
 }
}

//deallocate authenticator and send session headers
unset($a);

//output valid JSON
echo json_encode(($out + array("debug" => ob_get_clean())));
