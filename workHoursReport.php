<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewWorkHoursReport") === False) {
 die("Potrebno se je prijaviti");
}

$printout = False;
if(isset($_REQUEST["print"])) {

	require_once "interfaces/pdfInterface.php";
	$pdf = new pdfInterface();

	ob_start();	
	$printout = True;
}

require_once "inc/html.php";

$year = ((int) date("Y")) - 1;

if($printout) {
	$document = new html("POROČILO O PROSTOVOLJSTVU za leto ".$year, array(
	 "css" => "print.css"
	));	
} else {
	$document = new html("Aplikacija eRSO - Letno poročilo za prisotnost", array(
	 "bootstrap" => True,
	 "css" => "style.css"
	));

	$document->add("header", array("auth" => $a));

?><h2>Letno poročilo za prisotnost</h2>
<p><label class="label label-info">Pojasnilo</label> To je vsebina za letno poročilo, ki se <a href="https://www.ajpes.si/Letna_porocila/Predlozitev/Prostovoljske_organizacije/Splosno" target="_blank">oddaja na AJPES</a> na podlagi zakona o prostovoljstvu. Poročilo mora biti podpisano s strani pooblaščene osebe za oddajo poročila, tako da ga eRSO ne more oddati avtomatsko.</p>
<p><label class="label label-info">Opomba</label> Podatki so izračunani na zadnji dan v letu, kot je potrebno za poročilo. Vrednosti so na podlagi podatkov, ki so dejansko zabeleženi v aplikaciji (torej naprimer ne vključuje ur ki še bodo beležene do konca leta).</p>

<a href="?print=1" target="_blank" class="btn btn-info">
	<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Tiskanje poročila
</a>

<br><br>

<?php
}

require_once "interfaces/membersInterface.php";
$m = new membersInterface();

require_once "interfaces/workHoursInterface.php";
$w = new workHoursInterface($m);

$o = $w->organizationData();

?>

<div class="whReport">
<h3>POROČILO O PROSTOVOLJSTVU za leto <?php echo $year; ?></h3>
<p>(Podlaga: 41. člen Zakona o prostovoljstvu)</p>

<p>Izpolnijo prostovoljske organizacije ali organizacije s prostovoljskim programom, ki so vpisane v vpisnik prostovoljskih organizacij na dan 31.12. in ga oddajo Agenciji za javnopravne evidence in storitve preko spletnega portala istočasno s podatki iz letnega poročila za namen državne statistike, ki jih te organzacije predložijo v skladu s predpisi, ki določajo vodenje poslovnih knjig in pripravo letnih poročil; podatki se izpolnjujejo za preteklo koledarsko leto.</p>

<h4>1. PODATKI O ORGANIZACIJI</h4>
<p>Podatke vpisujte v celih številih!</p>

<table class="table">
	<tbody>
		<tr>
			<td>1.1</td>
			<td>Matična številka pravne osebe</td>
			<td class="whReportData"><?php echo $o["matična"]; ?></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>1.2</td>
			<td>Naziv pravne osebe</td>
			<td colspan="3" class="whReportData"><?php echo $o["naziv"]; ?></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td>(1)</td>
			<td></td>
			<td>(2)</td>
		</tr>
		<tr>
			<td>1.3</td>
			<td>Sedež pravne osebe</td>
			<td class="whReportData"><?php echo $o["sedežNaslov"]; ?></td>
			<td></td>
			<td class="whReportData"><?php echo $o["sedežMesto"]; ?></td>
		</tr>
		<tr>
			<td>1.4</td>
			<td>Osebno ime zakonitega zastopnika</td>
			<td colspan="3" class="whReportData"><?php echo $o["zastopnik"]; ?></td>
		</tr>
		<tr>
			<td>1.5</td>
			<td>Statusna oblika pravne osebe</td>
			<td class="whReportData"><?php echo $o["oblika"]; ?></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>1.6</td>
			<td>Glavna dejavnost pravne osebe po SKD 2008</td>
			<td class="whReportData"><?php echo $o["skd2008"]; ?></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>1.7</td>
			<td>Osebno ime osebe, ki izpolnjuje poročilo o prostovoljstvu</td>
			<td colspan="3"><?php echo ""; ?></td>
		</tr>
		<tr>
			<td>1.8</td>
			<td>Telefon osebe, ki izpolnjuje poročilo o prostovoljstvu</td>
			<td colspan="3"><?php echo ""; ?></td>
		</tr>
		<tr>
			<td>1.9</td>
			<td>Email osebe, ki izpolnjuje poročilo o prostovoljstvu</td>
			<td colspan="3"><?php echo ""; ?></td>
		</tr>
	</tbody>
</table>

<h4>2. POROČILO O PROSTOVOLJSTVU</h4>

<?php 

$ageOn = $year."-12-31";

$results = array();
foreach(array("m", "ž") as $gender) {
	foreach(array(
		1 => array(0, 18),
		2 => array(18, 30),
		3 => array(30, 60),
		4 => array(60, 150),
	) as $ageGroupIndex => $ageGroup) {
		$results[$gender][$ageGroupIndex] = count(
			$w->activeMembersByGenderAge($year, $gender, $ageGroup[0], $ageGroup[1], $ageOn)
		);
	}
}

$results["m"][5] = array_sum($results["m"]);
$results["ž"][5] = array_sum($results["ž"]);

//var_dump($w->activeMembersByGenderAge($year, "ž", 0, 150, $ageOn))

?>

<table class="table">
	<tbody>
		<tr>
			<th></th>
			<th></th>
			<th>Do 18. leta<br>(1)</th>
			<th>Od vključno 18. do 30. leta<br>(2)</th>
			<th>Od vključno 30. do 60. leta<br>(3)</th>
			<th>Od vključno 60. leta dalje<br>(4)</th>
			<th>(5 = 1 + 2 + 3 + 4)</th>
		</tr>
		<tr>
			<td>2.1</td>
			<td>Skupno število prostovoljcev po spolu in starostnih skupinah<br>(2.1.1 + 2.1.2)</td>
			<td class="whReportData"><?php echo $results["m"][1] + $results["ž"][1]; ?></td>
			<td class="whReportData"><?php echo $results["m"][2] + $results["ž"][2]; ?></td>
			<td class="whReportData"><?php echo $results["m"][3] + $results["ž"][3]; ?></td>
			<td class="whReportData"><?php echo $results["m"][4] + $results["ž"][4]; ?></td>
			<td class="whReportData"><?php echo $results["m"][5] + $results["ž"][5]; ?></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td>(1)</td>
			<td>(2)</td>
			<td>(3)</td>
			<td>(4)</td>
			<td>(5 = 1 + 2 + 3 + 4)</td>
		</tr>
		<tr>
			<td>2.1.1</td>
			<td>Moški</td>
			<td class="whReportData"><?php echo $results["m"][1]; ?></td>
			<td class="whReportData"><?php echo $results["m"][2]; ?></td>
			<td class="whReportData"><?php echo $results["m"][3]; ?></td>
			<td class="whReportData"><?php echo $results["m"][4]; ?></td>
			<td class="whReportData"><?php echo $results["m"][5]; ?></td>
		</tr>
		<tr>
			<td>2.1.2</td>
			<td>Ženske</td>
			<td class="whReportData"><?php echo $results["ž"][1]; ?></td>
			<td class="whReportData"><?php echo $results["ž"][2]; ?></td>
			<td class="whReportData"><?php echo $results["ž"][3]; ?></td>
			<td class="whReportData"><?php echo $results["ž"][4]; ?></td>
			<td class="whReportData"><?php echo $results["ž"][5]; ?></td>
		</tr>
	</tbody>
</table>

<?php

//at the moment we don't manage any locations outside of Slovenia in this app
$totalHours = $w->getHours($year."-01-01", $year."-12-31");

//var_dump($w->getHours("last week", "today"));
?>

<table class="table">
	<tbody>
		<tr>
			<th>2.2</th>
			<th>Skupno število opravljenih prostovoljskih ur glede na kraj opravljanja prostovoljskega dela<br>(2.2.1 do 2.2.3)</th>
			<th class="whReportData"><?php echo $totalHours; ?></th>
		</tr>
		<tr>
			<td>2.2.1</td>
			<td>V Republiki Sloveniji</td>
			<td class="whReportData"><?php echo $totalHours; ?></td>
		</tr>
		<tr>
			<td>2.2.2</td>
			<td>V drugi državi članici EU</td>
			<td class="whReportData"><?php echo 0; ?></td>
		</tr>
		<tr>
			<td>2.2.3</td>
			<td>V tretji državi</td>
			<td class="whReportData"><?php echo 0; ?></td>
		</tr>
	</tbody>
</table>

<?php

foreach($m->workTypeClassification as $workType => $c) {
	$hours[$c["type"]][$c["field"]] = 
		$w->getHours($year."-01-01", $year."-12-31", $workType);
}

?>

<table class="table">
	<tbody>
		<tr>
			<th>2.3</th>
			<th>Skupno število opravljenih prostovoljskih ur glede na področje prostovoljskega dela po vrsti prostovoljskega dela<br>(2.3.1 do 2.3.10)</th>
			<th>Organizacijsko delo<br>(1)</th>
			<th>Vsebinsko delo<br>(2)</th>
			<th>Drugo delo<br>(3)</th>
			<th>Skupaj<br>(4 = 1 + 2 + 3)</th>
		</tr>
		<tr>
			<th colspan="2"></th>
			<th class="whReportData"><?php echo $hours["2.3.3"][1] + $hours["2.3.5"][1]; ?></th>
			<th class="whReportData"><?php echo $hours["2.3.3"][2] + $hours["2.3.5"][2]; ?></th>
			<th></th>
			<th class="whReportData"><?php echo $totalHours; ?></th>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td>(1)</td>
			<td>(2)</td>
			<td>(3)</td>
			<td>(4 = 1 + 2 + 3)</td>
		</tr>
		<tr>
			<td>2.3.1</td>
			<td>Civilna zaščita in reševanje</td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
		</tr>
		<tr>
			<td>2.3.2</td>
			<td>Človekove pravice in civilne svoboščine</td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
		</tr>
		<tr>
			<td>2.3.3</td>
			<td>Varstvo okolja in ohranjanje narave</td>
			<td class="whReportData"><?php echo $hours["2.3.3"][1]; ?></td>
			<td class="whReportData"><?php echo $hours["2.3.3"][2]; ?></td>
			<td><?php echo ""; ?></td>
			<td class="whReportData"><?php echo $hours["2.3.3"][1] + $hours["2.3.3"][2]; ?></td>
		</tr>
		<tr>
			<td>2.3.4</td>
			<td>Kultura in umetnost</td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
		</tr>
		<tr>
			<td>2.3.5</td>
			<td>Socialna dejavnost</td>
			<td class="whReportData"><?php echo $hours["2.3.5"][1]; ?></td>
			<td class="whReportData"><?php echo $hours["2.3.5"][2]; ?></td>
			<td><?php echo ""; ?></td>
			<td class="whReportData"><?php echo $hours["2.3.5"][1] + $hours["2.3.5"][2]; ?></td>
		</tr>
		<tr>
			<td>2.3.6</td>
			<td>Rekreacija</td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
		</tr>
		<tr>
			<td>2.3.7</td>
			<td>Turizem</td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
		</tr>
		<tr>
			<td>2.3.8</td>
			<td>Vzgoja in izobraževanje</td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
		</tr>
		<tr>
			<td>2.3.9</td>
			<td>Zdravje</td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
		</tr>
		<tr>
			<td>2.3.10</td>
			<td>Človek, narava in družbene vrednote</td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo ""; ?></td>
		</tr>
	</tbody>
</table>

<table class="table">
	<tbody>
		<tr>
			<td>2.4</td>
			<td>Opis morebitne problematike pri izvajanju prostovoljstva, pobude in predlogi</td>
		</tr>
	</tbody>
</table>

</div>
<?php

if($printout) {
	unset($document);
	$pdf->collectContent();
}