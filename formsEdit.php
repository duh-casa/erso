<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewForms") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Podrobnosti vloge", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

require_once "interfaces/formsInterface.php";
$c = new formsInterface();

if(!isset($_GET["q"])) {
 header("Location: index.php", true, 307); //invalid request, redirect back
} else {

if($_SERVER['REQUEST_METHOD'] === 'POST') {

 if(isset($_POST["status"])) {
  $c->updateStatus($_POST["q"], $_POST["status"], $a->user["username"]);
 } else {
  $c->updateNotes($_POST["q"], $_POST["notes"]);
 }

}

$i = $c->details($_GET["q"]);

$document->add("header", array("auth" => $a));

?>
<h2>Povzetek vloge</h2>

<table id="main" class="table table-striped">
 <thead>
  <tr>
   <th>Vloga</th>
   <th>Material</th>
  </tr>
 </thead>
 <tbody id="rows">
     <tr class="<?php echo $i["status-color"]; ?>">
      <td>
        <p style="font-weight: bold;">Referenčna številka: <?php echo $i["type-code"]; ?>-<?php echo $i["id"]; ?></p>
        <hr>
        <p><?php
          echo implode(", ", array_filter(
            array($i["nameLastname"], $i["address"], $i["email"], $i["phone"])
          ));
         ?></p>
        <p><?php echo $i["status"]; ?></p>
      </td>
      <td>
        <p><?php echo $i["type"]; ?> sledečo računalniško opremo:</p>
        <ul>
        <?php foreach($i["gear"] as $g) { ?>
          <li><?php echo $g; ?></li>
        <?php } ?>
        </ul>
      </td>
     </tr> 
  </tbody>
</table>

<h2>Stanje vloge</h2>
<table id="main" class="table table-striped">
  <tbody>
    <tr>
      <td style="text-align: center;">
       <form method="POST">
        <input type="hidden" name="q" value="<?php echo $i["id"]; ?>">
        <input type="hidden" name="status" value="sprejeto">
        <input type="submit" class="btn btn-default" value="Odobri">
       </form>
      </td>
      <td style="text-align: center;">
       <form method="POST">
        <input type="hidden" name="q" value="<?php echo $i["id"]; ?>">
        <input type="hidden" name="status" value="pripravljeno">
        <input type="submit" class="btn btn-success" value="Pripravljeno">
       </form>
      </td>
      <td style="text-align: center;">
       <form method="POST">
        <input type="hidden" name="q" value="<?php echo $i["id"]; ?>">
        <input type="hidden" name="status" value="prevzeto">
        <input type="submit" class="btn btn-info" value="Prevzeto">
       </form>
      </td>
      <td style="text-align: center;">
       <form method="POST">
        <input type="hidden" name="q" value="<?php echo $i["id"]; ?>">
        <input type="hidden" name="status" value="preklicano">
        <input type="submit" class="btn btn-danger" value="Dopolnitev ali preklic">
       </form>
      </td>
    </tr>
  </tbody>
</table>

<?php if($i["type"] == "POTREBUJE") { ?>
<p><label class="label label-info">V vednost</label> Ko tu pritisneš tipko Pripravljeno, se prejemnik samodejno vnese v bazo strank (prejemnikov donacij). Če prejemnika tam ne najdeš, poizkusi osvežiti stran. Samodejni vnos se za posamezno vlogo zgodi samo enkrat.</p>
<?php } ?>
<p><label class="label label-info">V vednost</label> Samodejna obvestila po e-mailu se pošiljajo vsak dan ob 18:00. Vsaka vloga dobi eno obvestilo na dan ob spremembi -- za vloge ki so do takrat v stanju odobreno in prošnje ki so do takrat v stanju pripravljeno. Sistem e-mail obvestil ne obravnava več vlog ki so že prevzete.</p>


<table id="main" class="table table-striped">


<h2>Podrobnosti vloge</h2>

<?php if(isset($_GET["debug"])) { ?>
  <pre>
  <?php echo print_r($i, true); ?>
  </pre>
<?php } ?>

<?php

if($i["type-code"] == "PP") {
  include "formsDetails-PP.php";
} elseif($i["type-code"] == "PO") {
  include "formsDetails-PO.php";
} elseif($i["type-code"] == "DP") {
  include "formsDetails-DP.php";
} elseif($i["type-code"] == "DO") {
  include "formsDetails-DO.php";
}

?>

<h2>Interna opomba</h2>

<form method="POST">
  <input type="hidden" name="q" value="<?php echo $i["id"]; ?>">
  <table class="table">
    <tbody>
      <tr>
        <td>
          <textarea rows="5" autocomplete="off" class="form-control" name="notes" id="notes" placeholder="Poljubna opomba"><?php echo $i['opombe']; ?></textarea>
        </td>
      </tr>
      <tr>
        <td>
          <input type="submit" class="btn btn-primary" value="Shrani opombo">
        </td>
      </tr>
    </tbody>
  </table>
</form>

<br>
<br>
<br>
<br>

<?php }
