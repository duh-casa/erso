<div class="bee-page-container">
	<div class="bee-row bee-row-1">
		<div class="bee-row-content">
			<div class="bee-col bee-col-1 bee-col-w12">
				<div class="bee-block bee-block-1 bee-paragraph">
					<p><strong>PROŠNJA - ORGANIZACIJA - REFERENČNA ŠTEVILKA PO-<?php echo $i["id"]; ?> - PODATKI O ORGANIZACIJI:</strong></p>
				</div>
				<div class="bee-block bee-block-2 bee-divider">
					<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
				</div>
				<div class="bee-block bee-block-3 bee-html-block">
					<style type="text/css">
						.tg {
							border-collapse: collapse;
							border-spacing: 0;
						}

						.tg td {
							border-color: black;
							border-style: solid;
							border-width: 1px;
							font-family: Arial, sans-serif;
							font-size: 14px;
							overflow: hidden;
							padding: 10px 5px;
							word-break: normal;
						}

						.tg th {
							border-color: black;
							border-style: solid;
							border-width: 1px;
							font-family: Arial, sans-serif;
							font-size: 14px;
							font-weight: normal;
							overflow: hidden;
							padding: 10px 5px;
							word-break: normal;
						}

						.tg .tg-lqy6 {
							text-align: right;
							vertical-align: top
						}

						.tg .tg-0lax {
							text-align: left;
							vertical-align: top
						}

						@media screen and (max-width: 767px) {
							.tg {
								width: auto !important;
							}

							.tg col {
								width: auto !important;
							}

							.tg-wrap {
								overflow-x: auto;
								-webkit-overflow-scrolling: touch;
							}
						}
					</style>
					<div class="tg-wrap">
						<table class="tg table table-striped">
							<tbody>
								<tr>
									<td class="tg-lqy6" style="width: 20%;">Naziv organizacije<br /></td>
									<td class="tg-0lax"><?php echo $i["q6_nazivOrganizacije"]; ?></td>
								</tr>
								<tr>
									<td class="tg-lqy6">Naslov organizacije</td>
									<td class="tg-0lax"><?php echo $i["q7_naslovOrganizacije"]["addr_line1"]; ?></td>
								</tr>
								<tr>
									<td class="tg-lqy6">Poštna številka</td>
									<td class="tg-0lax"><?php echo $i["q7_naslovOrganizacije"]["city"]; ?></td>
								</tr>
								<tr>
									<td class="tg-lqy6">Kraj</td>
									<td class="tg-0lax"><?php echo $i["q7_naslovOrganizacije"]["state"]; ?></td>
								</tr>
								<tr>
									<td class="tg-lqy6">Kontaktna oseba v organizaciji</td>
									<td class="tg-0lax"><?php echo $i["q8_kontaktnaOseba"]["first"]; ?></td>
								</tr>
								<tr>
									<td class="tg-lqy6"></td>
									<td class="tg-0lax"><?php echo $i["q8_kontaktnaOseba"]["last"]; ?></td>
								</tr>
								<tr>
									<td class="tg-lqy6">Telefonska številka kontaktne osebe</td>
									<td class="tg-0lax"><?php echo $i["q9_telefonskaStevilka"]["full"]; ?><br /></td>
								</tr>
								<tr>
									<td class="tg-lqy6">Elektronski naslov kontaktne osebe</td>
									<td class="tg-0lax"><?php echo $i["q10_elektronskiNaslov"]; ?><br /></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<p> </p>
	<div class="bee-row bee-row-2">
		<div class="bee-row-content">
			<div class="bee-col bee-col-1 bee-col-w12">
				<div class="bee-block bee-block-1 bee-divider">
					<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
				</div>
				<div class="bee-block bee-block-2 bee-paragraph">
					<p><strong>NAMEN UPORABE DONIRANE OPREME: <?php foreach($i["q12_namenUporabe"] as $g) { ?>
          										  <li><?php echo $g; ?></li>
        										  <?php } ?><br /></strong></p>
				</div>
				<div class="bee-block bee-block-3 bee-divider">
					<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
				</div>
			</div>
		</div>
	</div>
	<p> </p>
	<div class="bee-row bee-row-3">
		<div class="bee-row-content">
			<div class="bee-col bee-col-1 bee-col-w12">
				<div class="bee-block bee-block-1 bee-paragraph">
					<p><strong>Organizacija prosi za sledečo opremo:<br /></strong></p>
				</div>
				<div class="bee-block bee-block-2 bee-paragraph">
					<p><?php foreach($i["gear"] as $g) { ?>
          					<li><?php echo $g; ?></li>
        					<?php } ?> </p>
				<p> </p>
					
				</div>
				<div class="bee-block bee-block-3 bee-paragraph">
					<p>Druga oprema: <?php echo $i["q39_drugo"]; ?></p>
				</div>
				<div class="bee-block bee-block-4 bee-divider">
					<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
				</div>
			</div>
		</div>
	</div>
	<p> </p>
	<div class="bee-row bee-row-4">
		<div class="bee-row-content">
			<div class="bee-col bee-col-1 bee-col-w12"></div>
		</div>
	</div>
	<p> </p>
	<div class="bee-row bee-row-5">
		<div class="bee-row-content">
			<div class="bee-col bee-col-1 bee-col-w12">
				<div class="bee-block bee-block-1 bee-paragraph">
					<p><strong>Dodatne opombe k prošnji:<br /></strong></p>
				</div>
				<div class="bee-block bee-block-2 bee-paragraph">
					<p><?php echo $i["q42_biNam"]; ?></p>
				</div>
			</div>
		</div>
	</div>
	<p> </p>
	<div class="bee-block bee-block-3 bee-divider">
					<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="bee-row bee-row-5">
		<div class="bee-row-content">
			<div class="bee-col bee-col-1 bee-col-w12">
				<div class="bee-block bee-block-1 bee-paragraph">
					<p><strong>Zgodovina obravnave te prošnje<br /></strong></p>
				</div>
				<div class="bee-block bee-block-2 bee-paragraph">
					<p><?php foreach($i["status-tooltip"] as $g) { ?>
          						<li><?php echo $g; ?></li>
        						<?php } ?></p>
				</div>
			</div>
		</div>
	</div>
</div>
