<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewForms") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Vloge", array(
 "bootstrap" => True,
 "chosen" => True, 
 "css" => "style.css",
 "handheldFriendly" => True
));

$document->add("header", array("auth" => $a));

?><h2>Vloge</h2>

<?php ob_start(); ?>
<script>
 window.pgLimit = 10;
 window.pgOffset = 0;

 $.ajaxSetup({ cache: false });
 function appendAjax() {
  $.ajax({
   data: {
    o: "forms",
    q: {
     searchVloge: $("#searchVloge").val(),
     searchStatus: $("#searchStatus").val(),
     searchLocation: "<?php echo $a->user["location"]; ?>",
     pgLimit: window.pgLimit,
     pgOffset: window.pgOffset
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#rows").append(result.html);
   }
  }); 
 }

 function refreshAjax() {
  window.pgOffset = 0;
  $("#rows").empty();
  appendAjax();
 }

 function showAll() {
  $("table#main td").show();
  $("table#main th").show();
 }

 $('input.search').change(function () {
  refreshAjax();
 });

 $("#searchStatus").chosen().change(function () {
  refreshAjax();  
 });

 $(document).ready(function() {
  refreshAjax();
 });

 $(window).scroll(function() {
  if($(window).scrollTop() + $(window).height() == $(document).height()) {
   window.pgOffset = window.pgOffset + window.pgLimit;
   appendAjax();
  }
 });

</script>
<?php $document->addJS(ob_get_clean()); ?>

<p><label class="label label-info">V vednost</label> Če želiš iskati samo prošnje išči besedo "prošnja". Za samo ponudbe pa "ponudba". Lahko napišeš več besed in bo iskalo po vseh.</p>
<p><label class="label label-warning">Pozor!</label> Ta iskalnik ima težave z malimi in velikimi šumniki. Če ne najdeš "društvo", probaj "DRUŠTVO".</p>

<table id="main" class="table table-striped">
 <thead>
  <tr>
   <th>Filtriranje po stanju</th>
   <th>Iskanje po oznaki ali vsebini</th>
   <th></th>
   <th></th>
  </tr>
  <tr>
   <td><select id="searchStatus" name="searchStatus">
     <option value="" selected="selected">Vsa stanja</option>
     <option value="nepreverjeno">NEPREVERJENO</option>
     <option value="qc-sprejeto">Sprejeto in v pripravljanju</option>
     <option value="qc-pripravljeno">Pripravljeno za prevzem</option>
     <option value="qc-prevzeto">Prevzeto, končano</option>
     <option value="qc-preklicano">Preklicano</option>
   </select></td>
   <td><input type="text" class="form-control search" id="searchVloge"></td>
   <td><a href="javascript:refreshAjax();" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></td>
   <td></td>
  </tr>
  <tr>
   <th>Vloga</th>
   <th>Material</th>
   <th></th>
   <th></th>
  </tr>
 </thead>
 <tbody id="rows">
 </tbody>
 <tfoot>
  <tr>
   <td colspan="2"></td>
   <td><a href="javascript:window.pgOffset=window.pgOffset+window.pgLimit; appendAjax();" class="btn btn-info"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Naloži več</a></td>
   <td></td>
  </tr>   
 </tfoot>
</table>
