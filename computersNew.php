<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "editComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Nov računalnik", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

if($_SERVER['REQUEST_METHOD'] === 'POST') {

 require_once "interfaces/computersInterface.php";
 $c = new computersInterface();
 $data = $_POST;
 $data["enteredBy"] = $a->user["username"];
 $c->newEntry($data);
 
 ?><script>
  const bc = new BroadcastChannel("erso-computers");
  bc.postMessage("reload");
  <?php if (http_response_code() == 200) { ?>window.close();<?php } ?>
 </script><?php
 
} else {

 $document->add("header", array("auth" => $a));

?><h2>Oprema</h2>

<?php ob_start(); ?>
<script>
 $.ajaxSetup({ cache: false });
 function refreshAjax() {
  $.ajax({
   data: {o: "autoEntry"},
   url: "ajax.php",
   success: function(result) {
    $("#autoEntryList").html(result.html);
   }
  }); 
 }
 
 function ajaxFill(id) {
  $.ajax({
   data: {o: "autoEntryFill", q: {"id": id}},
   url: "ajax.php",
   success: function(result) {
    $.each(result.fields, function (key, value) {
     if($("#" + key).val() == "") {
      $("#" + key).val(value); 
     }
    });
   }
  }); 
 }
 
 $(document).ready(function() {
  refreshAjax();
 });
 
</script>>
<?php $document->addJS(ob_get_clean()); ?>

<table class="table">
 <tbody>
  <tr> 
   <td>
    <h3>Nov vnos</h3>
    <form method="POST">
     <table class="table">
      <tbody>
       <tr>
        <th>Tip</th>
        <td><input type="text" class="form-control" name="type" id="type"></td>
       </tr>
       <tr>
        <th>Model</th>
        <td><input type="text" class="form-control" name="model" id="model"></td>
       </tr>
       <tr>
        <th>Stara oznaka</th>
        <td><input type="text" class="form-control" name="legacyID"></td>
       </tr>
       <tr>
        <th>Grafična</th>
        <td><input type="text" class="form-control" name="graphics" id="graphics"></td>
       </tr>
       <tr>
        <th>CPU</th>
        <td>
         <table class="table">
          <tbody>
           <tr>
            <th>Model</th>
            <td><input type="text" class="form-control" name="cpuModel" id="cpuModel"></td>
           </tr>
           <tr>
            <th>Frekvenca</th>
            <td><input type="text" class="form-control" name="cpuSpeed" id="cpuSpeed"></td>
           </tr>
           <tr>
            <th>Cache</th>
            <td><input type="text" class="form-control" name="cpuCache" id="cpuCache"></td>
           </tr>
          </tbody>
         </table>
        </td>
       </tr>
       <tr>
        <th>Disk</th>
        <td>
         <table class="table">
          <tbody>
           <tr>
            <th>Model</th>
            <td><input type="text" class="form-control" name="diskModel" id="diskModel"></td>
           </tr>
           <tr>
            <th>Serijska</th>
            <td><input type="text" class="form-control" name="diskSerial" id="diskSerial"></td>
           </tr>
           <tr>
            <th>Velikost (GB)</th>
            <td><input type="text" class="form-control" name="diskSize" id="diskSize"></td>
           </tr>
          </tbody>
         </table>    
        </td>
       </tr>
       <tr>
        <th>RAM</th>
        <td>
         <table class="table">
          <tbody>
           <tr>
            <th>Frekvenca</th>
            <td><input type="text" class="form-control" name="ramSpeed" id="ramSpeed"></td>
           </tr>
           <tr>
            <th>Velikost (MB)</th>
            <td><input type="text" class="form-control" name="ramSize" id="ramSize"></td>
           </tr>
          </tbody>
         </table>    
        </td>
       </tr>
      </tbody>
      <tfoot>
       <tr>
        <td></td>
        <td>
         <button type="submit" class="btn btn-primary">
          <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Shrani
         </button>
        </td>
       </tr>
      </tfoot>
     </table>
     <input type="hidden" name="json" id="json">
     <input type="hidden" name="diskManufacturer" id="diskManufacturer">
    </form>
   </td>
   <td>
    <h3>Avtomatski vnos</h3>
    Prosim upoštevajte da avtomatski vnos ne prepisuje polj ki niso prazna.
    <table class="table">
     <thead>
      <tr>
       <th>#</th>
       <th>Model</th>
       <th>Čas odčitka</th>
       <th><a href="javascript:refreshAjax();" class="btn btn-info"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></th>
      </tr>
     </thead>
     <tbody id="autoEntryList">
     </tbody>
    </table>
   </td>
  </tr>
 </tbody>
</table>
<?php }
