<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify() === False) { //anyone can print documents, for now
 die("Potrebno se je prijaviti");
}

if(isset($_GET["o"])) {

 $document = array(
  "computers" => "Nalepke_PC",
  "screens" => "Nalepke_ostalo",
  "peripherals" => "Nalepke_ostalo"
 );

 require_once "interfaces/labelsInterface.php";
 $l = new labelsInterface($document[$_GET["o"]]);

 require_once "interfaces/labelQueueInterface.php";

 require_once "inc/idEncoding.php";
 $e = new idEncoding();

 if(isset($_GET["q"]["first"])) {
  $l->first = (int) $_GET["q"]["first"];
 }

 if($_GET["o"] == "computers") {

  require_once "interfaces/computersInterface.php";
  $c = new computersInterface();

  $lq = new labelQueueInterface("computers");
  foreach($lq->getMarked() as $item) {
   $encodedId = $e->idEncode($item);
   $citem = $c->details($encodedId);

   //140-6ATEIKD,ChaletOS,04.12.2018,core 2 Duo e8400,3000

   $l->add(array(
    "c-".$encodedId,
    "Ubuntu", //TODO OS is in the disk record
    date("j. n. Y", strtotime($citem["tds"])),
    $citem["cpuModel"],
    $citem["cpuSpeed"]
   ));
  } 

 } elseif($_GET["o"] == "screens" || $_GET["o"] == "peripherals") {

  require_once "interfaces/gearInterface.php";
  $g = new gearInterface();
 
  //70-O224CPD,LCD,17,1740

  $lq = new labelQueueInterface("screens");
  foreach($lq->getMarked() as $item) {
   $encodedId = $e->idEncode($item);
   $gitem = $g->details(array("type" => "screens", "id" => $encodedId));

   $l->add(array(
    "s-".$encodedId,
    "Ekran",
    "", //this is kinda pointless
    $gitem["model"]
   ));
  }

  //screens and peripherianls are printed together
  $lq = new labelQueueInterface("peripherals");
  foreach($lq->getMarked() as $item) {
   $encodedId = $e->idEncode($item);
   $gitem = $g->details(array("type" => "peripherals", "id" => $encodedId));

   $l->add(array(
    "p-".$encodedId,
    "Periferija",
    "", //this is kinda pointless
    $gitem["model"]
   ));
  }
 
 }

}
