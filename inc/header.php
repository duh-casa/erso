<h1>Aplikacija eRSO</h1>
<table style="width: 100%">
 <tbody>
  <tr>
   <td>
   
<?php if(!isset($parameters["auth"]) || $parameters["auth"]->verify(False, "viewComputers"))  {?>
 <a href="index.php" class="btn btn-default"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Računalniki</a>
<?php } ?>
<?php if(!isset($parameters["auth"]) || $parameters["auth"]->verify(False, "viewComputers"))  {?>
 <a href="screens.php" class="btn btn-default"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Monitorji</a>
<?php } ?>
<?php if(!isset($parameters["auth"]) || $parameters["auth"]->verify(False, "viewComputers"))  {?>
 <a href="peripherals.php" class="btn btn-default"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Periferija</a>
<?php } ?>
<?php if(!isset($parameters["auth"]) || $parameters["auth"]->verify(False, "viewComputers"))  {?>
 <a href="disks.php" class="btn btn-default"><span class="glyphicon glyphicon-hdd" aria-hidden="true"></span> Diski</a>
<?php } ?>
<?php if(!isset($parameters["auth"]) || $parameters["auth"]->verify(False, "viewComputers"))  {?>
 <a href="labels.php" class="btn btn-default"><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span> Nalepke</a>   
<?php } ?>
<?php if(!isset($parameters["auth"]) || $parameters["auth"]->verify(False, "viewMembers"))  {?>
 <a href="members.php" class="btn btn-default"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Člani</a>
<?php } ?>
<?php if(!isset($parameters["auth"]) || $parameters["auth"]->verify(False, "viewWorkHours"))  {?>
 <a href="workHours.php" class="btn btn-default"><span class="glyphicon glyphicon-time" aria-hidden="true"></span> Prisotnost</a>
<?php } ?>
<?php if(!isset($parameters["auth"]) || $parameters["auth"]->verify(False, "donations"))  {?>
 <a href="donations.php" class="btn btn-default"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span> Stranke</a>
<?php } ?>
<?php if(!isset($parameters["auth"]) || $parameters["auth"]->verify(False, "returns"))  {?>
 <a href="returns.php" class="btn btn-default"><span class="glyphicon glyphicon-resize-small" aria-hidden="true"></span> Vračila</a>
<?php } ?>
<?php if(!isset($parameters["auth"]) || $parameters["auth"]->verify(False, "viewForms"))  {?>
 <a href="forms.php" class="btn btn-default"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Vloge</a>
<?php } ?>

   </td>
   <td>
   
<div class="right">
<?php if(isset($parameters["auth"])) { 
 ob_start();
?>
 <script>
 $.ajaxSetup({ cache: false });
 function authPing() {
  $.ajax({
   data: {
    o: "authPing",
   },
   url: "ajax.php",
   success: function(result) {
		if(typeof(result.redirect) != "undefined" && result.redirect !== null) {
	    window.location.replace(result.redirect);
		}
   }
  });
 }
 setInterval(authPing, 30000);
 </script>
<?php
 $this->addJS(ob_get_clean());

 require_once "interfaces/membersInterface.php";
 $m = new membersInterface($parameters["auth"]);
?>
 <p class="greeting">Pozdravljen, <?php echo $m->name($parameters["auth"]->user["username"]); ?> (<?php echo $parameters["auth"]->user["username"]; ?>) [&nbsp;<?php echo $parameters["auth"]->user["location"]; ?>&nbsp;]</p>
<?php } ?>
<a href="login.php?q=logout" class="btn btn-danger">
 <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Odjava</a>
<a href="javascript:window.open('membersPassword.php', '_blank'); void(0);" class="btn btn-default">
 <span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Spremeni svoje geslo</a>
</div>

   </td>
  </tr>
 </tbody>
</table>
