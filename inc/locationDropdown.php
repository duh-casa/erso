<?php
require_once "interfaces/locationsInterface.php";
require_once "inc/auth.php";

function locationDropdown($name, $default = null, $blank = false) { 
	if (!$default) {
	 	$a = new auth();
	  $default = $a->user["location"];
	}
	?>
	<select name="<?php echo $name ?>"><?php 
	if ($blank){ ?>
		<option value=""><?php echo $blank ?></option><?php
	}

	$locI = new locationsInterface();
	foreach ($locI->all() as $id => $details) {
		?>
		<option value="<?php echo $id ?>"<?php if ($id == $default) echo " selected"?>><?php echo $details["name"] . " [" . $id . "]" ?></option>
		<?php
	}
	?>
	</select>
	<?php
}

?>
