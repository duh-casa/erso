<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "editComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Naredi evidenco ur", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

if(!isset($_GET["q"])) {
 header("Location: members.php", true, 307); //invalid request, redirect back
}

?>

<script>
function printout(docType) {
  var url = "printouts.php?" + $.param({
    t: docType,
    o: "timeRecord",
    q: {
      username: $("#username").val(),
      from: $("#from").val(),
      length: $("#length").val()
    }
  });
  window.open(url, '_blank');
}
</script>

<?php $document->add("header", array("auth" => $a)); ?>

<h2>Naredi evidenco ur</h2>

<h3>Uredi vnos</h3>
<form method="POST">
 <table class="table">
  <tbody>
   <tr>
    <th style="width: 20%;">Uporabniško ime</th>
    <td><input type="text" class="form-control" id="username" name="username" readonly value="<?php echo $_GET['q']; ?>"></td>
   </tr>
   <tr>
    <th>Začetni mesec</th>
    <td><input type="text" class="form-control" id="from" name="from" placeholder="leto-mesec" value="<?php echo date("Y-m"); ?>"</td>
   </tr>
   <tr>
    <th>Trajanje <small>(v mesecih)</small></th>
    <td><input type="number" class="form-control" id="length" name="length" value="1" min="1"></td>
   </tr>
  </tbody>
  <tfoot>
   <tr>
    <td></td>
    <td>
     <a href="javascript:printout('pdf')" type="submit" class="btn btn-success">
      <span class="glyphicon glyphicon-time" aria-hidden="true"></span> Naredi za tisk
     </a>
     <a href="javascript:printout('odt')" type="submit" class="btn btn-success">
      <span class="glyphicon glyphicon-time" aria-hidden="true"></span> Naredi za urejanje
     </a>
    </td>
   </tr>
  </tfoot>
 </table>
 <input type="hidden" name="id" value="<?php echo $_GET["q"]; ?>">
</form>
