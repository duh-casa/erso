<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "donations") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Oddaja", array(
 "bootstrap" => True,
 "chosen" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

/*

  * Name
  * Last name
  * Company name
  * Address
  * City
  * Birthdate
  * eMail address
  * Phone
  * Status (unemployed, etc)
  * Notes
  
  * Equipment (list)

*/

require_once "interfaces/donationsInterface.php";
$d = new donationsInterface($a);

if($_SERVER['REQUEST_METHOD'] === 'POST') {

 $data = $_POST;
 $data["enteredBy"] = $a->user["username"];
 
 if($data["id"] == 0) {
  $data["id"] = $d->newEntry($data);
 } else {
  $d->modifyEntry($data); 
 }
 
}

$addGear = False;
if(isset($_GET["q"])) {

 $e = $d->donated($_GET["q"]);
 if($e === False) {

  if($_GET["q"]["type"] == "computers") {

   require_once "interfaces/computersInterface.php";
   $c = new computersInterface();

  } elseif($_GET["q"]["type"] == "screens") {

   require_once "interfaces/screensInterface.php";
   $c = new screensInterface();

  } elseif($_GET["q"]["type"] == "peripherals") {

   require_once "interfaces/peripheralsInterface.php";
   $c = new peripheralsInterface();
  
  }
  $addGear = $c->details($_GET["q"]["id"]);
  $addGear["type"] = $_GET["q"]["type"];
  unset($c);

  $entered = "";
  $e = $d->emptyUser();
  $e["id"] = 0;
 
 } else {
  
  $entered = "disabled";
  
 }

} else {

 $entered = "";
 $e = $d->emptyUser();
 $e["id"] = 0;

}

 $document->add("header", array("auth" => $a));
 
 ?><h2>Stranke</h2>
 
 <?php ob_start(); ?>
 <script>
  $.ajaxSetup({ cache: false }); 
  function loadListOfOptions() {
   $.ajax({
    data: {
     o: "donationsUserList"
    },
    url: "ajax.php",
    success: function(result) {
     $("#search").html(result.html);
     $("#search").val('<?php echo $e["id"]; ?>');
     $("#search").trigger("chosen:updated");
    }
   });
   
  }
  
  function printout(docType) {
   var checked = [];
   $('input[type=checkbox]:checked').not('#checkAll').each(function(i, obj) {
    checked.push($(this).val());
   });
   
   var url = "printouts.php?" + $.param({
    t: docType,
    o: 'donation', q: {
     name: $("#name").val(),
     lastname: $("#lastname").val(),
     company: $("#company").val(),
     address: $("#address").val(),
     city: $("#city").val(),
     birthdate: $("#birthdate").val(),
     email: $("#email").val(),
     gsm: $("#gsm").val(),
     status: $("#status").val(),
     notes: $("#notes").val(),
     date: $("#date").val(),
     gear: checked
    }
   });
   <?php if($entered === "") {?>
    window.open(url, '_blank');
   <?php } else { ?>
    window.location.replace(url);
   <?php } ?>
  }
  
  function userDetails(id) {
   $.ajax({
    data: {
     o: "donationsUserSearch",
     q: {id: $('#search').val()}
    },
    url: "ajax.php",
    success: function(result) {
     $("#code").html(result.code);
     $("#name").val(result.name);
     $("#lastname").val(result.lastname);
     $("#company").val(result.company);
     $("#address").val(result.address);
     $("#city").val(result.city);
     $("#birthdate").val(result.birthdate);
     $("#email").val(result.email);
     $("#gsm").val(result.gsm);
     $("#status").val(result.status);
     $("#notes").val(result.notes);
     $("#gear").html(result.html);
     $("#forms").html(result.forms);
    }
   });  
  }

  function userGearOnly(id) {
   $.ajax({
    data: {
     o: "donationsUserSearch",
     q: {id: '<?php echo $e["id"]; ?>'}
    },
    url: "ajax.php",
    success: function(result) {
     $("#gear").html(result.html);
     $('input[type=checkbox]').each(function(i, obj) {
      $(this).prop('checked', true);
     });
    }
   });
  }  
  
  $(document).ready(function() {

   $("#checkAll").click(function(){
     $('input:checkbox').not(this).prop('checked', this.checked);
   });

   loadListOfOptions();
   <?php if($entered === "") {?>
    $("#search").chosen().change(userDetails);
   <?php } else { ?>
    userGearOnly();   
   <?php }  ?>
  });
 </script>
 <?php $document->addJS(ob_get_clean()); ?> 
 
 <h3>Oddaja</h3>
 
 <p><label class="label label-info">V vednost</label> Ta vmesnik deluje tako da za vsak kos opreme, ki ga želite dodati kliknete na gumb Oddaja, tu izberete prejemnika v seznamu in kliknete na tipko Vnesi prejem. Če dodajate več kosov opreme istemu prejemniku je ključno da ga poiščete v seznamu in ne vnašate njegovih podatkov na novo. Da gre za istega prejemnika lahko vidite po ID prejemnika.</p>
 <p><label class="label label-warning">Pozor!</label> V kolikor stran osvežite (naprimer z F5) preden vnesete prejem, se neshranjeni podatki izgubijo.</p>

 <table class="table">
  <tbody>
   <tr>
    <td>
     <h4>Prejemnik</h4>
     <form method="POST">
      <table class="table">
       <thead>
        <tr>
         <th style="width: 20%;">Poišči<br>
          <small>
           <label class="label label-warning">Pozor!</label> Če ne izbereš "Nov vnos" popravljaš obstoječega prejemnika.
          </small>
         </th>
         <td>
          <select id="search" name="id" <?php echo $entered; ?>>
          </select>
         </td>
        </tr>      
       </thead>
       <tbody>
        <tr>
         <th>ID prejemnika</th>
         <td id="code"></td>
        </tr>
        <tr>
         <th>Ime</th>
         <td><input type="text" autocomplete="off" class="form-control" name="name" id="name" placeholder="Janez" <?php echo $entered; ?> value="<?php echo $e['name']; ?>"></td>
        </tr>
        <tr>
         <th>Priimek</th>
         <td><input type="text" autocomplete="off" class="form-control" name="lastname" id="lastname" placeholder="Sklopka" <?php echo $entered; ?> value="<?php echo $e['lastname']; ?>"></td>
        </tr>
        <tr>
         <th>Podjetje</th>
         <td><input type="text" autocomplete="off" class="form-control" name="company" id="company" placeholder="" <?php echo $entered; ?> value="<?php echo $e['company']; ?>"></td>
        </tr>
        <tr>
         <th>Naslov</th>
         <td><input type="text" autocomplete="off" class="form-control" name="address" id="address" placeholder="Napačna ulica 13" <?php echo $entered; ?> value="<?php echo $e['address']; ?>"></td>
        </tr>
        <tr>
         <th>Mesto</th>
         <td><input type="text" autocomplete="off" class="form-control" name="city" id="city" placeholder="1234 Neka poljana" <?php echo $entered; ?> value="<?php echo $e['city']; ?>"></td>
        </tr>
        <tr>
         <th>Rojstni datum</th>
         <td><input type="date" autocomplete="off" class="form-control" name="birthdate" id="birthdate" <?php echo $entered; ?> value="<?php $tmpBirthdate = strtotime($e['birthdate']); if($tmpBirthdate > 0) { echo date("Y-m-d", $tmpBirthdate); } ?>"></td>
        </tr>
        <tr>
         <th>eMail naslov</th>
         <td><input type="text" autocomplete="off" class="form-control" name="email" id="email" placeholder="janez.sklopka@gmail.com" <?php echo $entered; ?> value="<?php echo $e['email']; ?>"></td>
        </tr>
        <tr>
         <th>GSM</th>
         <td><input type="text" autocomplete="off" class="form-control" name="gsm" id="gsm" placeholder="040 123 456" <?php echo $entered; ?> value="<?php echo $e['gsm']; ?>"></td>
        </tr>
        <tr>
         <th>Status</th>
         <td><input type="text" autocomplete="off" class="form-control" name="status" id="status" placeholder="Brezposlen" <?php echo $entered; ?> value="<?php echo $e['status']; ?>"></td>
        </tr>
        <tr>
         <th>Opombe</th>
         <td><textarea rows="5" autocomplete="off" class="form-control" name="notes" id="notes" placeholder="Ljubitelj korenja" <?php echo $entered; ?>><?php echo $e['notes']; ?></textarea></td>
        </tr>
       </tbody>
       <tfoot>
        <tr>
         <td></td>
         <td>
          <?php if($addGear === False && $entered === "") {?>
          <button type="submit" class="btn btn-primary">
           <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Vnesi popravek
          </button>
          <?php } elseif($addGear !== False && $entered === "") {?>
          <button type="submit" class="btn btn-primary">
           <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Vnesi prejem
          </button>
          <?php } else {?>
          <button class="btn btn-success" disabled>
           <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Vnos uspel
          </button>
          <?php } ?>
         </td>
        </tr>
       </tfoot>
      </table>
      <?php if($addGear !== False) { ?>
       <input type="hidden" name="addGearId" value="<?php echo $addGear["id"]; ?>">
       <input type="hidden" name="addGearType" value="<?php echo $addGear["type"]; ?>">
      <?php } ?>      
     </form>
    </td>
    <td>
     <table>
      <tbody>
       <tr>
        <td> 

     <h4>Oprema</h4>
     <table class="table">
      <thead>
       <tr>
        <th>Oznaka</th>
        <th>Model</th>
        <th>Tip</th>
        <th>Izpis <input id="checkAll" type="checkbox" value="" checked></th>
       </tr>
      </thead>
      <tbody id="gear">
      </tbody>
      <tfoot>
       <?php if($addGear !== False) { ?>
       <tr>
        <td colspan="4">Dodal se bo vnos:</td>
       </tr>
       <tr>
        <td><?php echo $addGear["id"]; ?></td>
        <td><?php echo $addGear["model"]; ?></td>
        <td><?php echo $addGear["type"]; ?></td>
        <td><input type="checkbox" value="<?php echo substr($addGear["type"], 0, 1)."-".$addGear["id"] ?>" checked></td>
       </tr>
       <?php } ?>
       <tr>
        <th colspan="2" style="text-align: right;">Datum za izpis</th>
        <td colspan="2"><input type="date" autocomplete="off" class="form-control" name="date" id="date" value="<?php echo date("Y-m-d"); ?>"></td>
       </tr>
       <tr>
        <td colspan="4" style="text-align: right;">
         <a href="javascript:printout('pdf');" class="btn btn-info">
          <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Obrazec za oddajo (za tisk)
         </a>
         <br><br>
         <a href="javascript:printout('odt');" class="btn btn-info">
          <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Obrazec za oddajo (za urejanje)
         </a>
        
            </td>
           </tr>
           <tr>
            <td colspan="4">

             <h4>Vloge</h4>
             <table class="table">
              <tbody id="forms">
              </tbody>
             </table>

            </td>
           </tr>
          </tbody>
         </table>
        </td>
       </tr>
      </tfoot>
     </table>
    </td>
   </tr>


  </tbody>
 </table>
