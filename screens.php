<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Monitorji", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

$document->add("header", array("auth" => $a));

?><h2>Monitorji</h2>

<?php ob_start(); ?>
<script>
 $.ajaxSetup({ cache: false });
 const bc = new BroadcastChannel("erso-screens");
 bc.onmessage = function (ev) { refreshAjax(); } 
 
 window.pgLimit = 10;
 window.pgOffset = 0;

 function appendAjax() {
  $.ajax({
   data: {
    o: "screens",
    q: {
     searchManufacturer: $("#searchManufactuter").val(),
     searchModel: $("#searchModel").val(),
     searchSerial: $("#searchSerial").val(),
     searchSize: $("#searchSize").val(),
     searchNewID: $("#searchNewID").val(),
     searchLegacyID: $("#searchLegacyID").val(),
     searchNotes: $("#searchNotes").val(),
     searchLocation: "<?php echo $a->user["location"]; ?>",
     pgLimit: window.pgLimit,
     pgOffset: window.pgOffset
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#rows").append(result.html);
   }
  }); 
 }

 function refreshAjax() {
  window.pgOffset = 0;
  $("#rows").empty();
  appendAjax();
 }

 function showAll() {
  $("table#main td").show();
  $("table#main th").show();
 }

 $('input.search').change(function () {
  refreshAjax();
 });

 $(document).ready(function() {
  refreshAjax();
 });

 $(window).scroll(function() {
  if($(window).scrollTop() + $(window).height() == $(document).height()) {
   window.pgOffset = window.pgOffset + window.pgLimit;
   appendAjax();
  }
 });
 
</script>
<?php $document->addJS(ob_get_clean()); ?>

<?php if($a->verify(False, "editComputers") !== False) { ?>
 <a href="javascript:window.open('screensNew.php', '_blank'); void(0);" class="btn btn-success"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Nov vnos</a>
<?php } ?>

<table id="main" class="table table-striped">
 <thead>
  <tr>
   <th>Proizvajalec</th>
   <th>Model</th>
   <th class="hide5">Serijska</th>
   <th>Diagonala</th>
   <th>Oznaka</th>
   <th>Stara oznaka</th>
   <th class="hide3">Opombe</th>
   <th>Stanje</th>
   <th colspan="2" class="showAll"><a href="javascript:showAll();" class="btn btn-info pull-right"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Prikaži vse stolpce</a></th>
  </tr>
  <tr>
   <td><input type="text" class="form-control search" id="searchManufacturer"></td>
   <td><input type="text" class="form-control search" id="searchModel"></td>
   <td class="hide5"><input type="text" class="form-control search" id="searchSerial"></td>
   <td><input type="text" class="form-control search" id="searchSize"></td>
   <td><input type="text" class="form-control search" id="searchNewID"></td>
   <td><input type="text" class="form-control search" id="searchLegacyID"></td>
   <td class="hide3"><input type="text" class="form-control search" id="searchNotes"></td>
   <td></td>
   <td><a href="javascript:refreshAjax();" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></td>
   <td></td>
  </tr>
 </thead>
 <tbody id="rows">
 </tbody>
 <tfoot>
  <tr>
   <td></td>
   <td></td>
   <td class="hide5"></td>
   <td></td>
   <td></td>
   <td></td>
   <td class="hide3"></td>
   <td></td>
   <td><a href="javascript:window.pgOffset=window.pgOffset+window.pgLimit; appendAjax();" class="btn btn-info"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Naloži več</a></td>
   <td></td>
  </tr>
 </tfoot>
</table>
