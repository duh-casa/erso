<div class="bee-page-container">
<div class="bee-row bee-row-1">
	<div class="bee-row-content">
		<div class="bee-col bee-col-1 bee-col-w12">
			<div class="bee-block bee-block-1 bee-paragraph">
				<p><strong>DONACIJA - POSAMEZNIK - REFERENČNA ŠTEVILKA DP-<?php echo $i["id"]; ?> - PODATKI O DONATORJU:</strong></p>
			</div>
			<div class="bee-block bee-block-2 bee-divider">
				<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
			</div>
			<p> </p>
			<div class="bee-block bee-block-3 bee-html-block">
				<style type="text/css">
					.tg {
						border-collapse: collapse;
						border-spacing: 0;
					}

					.tg td {
						border-color: black;
						border-style: solid;
						border-width: 1px;
						font-family: Arial, sans-serif;
						font-size: 14px;
						overflow: hidden;
						padding: 10px 5px;
						word-break: normal;
					}

					.tg th {
						border-color: black;
						border-style: solid;
						border-width: 1px;
						font-family: Arial, sans-serif;
						font-size: 14px;
						font-weight: normal;
						overflow: hidden;
						padding: 10px 5px;
						word-break: normal;
					}

					.tg .tg-lqy6 {
						text-align: right;
						vertical-align: top
					}

					.tg .tg-0lax {
						text-align: left;
						vertical-align: top
					}

					@media screen and (max-width: 767px) {
						.tg {
							width: auto !important;
						}

						.tg col {
							width: auto !important;
						}

						.tg-wrap {
							overflow-x: auto;
							-webkit-overflow-scrolling: touch;
						}
					}
				</style>
				<div class="tg-wrap">
					<table class="tg table table-striped">
						<thead>
							<tr>
								<td class="tg-lqy6" style="width: 20%;">Ime<br /></td>
								<td class="tg-0lax"><?php echo $i["q7_kontaktnaOseba"]["first"]; ?></td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="tg-lqy6">Priimek</td>
								<td class="tg-0lax"><?php echo $i["q7_kontaktnaOseba"]["last"]; ?></td>
							</tr>
							<tr>
								<td class="tg-lqy6">Telefonska številka</td>
								<td class="tg-0lax"><?php echo $i["q8_telefonskaStevilka"]["full"]; ?></td>
							</tr>
							<tr>
								<td class="tg-lqy6">Elektronski naslov</td>
								<td class="tg-0lax"><?php echo $i["q9_elektronskiNaslov"]; ?><br /></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<p> </p>
<div class="bee-row bee-row-2">
	<div class="bee-row-content">
		<div class="bee-col bee-col-1 bee-col-w12">
			<div class="bee-block bee-block-1 bee-divider">
				<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
			</div>
			<div class="bee-block bee-block-2 bee-divider">
				<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
			</div>
		</div>
	</div>
</div>
<div class="bee-row bee-row-3">
	<div class="bee-row-content">
		<div class="bee-col bee-col-1 bee-col-w12">
			<div class="bee-block bee-block-1 bee-paragraph">
				<p><strong>Donator ponuja sledečo opremo:<br /></strong></p>
			</div>
			<div class="bee-block bee-block-2 bee-paragraph">
				<p><?php echo $i["q13_ponujamDonacijo"]; ?></p>
			</div>
			<div class="bee-block bee-block-3 bee-divider">
				<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
			</div>
		</div>
	</div>
</div>
<p> </p>
<div class="bee-row bee-row-4">
	<div class="bee-row-content">
		<div class="bee-col bee-col-1 bee-col-w12">
			<div class="bee-block bee-block-1 bee-paragraph">
				<p><strong>Datoteke, pripete k tej ponudbi:<br /></strong></p>
			</div>
			<div class="bee-block bee-block-2 bee-paragraph">
				<p>Ni pripetih datotek.</p>
			</div>
			<div class="bee-block bee-block-3 bee-divider">
				<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
			</div>
			<div class="bee-block bee-block-4 bee-paragraph">
				<p><strong>Donator lahko ponujeno opremo dostavi v enoto: <?php echo $i["q36_donacijoLahko36"]; ?><br /></strong></p>
			</div>
			<div class="bee-block bee-block-5 bee-divider">
				<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
			</div>
		</div>
	</div>
</div>
<p> </p>
<div class="bee-row bee-row-5">
	<div class="bee-row-content">
		<div class="bee-col bee-col-1 bee-col-w12">
			<div class="bee-block bee-block-1 bee-paragraph">
				<p><strong>Dodatne opombe k ponudbi:<br /></strong></p>
			</div>
			<div class="bee-block bee-block-2 bee-paragraph">
				<p><?php echo $i["q25_biNam"]; ?></p>
			</div>
		</div>
		<div class="bee-block bee-block-3 bee-divider">
			<div class="center bee-separator" style="border-top:1px solid #dddddd;width:100%;"></div>
		</div>
	</div>
</div>
<p> </p>
<div class="bee-row bee-row-5">
	<div class="bee-row-content">
		<div class="bee-col bee-col-1 bee-col-w12">
			<div class="bee-block bee-block-1 bee-paragraph">
				<p><strong>Zgodovina obravnave te prošnje<br /></strong></p>
			</div>
			<div class="bee-block bee-block-2 bee-paragraph">
				<p><?php foreach($i["status-tooltip"] as $g) { ?>
          						<li><?php echo $g; ?></li>
        						<?php } ?></p>
			</div>
		</div>
	</div>
</div>