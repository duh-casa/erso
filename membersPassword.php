<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify() === False) { //anyone can change their password
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Spremeni geslo", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

require_once "interfaces/membersInterface.php";
$m = new membersInterface($a);

if(!isset($_GET["q"])) {
 $q = $a->user["username"];
} else {
 $q = $_GET["q"];
}

$error = "";
if($_SERVER['REQUEST_METHOD'] === 'POST') {

 $data = $_POST;
 
 if($a->verify(False, "editPasswords") !== False || $a->checkPassword($data["oldPassword"], $data["username"])) { //skip this check if you are an admin
  if($data["verifyPassword"] == $data["newPassword"]) {
   $a->changePassword($data["newPassword"], $data["username"]);

   ?><script>
    const bc = new BroadcastChannel("erso-members");
    bc.postMessage("reload");
    <?php if (http_response_code() == 200) { ?>window.close();<?php } ?>
   </script><?php
    
  } else {
   $error = "Geslo ni ustrezno ponovljeno";
  }
 } else {
  $error = "Staro geslo ni pravilno";
 }

}

$document->add("header", array("auth" => $a));

?>
<h2>Člani</h2>
<h3>Sprememba gesla</h3>
<form method="POST">
 <table class="table">
  <tbody>
   <tr>
    <th style="width: 20%;">Uporabniško ime</th>
    <td><input type="text" class="form-control" name="username" value="<?php echo $q; ?>" readonly></td>
   </tr>
   <tr>
    <th>
     Staro geslo<br>
     <small>Če si admin ti ni treba tega izpolnjevati</small>
    </th>
    <td><input type="password" class="form-control" name="oldPassword"></td>
   </tr>
   <tr>
    <th>Novo geslo</th>
    <td><input type="password" class="form-control" name="newPassword" required></td>
   </tr>
   <tr>
    <th>Ponovi novo geslo</th>
    <td><input type="password" class="form-control" name="verifyPassword" required></td>
   </tr>
   <tr>
    <td></td>
    <td>
     <button type="submit" class="btn btn-primary">
      <span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Spremeni geslo
     </button>
     <span style="color: red;"><?php echo $error; ?></span>
    </td>
   </tr>
  </tbody>
 </table>
</form>
