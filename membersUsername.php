<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "editUsername") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Spremeni uporabniško ime", array(
 "bootstrap" => True,
 "css" => "style.css",
 "handheldFriendly" => True
));

require_once "interfaces/membersInterface.php";
$m = new membersInterface($a);

if(!isset($_GET["q"])) {
 header("Location: members.php", true, 307); //invalid request, redirect back
} else {

$error = "";
if($_SERVER['REQUEST_METHOD'] === 'POST') {

 $data = $_POST;
 
 if(!$m->exists($data["newUsername"])) {
  if(!$m->changeUsername($data["newUsername"], $data["oldUsername"])) { 
   $error = "Nekaj je šlo po zlu"; 
  } else {
  
   ?><script>
    const bc = new BroadcastChannel("erso-members");
    bc.postMessage("reload");
    <?php if (http_response_code() == 200) { ?>window.close();<?php } ?>
   </script><?php

  }
 } else {
  $error = "Novo uporabniško ime je že zasedeno";
 }
 
}

$document->add("header", array("auth" => $a));

?>
<h2>Člani</h2>
<h3>Sprememba uporabniškega imena</h3>
<form method="POST">
 <table class="table">
  <tbody>
   <tr>
    <th style="width: 20%;">Staro uporabniško ime</th>
    <td><input type="text" class="form-control" name="oldUsername" value="<?php echo $_GET['q']; ?>" readonly></td>
   </tr>
   <tr>
    <th style="width: 20%;">Novo uporabniško ime</th>
    <td><input type="text" class="form-control" name="newUsername" placeholder="imepri"></td>
   </tr>
   <tr>
    <td></td>
    <td>
     <button type="submit" class="btn btn-primary">
      <span class="glyphicon glyphicon-random" aria-hidden="true"></span> Spremeni uporabniško ime
     </button>
     <span style="color: red;"><?php echo $error; ?></span>
    </td>
   </tr>
  </tbody>
 </table>
</form><?php }
