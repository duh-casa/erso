<?php

header('Content-Type: application/json');
ob_start();

//authentication is not required to access this API

require_once "interfaces/statisticsInterface.php";
$s = new statisticsInterface();

//output valid JSON
echo json_encode(($s->since("last week") + array("debug" => ob_get_clean())));
