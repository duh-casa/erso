<?php

header('Content-Type: application/json');
$out = array(); ob_start();

//import Query data
if(isset($_GET["q"])) {
 $q = $_GET["q"];
} else {
 $q = array();
}

//switch by Operation
if(isset($_GET["o"])) {
 if($_GET["o"] == "workHoursPresent") {
  
  require_once "interfaces/workHoursInterface.php";
  $w = new workHoursInterface();
  
  ob_start();
  foreach($w->allPresent() as $item) {
   ?>
    <tr>
     <td><?php echo $item["code"]; ?></td>
     <td><?php echo $item["name"]; ?></td>
     <td><?php echo $item["location"]; ?></td>
    </tr>   
   <?php
  }
  $out["html"] = ob_get_clean();   

 } elseif($_GET["o"] == "workType") {

   require_once "interfaces/workHoursInterface.php";
   $w = new workHoursInterface();

   $out = array("workType" => $w->getWorkType($q["code"]));
 
 } elseif($_GET["o"] == "workHoursLogon") {
  
  require_once "interfaces/workHoursInterface.php";
  $w = new workHoursInterface();
  
  $username = $w->resolveCode(trim($q["code"]));
  if(isset($q["workType"])) {
    $workType = (int) $q["workType"];    
  } else {
    $workType = 0;
  }

  if($username !== False) {
   if(!$w->present($username)) {
    $out["operation"] = "logon";
    $w->logon($q["code"], False, $workType);
   } else {
    $out["operation"] = "logoff";
    $w->logoff($q["code"], False, $workType);
   }
  }
  
  ob_start();
  foreach($w->allPresent() as $item) {
   ?>
    <tr>
     <td><?php echo $item["code"]; ?></td>
     <td><?php echo $item["name"]; ?></td>
    </tr>   
   <?php
  }
  $out["html"] = ob_get_clean();    
 }
}

//output valid JSON
echo json_encode(($out + array("debug" => ob_get_clean())));
